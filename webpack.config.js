const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['@babel/polyfill', './src/index.js'],
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'index_bundle.js',
        publicPath: '/'
    },
    resolve: {
        alias: {
            Shared: path.resolve(__dirname, 'src/shared/'),
            Icons: path.resolve(__dirname, 'src/icons'),
            Images: path.resolve(__dirname, 'src/images'),
            Api: path.resolve(__dirname, 'src/api'),
            Utils: path.resolve(__dirname, 'src/utils'),
            Redux: path.resolve(__dirname, 'src/redux'),
        }
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader',
                    options: {
                        limit: 8000, // Convert images < 8kb to base64 strings
                        name: 'images/[hash]-[name].[ext]'
                    }
                }]
            },
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            favicon: "./src/images/favicon-32x32.png",
            favicon2: "./src/images/favicon-16x16.png",
            favicon3: "./src/images/favicon-96x96.png",
            template: './src/index.html'
        })
    ],
    devServer: {
      historyApiFallback: true
    },
}
