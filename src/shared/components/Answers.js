import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import PropTypes from "prop-types";
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid'
import { withTranslation } from 'react-i18next';


function createData(type, questionnaire, date, createdBy) {
    return { type, questionnaire, date, createdBy };
};

const styles = theme => ({
    root: {
        minWidth: '80%',
        overflowX: 'auto',
      },
    table: {
      width: '100%',
    },
    moreBtn: {
      border: '1px solid #2E5BFF',
      fontSize: '0.7rem',
      width: '70%',
    },
    redCircle: {
      backgroundColor: 'red',
      borderRadius: '50%',
      height: '16px',
      width: '16px',
      marginLeft: '2em'
    }
  });

const rows = [
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
    createData('red', 'Heart check', '06.01.2019', 'John Williams'),
];

const Answers = (props) => {

    const { classes, t } = props;

    return (
        <Grid container justify="center">
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell></TableCell>
                            <TableCell>{t('questionnaire')}</TableCell>
                            <TableCell>{t('date')}</TableCell>
                            <TableCell>{t('createdBy')}</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                    {rows.map((row, i) => (
                        <TableRow key={i}>
                            <TableCell><div className={classes[`${row.type}Circle`]} /></TableCell>
                            <TableCell>{row.questionnaire}</TableCell>
                            <TableCell>{row.date}</TableCell>
                            <TableCell>{row.createdBy}</TableCell>
                            <TableCell>
                                <Button className={classes.moreBtn}>{t('more')}</Button>
                            </TableCell>
                        </TableRow>
                    ))}
                    </TableBody>   
                </Table>
            </Paper>
        </Grid>
    );
};

Answers.propTypes = {
    classes: PropTypes.object.isRequired
  }
  

export default withStyles(styles, { withTheme: true })(withTranslation()(Answers));
