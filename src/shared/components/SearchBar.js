
import React from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Paper from "@material-ui/core/Paper";
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { DebounceInput } from 'react-debounce-input';

const useStyles = makeStyles(theme => ({
  searchBar: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    padding: '0.5em',
    '&:hover': {
      backgroundColor: '#fafafa',
    },
    '&:focus': {
      backgroundColor: '#fafafa',
    },
    '& svg': {
      marginRight: '1em'
    },
  },
  searchInput: {
    width: '100%',
    background: 'none',
    backgroundColor: 'inherit',
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    fontSize: '1.1rem',
    border: 'none',
    minHeight: '2.5rem',
    '&:focus': {
      outline: 'none'
    }
  },
}));



function SearchBar({ value, onChange, onSubmit, ...props }) {
  const classes = useStyles();

  const handleSubmit = event => {
    if (event.key === 'Enter') {
      onSubmit();
    }
  }

  return (
    <Paper component="div" className={classes.searchBar}>
      <SearchIcon />
      <DebounceInput minLength={1} debounceTimeout={300} className={classes.searchInput} value={value} onChange={onChange} onKeyPress={handleSubmit}/>
    </Paper>
  );
}

export default SearchBar;