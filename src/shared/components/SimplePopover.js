import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles(theme => ({
  typography: {
    padding: theme.spacing(2)
  }
}));

export default function SimplePopover({ handleClose, target, ...rest }) {
  const classes = useStyles();

  const open = Boolean(target);

  return (
    <div>
      <Popover
        open={open}
        anchorEl={target}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right"
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left"
        }}
      >
        <Typography component="div" className={classes.typography}>
          
          {rest.children}
        </Typography>
      </Popover>
    </div>
  );
}
