import React from 'react';
import UserAvatar_ico from '../../icons/UserAvatar.svg'
import SVG from 'react-inlinesvg';
import './UserBlock.css'
import { NavLink } from 'react-router-dom'
import {
  Grid,
  Button,
  Divider,
  IconButton,
} from '@material-ui/core';
import { withTranslation } from 'react-i18next';
import ButtonLink from 'Shared/components/ButtonLink';
import ClearIcon from '@material-ui/icons/Clear';


const UserBlock = ({ name, phoneNumber, email, id, t, gridProps, handleDelete, ...rest}) => (
  <Grid item className="userBlock" {...gridProps}>
    {id ? (
      <NavLink to={"/patients/" + id}>
        <Grid container alignItems="center" style={{height: '65%'}}>
          <Grid item xs={2} style={{padding: '5px 0 0 5px'}}>
            <SVG src={UserAvatar_ico} className="avatar"/>
          </Grid>
          <Grid item xs={10} className="patientInfo" style={{paddingLeft: '1em'}}>
            <span className="patientName">{name}</span>
            <br/>
            <span className="phoneNumber">{t('phoneNumber')}: {phoneNumber}</span>
          </Grid>
        </Grid>
      </NavLink>
    ):
    (
      <Grid container alignItems="center" style={{height: '100%'}}>
        <Grid item xs={2} style={{padding: '0 0 0 5px'}}>
          <SVG src={UserAvatar_ico} className="avatar"/>
        </Grid>
        <Grid item xs={8} className="patientInfo" style={{paddingLeft: '1em'}}>
          <span className="patientName">{name}</span>
          <br/>
          <span className="phoneNumber">{t('email')}: {email}</span>
        </Grid>
        {handleDelete && 
          <Grid item container xs={2} alignItems="flex-start" justify="flex-end" style={{height: '100%'}}>
            <IconButton onClick={handleDelete}>
              <ClearIcon fontSize="small"/>
            </IconButton>
          </Grid>
        }
      </Grid>
    )}
    {id && (
      <>
        <Divider />
        <Grid container justify="flex-end" alignItems="center" style={{height: '35%'}}>
            <Button component={ButtonLink} to={`/patients/${id}/calendar`} className="addQuestionnaire" style={{height: '100%', padding: '6px'}} >
              <i className="material-icons">add</i>
              {t('questionnaireAdd')}
            </Button>
        </Grid>
      </>
    )}
  </Grid>
)



export default withTranslation()(UserBlock)