import React from 'react';
import {
  Grid,
  CircularProgress
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
const styles = {
  container: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: 9999,
    background: 'rgba(0, 0, 0, 0.35)',
  },
  normal:{
    height: "100%",
    width: '100%'
  }
}
export default function PendingScreen({absolute}) {
  const { t } = useTranslation();
  return (
    <Grid container alignItems="center" justify="center" style={styles.container}>
      <h1 style={{marginRight: '1em', color: 'white'}}>{t('loadingRequest')}</h1>
      <CircularProgress style = {{color: 'white' }}/>
    </Grid>
  )
}
