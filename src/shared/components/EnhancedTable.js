import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import EnhancedTableHead from 'Shared/components/EnhancedTableHead';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing(3)
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2)
  },
  table: {
    minWidth: 750
  },
  tableWrapper: {
    maxHeight: 600,
    overflow: 'auto',
  },
}));

export default function EnhancedTable({data, handleOrder, handlePerPage, handlePage, count, headRows, initialOrderBy, defaultRowsPerPage, ...props}) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [order, setOrder] = useState(initialOrderBy && initialOrderBy.split(' ')[1] || '');
  const [orderBy, setOrderBy] = useState(initialOrderBy && initialOrderBy.split(' ')[0] || '');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);

  function handleRequestSort(event, property) {
    const isDesc = orderBy === property && order === "desc";
    const nextOrder = isDesc ? "asc" : "desc";
    setOrder(nextOrder);
    setOrderBy(property);
    handleOrder({ order: nextOrder, orderBy: property })
  }

  function handleChangePage(event, newPage) {
    setPage(newPage);
    handlePage(newPage);
  };

  useEffect(() => {
    if (count <= (rowsPerPage * page)) {
      handleChangePage(null, 0);
    }
  }, [count]);

  useEffect(() => {
    handleOrder({order, orderBy});
    console.log('initial', initialOrderBy)
    let initalRowsPerPage = defaultRowsPerPage || rowsPerPage;
    setRowsPerPage(initalRowsPerPage);
    handlePerPage(initalRowsPerPage);
    
  }, []);

  function handleChangeRowsPerPage(event) {
    let value = +event.target.value;
    setRowsPerPage(value);
    handlePerPage(value);
    setPage(0);
    handlePage(0);
  }

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, count - page * rowsPerPage);

  return (
    <Paper className={classes.paper}>
      <div className={classes.tableWrapper}>
        <Table className={classes.table} stickyHeader aria-label="sticky table">
          <EnhancedTableHead
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            headRows={headRows}
          />
          <TableBody>
            {props.children}
            {emptyRows > 0 && (
              <TableRow style={{ height: 49 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </div>
      <TablePagination
        rowsPerPageOptions={[5, 10, 20]}
        component="div"
        count={count}
        rowsPerPage={rowsPerPage}
        labelRowsPerPage={t('pageSize')}
        page={page}
        backIconButtonProps={{
          "aria-label": "previous page"
        }}
        nextIconButtonProps={{
          "aria-label": "next page"
        }}
        labelDisplayedRows={({from, to, count}) => `${from}-${to} z ${count}`}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
};