import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { getAlertTypeIdByLabel, getAlertTypeLabelById } from 'Utils/Dictionary';
import { useTranslation } from 'react-i18next';

const useStyles = makeStyles(theme => ({
	alarmIcon: {
		marginRight: '0.3em',
		borderRadius: '50%',
		minHeight: '12px',
		minWidth: '12px',
	},
	alert: {
		border: '1px solid gray',
		padding: '1px 6px',
		borderRadius: '12px',
		display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: '100px'
	},

}))




export default function Alert({ idAlert }) {
	const { t } = useTranslation();
	const classes = useStyles();
  let color = getAlertTypeLabelById(idAlert);

  if (color === 'No alert') {
    return null;
  }
  
	return (
		<div className={classes.alert}>
			<div className={classes.alarmIcon} style={{ backgroundColor: `${color}` }} />
			{t('alertAlarming')}
		</div>
	)
}
