import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import AssignmentIcon from '@material-ui/icons/Assignment'
import Button from '@material-ui/core/Button'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next';
import ButtonLink from 'Shared/components/ButtonLink';

const useStyles = makeStyles(theme => ({
	paper: {
    minHeight: '250px',
		width: '100%',
		border: '1px solid #3B86FF',
	},
	selectedPaper: {
    minHeight: '250px',
		width: '100%',
		border: '1px solid green',
	},
	icon: {
		height: 46,
		width: 46,
	},
	header: {
		paddingLeft: '1em',
		height: '20%',
		backgroundColor: '#3B86FF',
		color: 'white',
	},
	selectedHeader: {
		paddingLeft: '1em',
		height: '20%',
		backgroundColor: 'green',
		color: 'white',
	},
	questionnaireName: {
		width: '80%',
		marginLeft: '1em',
		display: 'flex',
		flexDirection: 'column',
	},
	primarySpan: {
		textOverflow: 'ellipsis',
		overflow: 'hidden',
		whiteSpace: 'nowrap',
		fontSize: '1.1em',
		fontWeight: '500',
	},
	secondarySpan: {
		fontSize: '0.8em',
	},
	questions: {
		height: '60%',
		padding: '1em'
	},
	buttons: {
		height: '20%',
		padding: '0.5em',
	},
	questionName: {
		fontWeight: '500',
		color: '#4D4F5C',
		fontSize: '1.1em'
	},
	questionType: {
		fontSize: '0.8em',
		color: '#A5A4BF',
	},
	moreButton: {
		color: 'var(--main-blue-color)'
	}
}))



export default function QuestionnaireCard({ questionnaire, type, handleAdd }) {
	const classes = useStyles();
	const { title, questions, idQuestionnaire } = questionnaire;
	const { t } = useTranslation();
  const renderQuestions = questions ? questions.length < 4 ? questions : questions.slice(0, 4) : [];
  const isSelected = type === 'selected';
  const mode = type === 'unassigned' ? 'edit' : 'view';
  
	return (
		<Paper className={isSelected ? classes.selectedPaper : classes.paper}>
			<Grid container 
				direction="row" 
				justify="flex-start" 
				className={isSelected ? classes.selectedHeader : classes.header}
				alignItems="center"
				>
          <Grid item xs={2}>
            <AssignmentIcon className={classes.icon}/>
          </Grid>
          <Grid item xs={10}>
            <div className={classes.questionnaireName}>
              <span className={classes.primarySpan}>{title}</span>
              <span className={classes.secondarySpan}>{t(`${type}`)}</span>
            </div>
          </Grid>
			</Grid>
			<Grid container className={classes.questions}>
        <Link 
        to={`/questionnaires/${idQuestionnaire}/${mode}`} 
        style={{width: '100%'}}
        >
					{renderQuestions && renderQuestions.map((q, i) => (
						<Grid item container key={i} xs={12} justify="flex-start" direction="column">
							<span className={classes.questionName}>{q.questionTitle}</span>
							<span className={classes.questionType}>{t(q.questionType)}</span>
						</Grid>
					))}
				</Link>
			</Grid>
			<Grid container justify={"flex-end" } className={classes.buttons}>
				{!!handleAdd ? 
					<Button size="small" className={classes.moreButton} onClick={() => handleAdd(idQuestionnaire)}>{t('add')}</Button>
					:
          <Button 
            size="small" 
            component={ButtonLink} 
            to={`/questionnaires/${idQuestionnaire}/${mode}`} 
            className={classes.moreButton}
          >
            {t('more')}
          </Button>
				}
			</Grid>
		</Paper>
	)
}