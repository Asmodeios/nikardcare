import React from 'react';
import {
  Grid,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';

export default function ErrorPage(props) {
  const { t } = useTranslation();
  return (
    <Grid container alignItems="center" justify="center" style={{width: '100%', height: '100%'}}>
      <h1 style={{marginRight: '1em'}}>{t('errorMessage')}
        <span role="img" aria-label="error" style={{marginLeft: '0.5em'}}>⚠️</span>
      </h1>
      <h2 style={{color: 'red'}}>{t('errorResponseText')}: {props.error}</h2>
    </Grid>
  )
}