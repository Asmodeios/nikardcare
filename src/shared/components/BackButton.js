import React from 'react';
import { Button } from '@material-ui/core'
import { withRouter } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { useTranslation } from 'react-i18next';

const BackButton = ({ history, to, title }) => {
  const { t } = useTranslation();
  const handleClick = () => {
    if (to) {
      history.push(to);
    } else {
      history.goBack()
    }
  }
	return (
		<Button 
			onClick={handleClick}
      color="secondary" 
      variant="contained" 
			startIcon={<ArrowBackIosIcon />}
		>
			{title ? title : t('back')}
		</Button>
	)
};


export default withRouter(BackButton);
