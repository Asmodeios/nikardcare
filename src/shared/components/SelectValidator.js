import React from 'react';
import { ValidatorComponent } from 'react-material-ui-form-validator';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';

class SelectValidator extends ValidatorComponent {


  render() {
      const { errorMessages, validators, requiredError, validatorListener, variant, label, formStyles, ...rest } = this.props;
      const { isValid } = this.state;

      return (
          <FormControl variant={variant} style={{width: '100%'}} error={!isValid} className={formStyles} >
              <InputLabel>
                {label}
              </InputLabel>
              <Select
                {...rest}
              >
                {this.props.children}
              </Select>
              {this.errorText()}
          </FormControl>
      );
  }

  errorText() {
      const { isValid } = this.state;

      if (isValid) {
          return null;
      }

      return (
          <FormHelperText>
              {this.getErrorMessage()}
          </FormHelperText>
      );
  }
}

export default SelectValidator;
