import React, {Component} from 'react';
import Routes from '../routes/Routes'
import { BrowserRouter as Router} from 'react-router-dom'
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import {
  loginSuccess,
} from 'Redux/modules/authorization/actions';
import { connect } from 'react-redux';
import LocalStorageService from 'Utils/LocalStorageService';
import { parseJwt } from 'Utils/tokenParser';
import '../styles/App.css';

const localStorageService = LocalStorageService.getService();

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#ffffff',
      red: 'red'
    }
  },
  typography: {
    // Use the system font.
    fontFamily: `'Source Sans Pro', sans-serif`,
  },
});

class App extends Component {

  componentDidMount() {
    const token = localStorageService.getAccessToken();
    if (token) {
      let parsedToken = parseJwt(token);
      this.props.auth(parsedToken);
    }
  }

  render() {
    return (
    <ThemeProvider theme={theme}>
      <Router>
        <Routes />
      </Router>
    </ThemeProvider>

    );
  }
}

const mapDispatchToProps = dispatch => ({
  auth: (user) => dispatch(loginSuccess(user))
});

export default connect(null, mapDispatchToProps)(App)
