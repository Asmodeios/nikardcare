import React from 'react';
import Overview_ico from 'Icons/Overview_ico.svg';
import Patients_ico from 'Icons/Patients_ico.svg';
import Groups_ico from 'Icons/Groups_ico.svg';
import AlarmsHistory_ico from 'Icons/AlarmsHistory_ico.svg';
import Questionnaire_ico from 'Icons/Questionnaires_ico.svg';
import './styles/AppNavigation.css'
import SVG from 'react-inlinesvg';
import { NavLink } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import ButtonLink from 'Shared/components/ButtonLink';
import { Button, Link } from '@material-ui/core';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows';
import { connect } from 'react-redux'; 

class AppNavigation extends React.Component {

  render(){
    const { t, isAdmin } = this.props;

    return (
      <div className='nav-container' style={{display: 'flex', flexDirection: 'column', justifyContent: "space-between"}}>
        <ul className='navbar'>
          <li>
            <NavLink className='NavElement' activeClassName='SelectedElement' to='/patients'>
              <SVG src={Patients_ico} svgclassName ='elementIcon'/>
              <span className = 'NavText'>{t('patients')}</span>
            </NavLink>
          </li>
          <li>
            <NavLink className='NavElement' activeClassName='SelectedElement' to='/groups'>
              <SVG src={Groups_ico} svgclassName ='elementIcon'/>
              <span className = 'NavText'>{t('groups')}</span>
            </NavLink>
          </li>
          <li>
            <NavLink className='NavElement' activeClassName='SelectedElement' to='/questionnaires'>
              <SVG src={Questionnaire_ico} svgclassName ='elementIcon'/>
              <span className = 'NavText'>{t('questionnaires')}</span>
            </NavLink>
          </li>
          {isAdmin && (
              <li>
              <NavLink className='NavElement' activeClassName='SelectedElement' to='/administration'>
                <DesktopWindowsIcon style={{height: 25, width: 25}}/>
                <span className = 'NavText'>{t('administration')}</span>
              </NavLink>
            </li>
          )}
      </ul>
    </div>
    )
  }
}
const mapStateToProps = state => ({
  isAdmin: state.authorization.IsAdmin,
});

export default connect(mapStateToProps, null)(withTranslation()(AppNavigation));
