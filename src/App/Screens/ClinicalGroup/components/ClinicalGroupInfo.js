import React, { useState, useEffect } from 'react';

import {
  Paper,
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  LinearProgress,
  Tabs,
  Tab,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import UserBlock from 'Shared/components/UserBlock';
import { 
  getClinicalGroup,
  addPatientsToClinicalGroup,
  deleteClinicalGroup, 
} from "Api/ClinicalGroupApi";
import { a11yProps , TabPanel } from 'Shared/components/TabPanel';
import ButtonLink from 'Shared/components/ButtonLink';
import BackButton from 'Shared/components/BackButton';
import QuestionnaireCard from 'Shared/components/QuestionnaireCard';
import Overview from '../../Overview';
import ClinicalGroupAnswers from './ClinicalGroupAnswers';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';

const useStyles = makeStyles({
  tabPanel: {
    height: '100%',
  },
  cardContainer: {
    height: '100%',
  },
  paperContainer: {
		padding: '0.5em 0',
		height: '85%',
  },
  paper: {
		padding: '2em',
		overflowY: 'auto',
		height: '100%'
	},
});

const ClinicalGroupInfo = (props) => {
  const { t } = useTranslation();
  const [tab, setTab] = useState(0);
  const idClinicalGroup = props.match.params.idGroup;
  const classes = useStyles();
  const [clinicalGroup, setClinicalGroup] = useState(null);
  const [deleteOpen, setDeleteOpen] = useState(false);

  // load initial content
  useEffect(() => {
    loadClinicalGroup();
  }, []);

  const loadClinicalGroup = async () => {
    const clinicalGroup = await getClinicalGroup(idClinicalGroup);
    setClinicalGroup(clinicalGroup.data);
  };

  const handleTabChange = (event, newTab) => {
    setTab(newTab);
  }

  const handleDelete = async () => {
    const response = await deleteClinicalGroup(idClinicalGroup);
    if (response && response.status >= 200 && response.status <= 299) {
      props.history.push('/groups')
    };
  }

  if (!clinicalGroup) {
    return null;
  }

  const { questionnaires, patients, answers, alerts } = clinicalGroup;

  return (
    <React.Fragment>
      <Grid container justify="space-between" direction="row" style={{height: '10%'}}>
        <Grid item xs={12}>
          <h1>{t('groupName')}: {clinicalGroup.clinicalGroupName}</h1>
        </Grid>
        <Grid item container xs={12} alignItems="center">
          <Grid item xs={10}>
            <Tabs
              value={tab}
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary"
              aria-label="questionnaires-tabs"
              >
              <Tab label={t('patients')} {...a11yProps(0)} />
              <Tab label={t('questionnaires')} {...a11yProps(1)} />
              <Tab label={t('answers')} {...a11yProps(2)} />
              <Tab label={t('alerts')} {...a11yProps(3)} />
            </Tabs>
          </Grid>
          <Grid item container justify="flex-end" xs={2}>
            {tab === 1 && (
              <Button
              component={ButtonLink}
              variant="contained"
              color="primary"
              to={`/groups/${idClinicalGroup}/questionnaires`}
            >
              {t('questionnaireAdd')}
            </Button>
            )
          }
          </Grid>
        </Grid>
      </Grid>
      <Typography component="div" className={classes.paperContainer}>
        <Paper className={classes.paper} >
          <TabPanel value={tab} index={1} className={classes.tabPanel}>
            <Grid container spacing={4} alignContent="flex-start" className={classes.cardContainer}>
              {questionnaires && questionnaires.length > 0 && questionnaires.map((questionnaire, i) => (
                <Grid lg={3} key={i} item container justify="center" >
                  <QuestionnaireCard questionnaire={questionnaire} type="group" handleSettings={() => {}}/>
                </Grid>
              ))}
            </Grid>
          </TabPanel>
          <TabPanel value={tab} index={0} className={classes.tabPanel}>
            <Grid item xs={12} container>
              {patients && patients.length > 0 && patients.map((p, i) => (
                <UserBlock name={p.patientName} phoneNumber={p.phoneNumber} id={p.idPatient} key={p.idPatient} gridProps={{xs: 6, lg: 4, xl: 4}}/>
              ))}
            </Grid>
          </TabPanel>
          <TabPanel value={tab} index={3} className={classes.tabPanel}>
            <Overview alerted={alerts.alertedAppointments} missed={alerts.missedAppointments}  {...props}/>
          </TabPanel>
          <TabPanel value={tab} index={2} className={classes.tabPanel}>
            <ClinicalGroupAnswers rows={answers} {...props}/>
          </TabPanel>
        </Paper>
      </Typography>
      <Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
        <BackButton />
        {props.isAdmin && !clinicalGroup.hasPatients &&
          <Button 
            variant="contained" 
            style={{color: 'white', backgroundColor: 'red'}}
            endIcon={<DeleteIcon />}
            onClick={() => setDeleteOpen(true)}
          >
            {t('delete')}
          </Button>
        }
      </Grid>
      <Dialog
        maxWidth="sm"
        fullWidth
        open={deleteOpen}
        onClose={() => setDeleteOpen(false)}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>
          {`${t('deleteMessage')} "${clinicalGroup.clinicalGroupName}"?`}
        </DialogTitle>
        <DialogActions>
          <Button onClick={() => setDeleteOpen(false)} color="secondary" variant="contained">
            {t('cancel')}
          </Button>
          <Button 
            variant="contained" 
            style={{color: 'white', backgroundColor: 'red'}}
            endIcon={<DeleteIcon />}
            onClick={handleDelete}
          >
            {t('delete')}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  isAdmin: state.authorization.IsAdmin,
});

export default connect(mapStateToProps, null)(ClinicalGroupInfo) ;
