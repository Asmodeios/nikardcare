import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom'
import {
  Table,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  Drawer,
  Button,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  LinearProgress,
  Icon,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import ClinicalGroupModal from './ClinicalGroupModal';
import { ClinicalGroupListFilter } from './ClinicalGroupListFilter';
import { getClinicalGroups, getClinicalGroupsDictiponary, addClinicalGroup } from "Api/ClinicalGroupApi";
import { format } from 'date-fns'
import EnhancedTable from 'Shared/components/EnhancedTable';
import { buildQuery, buildCountQuery } from 'Utils/Query';

const MAX_PAGE_SIZE = 20;
const MIN_PAGE_SIZE = 5;

const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    width: '100%',
    minWidth: '50em',
  },
  tableCell: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
});


const ClinicalGroupList = (props) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [groups, setGroups] = React.useState([]);
  const [isModalOpen, setIsModalOpen] = React.useState(false);
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [queryParams, setQueryParams] = useState({
    orderby: '',
    pagination: {
      page: 1,
      perPage: 5,
    }
  });

  const handleOrderChange = ({order, orderBy}) => {
    console.log(order,orderBy);
    setQueryParams(oldValues => ({
      ...oldValues, 
      orderby: `${orderBy} ${order}`
    }));
    
  };

  const handlePerPage = value => {
    setQueryParams(oldValues => ({
      ...oldValues, 
      pagination: {
        ...oldValues.pagination,
        perPage: value
      }
    }));
  }

  const handlePage = value => {
    setQueryParams(oldValues => ({
      ...oldValues, 
      pagination: {
        ...oldValues.pagination,
        page: value + 1
      }
    }));
  }
  async function getCount(params) {
    const response = await getClinicalGroups(params);
    if (response.data.length > 0) {
      const groupsCount = response.data[0].count || 0;
      setCount(groupsCount)
    } else {
      setCount(0);
    };
  };

  useEffect(() => {
    getCount(buildCountQuery(queryParams.searchBy))
  }, [queryParams.searchBy]);

  useEffect(() => {
    fetchGroups(buildQuery(queryParams))
  }, [queryParams]);


  async function fetchGroups(params) {
    let response = await getClinicalGroups(params);
    if (response.status >= 200 && response.status < 300) {
      setData(response.data);
    }
  };

  const handleAddClinicalGroup = async (clinicalGroup) => {
    const response = await addClinicalGroup(clinicalGroup);
    if (response.status >= 200 && response.status < 300) {
      setIsModalOpen(false);
      fetchGroups(buildQuery(queryParams))
    }
  };


  const handleRowClick = (row) => {
    props.history.push(`/groups/${row.idClinicalGroup}`);
  }

  const headRows = [
    { id: "clinicalGroupName", disablePadding: false, label: t('groupName') },
    { id: "patientCount", disablePadding: false, label: t('numberOfPatients') },
    { id: "createdBy", disablePadding: false, label: t('createdBy') },
    { id: "createdAt", disablePadding: false, label: t('createdAt') },
    { id: "description", disablePadding: false, label: t('description') },
  ];
  

  return (
      <>
        <Typography component="div" style={{height: '5%'}}>
          <h1>{t('listOfGroups')}</h1>
        </Typography>
        <Grid container direction="row" justify="flex-end" className="clinicalgrouplist-buttons" style={{height: '5%'}}>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={() => setIsModalOpen(true)}
            >
              <i className="material-icons">group_add</i>{t('addClinicalGroup')}
            </Button>
          </Grid>
          <Dialog
            maxWidth="lg"
            fullWidth={true}
            open={isModalOpen}
            onClose={() => setIsModalOpen(false)}
            aria-labelledby="max-width-dialog-title"
          >
            <DialogTitle id="max-width-dialog-title">{t('groupData')}</DialogTitle>
            <DialogContent style={{ overflowY: 'hidden' }}>
              <ClinicalGroupModal handleAddClinicalGroup={handleAddClinicalGroup} />
            </DialogContent>
          </Dialog>
        </Grid>
        <Typography component="div" style={{height: '90%'}}>
          <EnhancedTable 
            data={data} 
            handleOrder={handleOrderChange} 
            handlePerPage={handlePerPage} 
            count={count} 
            handlePage={handlePage}
            initialOrderBy="patientCount desc"
            headRows={headRows}
          >
            {data
              .map((row, index) => {
                let page = queryParams.pagination.page - 1;
                let perPage = queryParams.pagination.perPage;
                return (
                  <TableRow hover tabIndex={-1} key={index}>
                    <TableCell onClick={() => handleRowClick(row)}>
                      {(index + 1) + (page * perPage)}
                    </TableCell>
                    <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                      {row.clinicalGroupName}
                    </TableCell>
                    <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                      {row.patientCount}
                    </TableCell>
                    <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                      {row.createdBy}
                    </TableCell>
                    <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                      {format(new Date(row.createdAt), 'dd/MM/yyyy')}
                    </TableCell>
                    <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                      {row.description}
                    </TableCell>
                  </TableRow>
                );
              })}
          </EnhancedTable>
        </Typography>
      </>
    );
};

export default ClinicalGroupList;
