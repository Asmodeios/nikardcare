import React, { useState, useEffect } from 'react'
import {
  Grid,
  Button,
	Paper,
  Typography,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Checkbox,
  FormControlLabel,
  InputAdornment,
  TextField,
  FormControl,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import QuestionnaireCard from 'Shared/components/QuestionnaireCard';
import { connect } from 'react-redux';
import { getQuestionnaires } from 'Api/QuestionnaireApi';
import { assignQuestionnaire } from 'Api/ClinicalGroupApi';
import {
  clearEditQuestion,
  clearEditQuestions
} from 'Redux/modules/questionnaires/actions';
import PendingScreen from 'Shared/components/PendingScreen';
import BackButton from 'Shared/components/BackButton';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  DateTimePicker ,
} from '@material-ui/pickers';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const useStyles = makeStyles(theme => ({
  container: {
    overflowY: 'auto',
    marginTop: '0.5em',
    padding: '2em',
    height: '90%',
    boxSizing: 'border-box',
  },
  tabPanel: {
    height: '100%'
  },
  cardContainer: {
    height: '100%'
	},
	paperContainer: {
		padding: '0.5em 0',
		height: '90%',
  },
  paper: {
		padding: '2em',
		overflowY: 'auto',
		height: '100%'
	},
}));


function ClinicalGroupQuestionnaires(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [questionnairesState, setQuestionnaresState] = useState(props);
  const [open, setOpen] = useState(false);
  const [interval, setInterval] = useState(false);
  const [params, setParams] = useState({
    idClinicalGroup: 0,
    idQuestionnaire: 0,
  });

  const formik = useFormik({
    initialValues: {
      startAfter: 1,
      availableFor: 1,
      intervalDays: 1,
      numberOfTimes: 1,
      startDate: new Date(),
    },
    validationSchema: Yup.object({
      startAfter: Yup.number()
      .required(t('requiredField'))
      .min(1, t('min1')).max(100, t('max100')),
      intervalDays: Yup.number()
      .required(t('requiredField'))
      .min(1, t('min1')).max(100, t('max100')),
      availableFor: Yup.number()
      .required(t('requiredField'))
      .min(1, t('min1')).max(100, t('max100'))
      .test('smaller', t('availableForLessThanIntervalError'), (value) => {
        if (!interval) return true;
        return value <= intervalDays.value;
      }),
      numberOfTimes: Yup.number()
      .required(t('requiredField'))
      .min(1, t('min1')).max(100, t('max100')),
    }),
    onSubmit: async (values) => {
      let addQuestionnaire = values;
      if (!interval) {
        addQuestionnaire = {
          ...addQuestionnaire,
          intervalDays: 0,
          numberOfTimes: 1,
        }
      }
      addQuestionnaire.idClinicalGroup = params.idClinicalGroup;
      addQuestionnaire.idQuestionnaire = params.idQuestionnaire;
      const response = await assignQuestionnaire(addQuestionnaire);
      if (response && response.status >= 200 && response.status <= 299) {
        props.history.goBack();
      }
    },
  });

  useEffect(() => {
    setQuestionnaresState(props);
  }, [props])

  useEffect(() => {
    props.fetchQuestionnaires(4);
    props.clearEditQuestion();
    props.clearEditQuestions();
    setParams(oldValues => ({
      ...oldValues,
      idClinicalGroup: +props.match.params.idGroup,
    }))
  }, [])

  const handleClickOpen = (idQuestionnaire) => {
    setParams(oldValues => ({
      ...oldValues,
      idQuestionnaire: idQuestionnaire,
    }));
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
	};

  const { questionnaires, isPending } = questionnairesState;
  const { 
    touched,
    handleChange, 
    handleBlur,
    values,
    errors,
    handleSubmit,
    setFieldValue
  } = formik;

  return (
    <>
      <Grid container style={{height: '5%'}}>
        <Grid item xs={12}>
          <h1>{t('questionnaires')}</h1>
        </Grid>
      </Grid>
      <Typography component="div" className={classes.paperContainer}>
				{isPending ? <PendingScreen /> : (
					<Paper className={classes.paper}>
						<Grid container spacing={4} alignContent="flex-start" className={classes.cardContainer}>
							{questionnaires && questionnaires.map((questionnaire, i) => (
							<Grid lg={3} key={i} item container justify="center" >
									<QuestionnaireCard questionnaire={questionnaire} type="unassigned" handleAdd={handleClickOpen}/>
							</Grid>
							))}
						</Grid>
					</Paper>
				)}
			</Typography>
			<Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
        <BackButton />
      </Grid>
      <Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
				fullWidth
			>
        <form onSubmit={handleSubmit} noValidate>
          <DialogTitle id="alert-dialog-title" style={{textAlign: 'center'}}>{t('questionnaireAdd')}</DialogTitle>
          <DialogContent>
            <Grid container alignItems="center" style={{marginBottom: '0.5em'}}>
              <Grid item xs={6}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <DateTimePicker
                    required
                    variant="inline"
                    ampm={false}
                    format="dd/MM/yyyy HH:mm"
                    id="startDate"
                    name="startDate"
                    label={t('startDate')}
                    value={values.startDate}
                    onChange={(date) => setFieldValue('startDate', date)}
                    onBlur={handleBlur}
                    autoOk
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={6}>
                <FormControlLabel
                  control={
                    <Checkbox checked={interval} color="primary" onChange={(event) => setInterval(event.target.checked)} />
                  }
                  label={t('interval')}
                />
              </Grid>
            </Grid>
              <Grid container alignItems="center" spacing={2}>
                <Grid item xs={3}>
                  <TextField
                    fullWidth
                    label={t("availableFor")}
                    id="availableFor"
                    name="availableFor"
                    type="number"
                    value={values.availableFor}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.availableFor && !!errors.availableFor}
                    helperText={touched.availableFor && errors.availableFor}
                    InputProps={{
                      inputProps: { min: 1, max: 100 },
                      startAdornment: <InputAdornment position="start">{t('days')}</InputAdornment>,
                    }}
                  />
                </Grid>
                <Grid item xs={3}>
                  <TextField
                    fullWidth
                    label={t("startAfter")}
                    id="startAfter"
                    name="startAfter"
                    type="number"
                    value={values.startAfter}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={touched.startAfter && !!errors.startAfter}
                    helperText={touched.startAfter && errors.startAfter}
                    InputProps={{
                      inputProps: { min: 1, max: 100 },
                      startAdornment: <InputAdornment position="start">{t('days')}</InputAdornment>,
                    }}
                  />
                </Grid>
              {interval && (
                <>
                  <Grid item xs={3}>
                    <TextField
                      fullWidth
                      label={t("intervalDays")}
                      id="intervalDays"
                      name="intervalDays"
                      type="number"
                      value={values.intervalDays}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={touched.intervalDays && !!errors.intervalDays}
                      helperText={touched.intervalDays && errors.intervalDays}
                      InputProps={{
                        inputProps: { min: 1, max: 100 },
                        startAdornment: <InputAdornment position="start">{t('days')}</InputAdornment>,
                      }}
                    />
                  </Grid>
                  <Grid item xs={3}>
                    <TextField
                      fullWidth
                      label={t("numberOfTimes")}
                      id="numberOfTimes"
                      name="numberOfTimes"
                      type="number"
                      value={values.numberOfTimes}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      error={touched.numberOfTimes && !!errors.numberOfTimes}
                      helperText={touched.numberOfTimes && errors.numberOfTimes}
                      InputProps={{
                        inputProps: { min: 1, max: 100 },
                      }}
                    />
                  </Grid>
                </>
              )}
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              {t('cancel')}
            </Button>
            <Button color="primary" type="submit">
              {t('save')}
            </Button>
          </DialogActions>
        </form>
      </Dialog>

    </>
  )
}

const mapStateToProps = state => ({
	questionnaires: state.questionnaires.questionnaires.list,
	isFetched: state.questionnaires.questionnaires.isFetched,
	isPending: state.questionnaires.questionnaires.isPending,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchQuestionnaires: (questionnaireType) => dispatch(getQuestionnaires(questionnaireType)),
    clearEditQuestion: () => dispatch(clearEditQuestion()),
    clearEditQuestions: () => dispatch(clearEditQuestions()),
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(ClinicalGroupQuestionnaires);
