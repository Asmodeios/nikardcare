
import React from 'react';
import Grid from '@material-ui/core/Grid';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import DiagnosisApi from 'Api/DiagnosisApi';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from "@material-ui/core/FormLabel";
import InputLabel from '@material-ui/core/InputLabel';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { withTranslation } from 'react-i18next';
import FormHelperText from '@material-ui/core/FormHelperText';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const styles = theme => ({
  error: {
    color: 'red',
    fontSize: '0.8em',
    marginLeft: '15em',
  },
  radioGroup: {
    display: 'flex',
    flexDirection: 'row',
  },
  formControl: {
    marginBottom: '1em',
    minWidth: 350,
    maxWidth: 350,
  },
  saveBtn: {
    margin: '2em 9em',
    minWidth: 100,
  }

})

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

class ClinicalGroupModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clinicalGroupName: '',
      description: ''
    };
  }
  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    const clinicalGroup = {
      clinicalGroupName: this.state.clinicalGroupName, description: this.state.description
    };
    /* TODO: take doctor id from state */
    clinicalGroup.idDoctor = 1;
    this.props.handleAddClinicalGroup(clinicalGroup);
  };

  render() {
    const { classes, t } = this.props;
    return (
      <>
        <ValidatorForm onSubmit={this.handleSubmit}>
          <Grid container>
            <Grid item container justify="center" xs={6}>
              <FormControl className={classes.formControl}>
                <TextValidator
                  id="clinical-group-name"
                  name="clinicalGroupName"
                  label={t('groupName')}
                  errorMessages={[t('requiredField')]}
                  validators={['required']}
                  value={this.state.clinicalGroupName}
                  onChange={this.handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item container justify="center" xs={6}>
              <FormControl className={classes.formControl}>
                <TextField
                  id="description"
                  name="description"
                  label={t("description")}
                  onChange={this.handleChange}
                  value={this.state.description}
                />
              </FormControl>
            </Grid>
            <Grid item container justify="flex-end" xs={12}>
              <Button variant="contained" className="saveBtn" type="submit" color="primary" className={classes.saveBtn}>
                {t('save')}
                  </Button>
            </Grid>
          </Grid>
        </ValidatorForm>
      </>
    )
  }
}

ClinicalGroupModal.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(withTranslation()(ClinicalGroupModal))
