import { makeStyles } from "@material-ui/core/styles";
import Divider from '@material-ui/core/Divider';
import React, { useState, useEffect } from 'react';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import buildQuery from 'odata-query';
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(theme => ({
    list: {
        width: 300
    },
    h1: {
        textAlign: 'center'
    },
    filterRow: {
        padding: '3em 0.8em',
    },
    filterButton: {
        backgroundColor: '#2e5bff',
        width: '100%',
    },
    formControl: {
        width: '100%',
        marginBottom: '1em',
    },
    label: {
        fontFamily: 'var(--main-font-Rubik)'
    },
    form: {
        padding: '0 1em',
    }
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};


export const ClinicalGroupListFilter = ({ onFilterSubmit }) => {
    const {t} = useTranslation();
    const classes = useStyles();
    const [clinicalGroupName, setClinicalGroupName] = React.useState('');

    const handleChange = event => {
        setClinicalGroupName(event.target.value);
    };
    const handleApply = () => {
      console.log(clinicalGroupName)
      onFilterSubmit({ clinicalGroupName });
    } 

    return (
        <div className={classes.list}>
            <h1 className={classes.h1}>{t('filterGroups')}</h1>
            <Divider variant="middle" style={{ marginBottom: '1em' }} />
            <form autoComplete="off" className={classes.form} onSubmit={handleApply} >
                <FormControl className={classes.formControl}>
                    <TextField
                        id="clinical-group-name"
                        name="clinicalGroupName"
                        label={t("groupName")}
                        value={clinicalGroupName}
                        onChange={handleChange}
                        margin="normal"
                    />
                </FormControl>
            </form>
            <div className={classes.filterRow}>
                <Button variant="contained" color="primary" className={classes.filterButton} onClick={handleApply}>{t('applyFilter')}</Button>
            </div>
        </div>
    );
}