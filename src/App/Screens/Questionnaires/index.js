import React, { useState, useEffect } from 'react'
import {
  Grid,
  Button,
  Tabs,
  Tab,
  Paper,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import QuestionnaireCard from 'Shared/components/QuestionnaireCard';
import { connect } from 'react-redux';
import { getQuestionnaires } from 'Api/QuestionnaireApi';
import { 
  clearEditQuestion,
  clearEditQuestions,
  clearAddedQuestions,
} from 'Redux/modules/questionnaires/actions';
import PendingScreen from 'Shared/components/PendingScreen';
import { a11yProps , TabPanel } from 'Shared/components/TabPanel';
import ButtonLink from 'Shared/components/ButtonLink';



const useStyles = makeStyles(theme => ({
  container: {
    overflowY: 'auto',
    marginTop: '0.5em',
    padding: '2em',
    height: '90%',
    boxSizing: 'border-box',
  },
  tabPanel: {
    height: '100%'
  },
  cardContainer: {
    height: '100%'
  },
}));

function Questionnaires(props) {
  const classes = useStyles();
  const [tab, setTab] = useState(0);
  const [questionnairesState, setQuestionnaresState] = useState(props)
  const { t } = useTranslation();

  useEffect(() => {
    setQuestionnaresState(props);
  }, [props])

  useEffect(() => {
    props.fetchQuestionnaires(tab + 1);
    props.clearEditQuestion();
    props.clearEditQuestions();
    props.clearAddedQuestions();
  }, [])

  useEffect(() => {
    props.fetchQuestionnaires(tab + 1);
  }, [tab])

  const handleTabChange = (event, newTab) => {
    setTab(newTab);
    
  }

  const { questionnaires, isPending } = questionnairesState;

  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={12}>
          <h1>{t('questionnaires')}</h1>
        </Grid>
        <Grid container alignItems="center"> 
          <Grid item lg={10} xs={12}>
            <Tabs 
              value={tab} 
              onChange={handleTabChange}
              indicatorColor="primary"
              textColor="primary" 
              aria-label="questionnaires-tabs"
              >
              <Tab label={t('unassigned')} {...a11yProps(0)} />
              <Tab label={t('individual')} {...a11yProps(1)} />
              <Tab label={t('group')} {...a11yProps(2)} />
            </Tabs>
          </Grid>
          <Grid item container justify="flex-end" lg={2} xs={12}>
            <Button
              component={ButtonLink}
              variant="contained"
              color="primary"
              to="/questionnaires/AddQuestionnaire"
            >
              {t('questionnaireAdd')}
            </Button>
          </Grid>
        </Grid>
      </Grid>
        
      {isPending ? <PendingScreen /> : (
        <Paper className={classes.container}>
          <TabPanel value={tab} index={0} className={classes.tabPanel}>
            <Grid container spacing={4} alignContent="flex-start" className={classes.cardContainer}>
              {questionnaires && questionnaires.map((questionnaire, i) => (
                <Grid lg={3} key={i} item container justify="center" >
                  <QuestionnaireCard questionnaire={questionnaire} type="unassigned"/>
                </Grid>
              ))}
            </Grid>
          </TabPanel>
          <TabPanel value={tab} index={1} className={classes.tabPanel}>
          <Grid container spacing={4} alignContent="flex-start" className={classes.cardContainer}>
              {questionnaires && questionnaires.map((questionnaire, i) => (
                <Grid lg={3} key={i} item container justify="center" >
                  <QuestionnaireCard questionnaire={questionnaire} type="individual"/>
                </Grid>
              ))}
            </Grid>
          </TabPanel>
          <TabPanel value={tab} index={2} className={classes.tabPanel}>
          <Grid container spacing={4} alignContent="flex-start" className={classes.cardContainer}>
              {questionnaires && questionnaires.map((questionnaire, i) => (
                <Grid lg={3} key={i} item container justify="center" >
                  <QuestionnaireCard questionnaire={questionnaire} type="group"/>
                </Grid>
              ))}
            </Grid>
          </TabPanel>
        </Paper>
      )}
      
    </React.Fragment>
  )
}

const mapStateToProps = state => ({
	questionnaires: state.questionnaires.questionnaires.list,
	isFetched: state.questionnaires.questionnaires.isFetched,
	isPending: state.questionnaires.questionnaires.isPending,
})

const mapDispatchToProps = dispatch => {
  return {
    fetchQuestionnaires: (questionnaireType) => dispatch(getQuestionnaires(questionnaireType)),
    clearEditQuestion: () => dispatch(clearEditQuestion()),
    clearEditQuestions: () => dispatch(clearEditQuestions()),
    clearAddedQuestions: () => dispatch(clearAddedQuestions()),
  }
}



export default connect(mapStateToProps, mapDispatchToProps)(Questionnaires);
