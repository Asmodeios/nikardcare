import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ClearIcon from '@material-ui/icons/Clear';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import { getQuestionAlerts } from 'Api/QuestionApi';
import { getAlertTypeIdByLabel, getAlertTypeLabelById } from 'Utils/Dictionary';
import { connect } from 'react-redux';
import IconButton from '@material-ui/core/IconButton';
import { useTranslation } from 'react-i18next';
import Alert from 'Shared/components/Alert';

const useStyles = makeStyles(theme => ({
	question: {
		backgroundColor: '#3B86FF',
		color: 'white',
		borderRadius: '4px 4px 0 0',
	},
	questionName: {
		display: 'flex',
		flexBasis: '70%',
		flexDirection: 'column',
	},
	questionType: {
		flexBasis: '30%'
	},
	expandIcon: {
		color: 'white',
	},
	questionTitle: {
		wordBreak: 'break-word',
	},
	answerList: {
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
	},
	answer: {
    marginBottom: '0.3em',
	},
	buttons: {
		display: 'flex',
		justifyContent: 'flex-end',
	},
	panelDetails: {
		padding: '8px 24px',
	},
}))

const setButtonIcon = (buttonType) => {
	switch (buttonType) {
		case 'edit': 
			return <EditIcon />;
		case 'add':
			return <AddIcon />;
		case 'remove':
			return <ClearIcon />;
		default:
			return '';
	}
}

function QuestionExpansionBlock({ buttonType, handler, handleEdit, question, ...props}) {
	const classes = useStyles();
	const [expanded, setExpanded] = useState(false);
  const { t } = useTranslation();
  
	const handleQuestionChange = panel => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
	}

	const { questionType, idQuestion, questionTitle, choiceOptions, questionBoundaries, limit } = question;
	const isAdd = buttonType === 'add';
	const isRemove = buttonType === 'remove';
	const isPreview = buttonType === 'preview';



	return (
		<Grid container alignItems="center" style={{marginBottom: '0.5em'}}>
			{isAdd && 
				<Grid item xs={1}>
					<IconButton 
						onClick={() => handler(question)} 
						color="primary"
						style={{height: '100%'}}
						>
						{setButtonIcon(buttonType)}
					</IconButton>
				</Grid>
			}
			<Grid item xs={ isPreview ? 12 : 11 }>
				<ExpansionPanel expanded={expanded === `panel${idQuestion}`} 
					onChange={handleQuestionChange(`panel${idQuestion}`)} 
					style={{width: '100%'}}
					className={classes.questionBlock}
					>
					<ExpansionPanelSummary
						expandIcon={<ExpandMoreIcon className={classes.expandIcon} />}
						aria-controls={`panel${idQuestion}bh-content`}
						id={`panel${idQuestion}bh-header`}
						className={classes.question}
					>
						
						<Grid container alignItems="center">
							<Grid item container lg={8} direction="column">
								<span className={classes.questionTitle}>{questionTitle}</span>
							</Grid>
							<Grid item container justify="flex-end" lg={4}>
								<span>{t(questionType)}</span>
							</Grid>
						</Grid>
						
					</ExpansionPanelSummary>
					<ExpansionPanelDetails className={classes.panelDetails}>
						<Grid container direction="column">
							{(choiceOptions && choiceOptions.length > 0) && choiceOptions.map((option, i) => (
								<Grid item container alignItems="center" key={i} className={classes.answer}>
									<Grid item xs={10}>
										<span>{`${i + 1}. ${option.value}`}</span>
									</Grid>
									<Grid item xs={2}>
										{(option.idAlert !== 1 || !option.idAlert) && <Alert idAlert={option.idAlert}/>}	
									</Grid>
								</Grid>
							))}
							{(questionBoundaries && questionBoundaries.length > 0) && questionBoundaries.map((option, i) => (
								<Grid item container alignItems="center" key={i} className={classes.answer}>
									<Grid item xs={10}>
										<span>{`${i + 1}. ${option.lowerBoundary} - ${option.upperBoundary}`}</span>
									</Grid>
									<Grid item xs={2}>
										{(option.idAlert !== 1 || !option.idAlert) && <Alert idAlert={option.idAlert}/>}
									</Grid>
								</Grid>
							))}
							{(questionType && ['Open', 'File'].includes(questionType)) && 
								<div className={classes.answer}>
									<span>{questionType == 'Open' ? t('characterLimit') : t('fileLimit')} {limit}</span>
								</div>
							}
							<div className={classes.buttons}>
							{isRemove &&
								<Button 
									onClick={() => handleEdit(question)} 
									endIcon={setButtonIcon('edit')} 
									color="primary"
									>
									{t('edit')}
								</Button>
							}
							</div>
						</Grid>
					</ExpansionPanelDetails>
				</ExpansionPanel>
			</Grid>
			{isRemove && 
				<Grid item xs={1}>
					<IconButton 
						onClick={() => handler(question)} 
						color="primary"
						style={{height: '100%'}}
						>
						{setButtonIcon(buttonType)}
					</IconButton>
				</Grid>
			}
		</Grid>
	)
}



export default QuestionExpansionBlock;