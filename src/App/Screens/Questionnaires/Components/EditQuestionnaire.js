import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import QuestionExpansionBlock from './QuestionExpansionBlock';
import { Link } from 'react-router-dom';
import { getQuestions } from 'Api/QuestionApi';
import { connect } from 'react-redux';
import {
	addQuestionToQuestionnaire,
	removeQuestionFromQuestionnaire,
	getQuestionToEdit,
	clearEditQuestion,
} from 'Redux/modules/questionnaires/actions';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import SaveIcon from '@material-ui/icons/Save';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { addQuestionnaire, getQuestionnaire, editQuestionnaire } from 'Api/QuestionnaireApi';
import { getQuestionTypeIdByLabel } from 'Utils/Dictionary';
import { useTranslation } from 'react-i18next';
import {
	CircularProgress,
	Button,
	Paper,
	DialogTitle,
	DialogContent,
	DialogActions,
	Grid,
	Dialog,
	LinearProgress,
} from '@material-ui/core'
import PendingScreen from 'Shared/components/PendingScreen';
import ErrorPage from 'Shared/components/ErrorPage';
import BackButton from 'Shared/components/BackButton';


const useStyles = makeStyles(theme => ({
	paper: {
		padding: '1em',
		height: '100%',
		overflowY: 'auto',
	},
	h3: {
		color: '#A5A4BF',
		margin: 0,
		fontWeight: 'normal',
	},
	paperHeader: {
		display: 'flex',
		justifyContent: 'space-between',
	},
	paperContainer: {
		paddingTop: '0.5em',
	},
	dialog: {
		width: '30%',
	},
	title: {
		width: '100%',
	}
}))


const ButtonLink = React.forwardRef((props, ref) => (
  <Link innerRef={ref} {...props} />
))


function EditQuestionnaire(props) {
	const { t } = useTranslation();
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [title, setTitle] = React.useState();
	const { existingQuestions, editQuestions, isQuestionsFetched, isQuestionsPending, isEditPending, isEdited, editError } = props;
	const { idQuestionnaire } = props.match.params;

	useEffect(() => {
		props.clearEditQuestion();
		props.fetchQuestions();
	}, [])

	useEffect(() => {
		setTitle(props.questionnaire.title);
  }, [props.questionnaire])

  useEffect(() => {
    if(!props.questionnaire.questions) {
      props.fetchQuestionnaire(idQuestionnaire);
    }
  }, []);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
	};

	const handleTitleChange = event => {
		const { value } = event.target;
		setTitle(value);
	}

	const handleRemove = (question) => {
		props.removeQuestion(question);
	};

	const handleAdd = (question) => {
		props.addQuestion(question);
	}

	const handleEdit = (question) => {
		props.getQuestionToEdit(question)
		props.history.push(`/questionnaires/CreateQuestion/${idQuestionnaire}`);
	}

	useEffect(() => {
		if (isEdited) {
			props.history.push(`/questionnaires`);
		}
	}, [isEdited])

	const handleSubmit = () => {
		handleClose();

		let questionnaire = props.questionnaire;
		questionnaire.title = title;
		questionnaire.questions = editQuestions;

		props.editQuestionnaire(questionnaire);
	}

	if (isEditPending) {
		return <PendingScreen />
	} else if (editError) {
		return <ErrorPage error={editError.title}/>
	}

	return (
		<React.Fragment>
			<Grid container style={{height: '100%'}}>
				<Grid item lg={12} style={{height: '5%'}}>
					<h1>{t('questionnaireEdit')} {title}</h1>

				</Grid>
				<Grid item container spacing={3} style={{height: '90%'}}>
					<Grid item lg={6} style={{height: '100%'}}>
						<Grid item lg={12} style={{height: '5%'}} className={classes.paperHeader}>
							<h3 className={classes.h3}>{t('questionsAdded')}</h3>
							<Button
								component={ButtonLink}
								to={`/questionnaires/CreateQuestion/${idQuestionnaire}`}
								variant="contained"
								color="primary"
							>
								{t('questionCreate')}
							</Button>
						</Grid>
						<Grid item lg={12} style={{height: '95%'}} className={classes.paperContainer} >
							<Paper className={classes.paper}>
								{editQuestions && editQuestions.map((question, i) => (
									<QuestionExpansionBlock key={i}
										question={question}
										handler={handleRemove}
										handleEdit={handleEdit}
										buttonType="remove"
										/>
								))}
							</Paper>
						</Grid>
					</Grid>
					<Grid item lg={6} style={{height: '100%'}}>
						<Grid item lg={12} style={{height: '5%'}} className={classes.paperHeader}>
							<h3 className={classes.h3}>{t('questionsExisting')}</h3>
						</Grid>
						<Grid item lg={12} style={{height: '95%'}} className={classes.paperContainer} >
							<Paper className={classes.paper}>
								{isQuestionsPending && null}
								{existingQuestions && existingQuestions.map((question, i) => (
									<QuestionExpansionBlock key={i}
										question={question}
										handler={handleAdd}
										buttonType="add"
									/>
								))}
							</Paper>
						</Grid>
					</Grid>
				</Grid>
				<Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
					<BackButton to={`/questionnaires/${idQuestionnaire}/edit`}/>
					{editQuestions.length > 0 &&
						<Button color="primary" variant="contained" endIcon={<SaveIcon />} onClick={handleClickOpen}>
							{t('save')}
						</Button>
					}
					<Dialog
						open={open}
						onClose={handleClose}
						aria-labelledby="alert-dialog-title"
						aria-describedby="alert-dialog-description"
						fullWidth
					>
						<ValidatorForm onSubmit={handleSubmit}>
							<DialogTitle id="alert-dialog-title">{t('questionnaireAssignTitle')}</DialogTitle>
							<DialogContent>
									<TextValidator
										id="title"
										name="title"
										label={t('title')}
										validators={['required']}
										errorMessages={[t('requiredField')]}
										value={title}
										onChange={handleTitleChange}
										className={classes.title}
									/>
							</DialogContent>
							<DialogActions>
								<Button onClick={handleClose} color="primary">
									{t('cancel')}
								</Button>
								<Button color="primary" type="submit">
									{t('save')}
								</Button>
							</DialogActions>
						</ValidatorForm>
					</Dialog>
				</Grid>
			</Grid>
		</React.Fragment>
	)
}

const mapStateToProps = state => ({
	existingQuestions: state.questionnaires.questions.existingQuestions,
	isQuestionsFetched: state.questionnaires.questions.isFetched,
  isQuestionsPending: state.questionnaires.questions.isPending,
	questionnaire: state.questionnaires.questionnaire,
	editQuestions: state.questionnaires.questions.editQuestions,
	isEditPending: state.questionnaires.questionnaire.isPending,
	isEdited: state.questionnaires.questionnaire.isEdited,
	editError: state.questionnaires.questionnaire.error,
})

const mapDispatchToProps = dispatch => ({
	addQuestion: question => dispatch(addQuestionToQuestionnaire(question)),
	removeQuestion: question => dispatch(removeQuestionFromQuestionnaire(question)),
	fetchQuestions: () => dispatch(getQuestions()),
  addQuestionnaire: questionnaire => dispatch(addQuestionnaire(questionnaire)),
	fetchQuestionnaire: id => dispatch(getQuestionnaire(id)),
	getQuestionToEdit: question => dispatch(getQuestionToEdit(question)),
	clearEditQuestion: () => dispatch(clearEditQuestion()),
	editQuestionnaire: questionnaire => dispatch(editQuestionnaire(questionnaire)),
})

export default connect(mapStateToProps, mapDispatchToProps)(EditQuestionnaire);
