import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import QuestionExpansionBlock from './QuestionExpansionBlock';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import { getQuestions } from 'Api/QuestionApi';
import { connect } from 'react-redux';
import {
	addQuestionToAdded,
	removeQuestionFromAdded,
  getQuestionToEdit,
  clearEditQuestion,
 } from 'Redux/modules/questionnaires/actions';
import LinearProgress from '@material-ui/core/LinearProgress';
import SaveIcon from '@material-ui/icons/Save';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { getQuestionTypeIdByLabel } from 'Utils/Dictionary';
import { useTranslation } from 'react-i18next';
import { addQuestionnaire } from 'Api/QuestionnaireApi';
import PendingScreen from 'Shared/components/PendingScreen';
import ErrorPage from 'Shared/components/ErrorPage';
import BackButton from 'Shared/components/BackButton';


const useStyles = makeStyles(theme => ({
	paper: {
		padding: '1em',
		height: '100%',
		overflowY: 'auto',
	},
	h3: {
		color: '#A5A4BF',
		margin: 0,
		fontWeight: 'normal',
	},
	paperHeader: {
		display: 'flex',
		justifyContent: 'space-between',
	},
	paperContainer: {
		paddingTop: '0.5em',
	},
	dialog: {
		width: '30%',
	},
	title: {
		width: '100%',
	}
}))

function AddQuestionnaire(props) {
	const { t } = useTranslation();
	const classes = useStyles();
	const [open, setOpen] = React.useState(false);
	const [title, setTitle] = React.useState('');
	const { addedQuestions, existingQuestions, isPending, isQuestionnaireAdded, isQuestionnairePending, isError } = props;

	useEffect(() => {
    props.clearEditQuestion();
		props.fetchQuestions();
	}, [])

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
	};

	const handleTitleChange = event => {
		const { value } = event.target;
		setTitle(value);
	}

	const handleRemove = (question) => {
		props.removeQuestion(question);
	};

	const handleAdd = (question) => {
		console.log(props);
		props.addQuestion(question);
	};

	const handleEdit = (question) => {
		props.getQuestionToEdit(question);
		props.history.push(`/questionnaires/CreateQuestion`);
	}

	useEffect(() => {
		if (isQuestionnaireAdded) {
			props.history.push('/questionnaires');
		}
	}, [isQuestionnaireAdded])

	const handleSubmit = async () => {
		handleClose();
		let questionnaire = {
			title,
			questions: [
				...addedQuestions
				.map(q => ({
					...q,
					idQuestionType: getQuestionTypeIdByLabel(q.questionType, props.questionTypes)
				}))
			]
		}
		props.addQuestionnaire(questionnaire);
  }

	if (isQuestionnairePending) {
		return <PendingScreen />
	}
	if (isError) {
		return <ErrorPage error={isError.title}/>
	}

	return (
		<React.Fragment>
			<Grid container style={{ height: '100%' }}>
				<Grid item lg={12} style={{ height: '5%' }}>
					<h1>{t('createNewQuestionnaire')}</h1>

				</Grid>
				<Grid item container spacing={3} style={{height: '90%'}}>
					<Grid item lg={6} style={{height: '100%'}}>
						<Grid item lg={12} style={{height: '5%'}} className={classes.paperHeader}>
							<h3 className={classes.h3}>{t('questionsAdded')}</h3>
							<Button
								component={Link}
								to={'CreateQuestion'}
								variant="contained"
								color="primary"
							>
								{t('questionCreate')}
							</Button>
						</Grid>
						<Grid item lg={12} style={{height: '95%'}} className={classes.paperContainer} >
							<Paper className={classes.paper}>
								{addedQuestions.map((question, i) => (
									<QuestionExpansionBlock key={i}
										question={question}
										handler={handleRemove}
										buttonType="remove"
										handleEdit={handleEdit}
										/>
								))}
							</Paper>
						</Grid>
					</Grid>
					<Grid item lg={6} style={{height: '100%'}}>
						<Grid item lg={12} style={{height: '5%'}} className={classes.paperHeader}>
							<h3 className={classes.h3}>{t('questionsExisting')}</h3>
						</Grid>
						<Grid item lg={12} style={{height: '95%'}} className={classes.paperContainer} >
							<Paper className={classes.paper}>
								{existingQuestions && existingQuestions.map((question, i) => (
									<QuestionExpansionBlock key={i}
										question={question}
										handler={handleAdd}
										buttonType="add"
									/>
								))}
							</Paper>
						</Grid>
					</Grid>
				</Grid>
				<Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
					<BackButton to='/questionnaires'/>
					{addedQuestions.length > 0 &&
						<Button color="primary" variant="contained" endIcon={<SaveIcon />} onClick={handleClickOpen}>
							{t('save')}
						</Button>
					}

				</Grid>
			</Grid>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
				fullWidth
			>
				<ValidatorForm onSubmit={handleSubmit}>
					<DialogTitle id="alert-dialog-title">{t('questionnaireAssignTitle')}</DialogTitle>
					<DialogContent>
						<TextValidator
							id="title"
							name="title"
							label={t('title')}
							validators={['required']}
							errorMessages={[t('requiredField')]}
							value={title}
							onChange={handleTitleChange}
							className={classes.title}
						/>
					</DialogContent>
					<DialogActions>
						<Button onClick={handleClose} color="primary">
							{t('cancel')}
						</Button>
						<Button color="primary" type="submit">
							{t('save')}
						</Button>
					</DialogActions>
				</ValidatorForm>
			</Dialog>
		</React.Fragment>
	)
}

const mapStateToProps = state => ({
	existingQuestions: state.questionnaires.questions.existingQuestions,
	addedQuestions: state.questionnaires.questions.addedQuestions,
	isFetched: state.questionnaires.questions.isFetched,
	isPending: state.questionnaires.questions.isPending,
	isQuestionnairePending: state.questionnaires.questionnaire.isPending,
	isQuestionnaireAdded: state.questionnaires.questionnaire.isAdded,
	isError: state.questionnaires.questionnaire.error,
})

const mapDispatchToProps = dispatch => ({
	addQuestion: question => dispatch(addQuestionToAdded(question)),
	removeQuestion: question => dispatch(removeQuestionFromAdded(question)),
	fetchQuestions: () => dispatch(getQuestions()),
	addQuestionnaire: questionnaire => dispatch(addQuestionnaire(questionnaire)),
  getQuestionToEdit: question => dispatch(getQuestionToEdit(question)),
  clearEditQuestion: () => dispatch(clearEditQuestion()),
})

export default connect(mapStateToProps, mapDispatchToProps)(AddQuestionnaire);
