import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import QuestionExpansionBlock from './QuestionExpansionBlock';
import Paper from '@material-ui/core/Paper';
import { Link } from 'react-router-dom';
import { getQuestionnaire } from 'Api/QuestionnaireApi';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Button from '@material-ui/core/Button';
import EditIcon from '@material-ui/icons/Edit';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';
import { useTranslation } from 'react-i18next';
import BackButton from 'Shared/components/BackButton';

const useStyles = makeStyles(theme => ({
	paper: {
		padding: '2em',
		overflowY: 'auto',
		height: '100%'
	},
	paperContainer: {
		padding: '0.5em 0',
		height: '90%',
	},
	header: {
		height: '5%',
	},
	question: {
		backgroundColor: '#3B86FF',
		color: 'white',
		'&content': {
			alignItems: 'center',
		}
	},
	questionName: {
		display: 'flex',
		flexBasis: '70%',
		flexDirection: 'column',
	},
	questionType: {
		flexBasis: '30%'
	},
	expandIcon: {
		color: 'white',
	},
}))

const ButtonLink = React.forwardRef((props, ref) => (
  <Link innerRef={ref} {...props} />
))


 function QuestionnaireInfo(props) {
	const classes = useStyles();
  const { t } = useTranslation();
  const { idQuestionnaire, mode } = props.match.params;

	useEffect(() => {
		props.fetchQuestionnaire(idQuestionnaire);
		console.log(props);
	}, []);

	const { isPending, questionnaire } = props;
  const { title, questions } = questionnaire;

	return (
		<React.Fragment>
			<Grid container justify="space-between" className={classes.header}>
				<Typography component="div">
					<h1>{title}</h1>
				</Typography>
			</Grid>
			<Typography component="div" className={classes.paperContainer}>
				<Paper className={classes.paper}>
					{isPending &&
						null
					}
					<Grid container spacing={4}>
						<Grid item xs={6}>
						{questions && questions.slice(0, Math.ceil(questions.length / 2)).map(question => (
							<QuestionExpansionBlock key={question.idQuestion} buttonType="preview" question={question} />
						))}
						</Grid>
						<Grid item xs={6}>
						{questions && questions.slice(Math.ceil(questions.length / 2), questions.length).map(question => (
							<QuestionExpansionBlock key={question.idQuestion} buttonType="preview" question={question} />
						))}
						</Grid>
					</Grid>
				</Paper>
			</Typography>
			<Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
        <BackButton to="/questionnaires" title={t('questionnaires')}/>
        {mode === 'edit' &&
          <Button
            color="primary"
            variant="contained"
            startIcon={<EditIcon />}
            component={ButtonLink}
            to={`/questionnaires/EditQuestionnaire/${idQuestionnaire}`}
            style={{height: '100%'}}
            >
            {t('edit')}
          </Button>
        }

			</Grid>
		</React.Fragment>

	)
}


const mapStateToProps = state => ({
	questionnaire: state.questionnaires.questionnaire,
	isPending: state.questionnaires.isPending,
})

const mapDispatchToProps = dispatch => ({
	fetchQuestionnaire: id => dispatch(getQuestionnaire(id)),
})


export default connect(mapStateToProps, mapDispatchToProps)(QuestionnaireInfo);
