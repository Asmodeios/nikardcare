import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Typography,
  FormControl,
  FormControlLabel,
  Checkbox,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Logo_img from 'Images/Logo_img.png';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { login } from 'Api/AuthorizationApi';
import ButtonLink from 'Shared/components/ButtonLink';

const useStyles = makeStyles(theme => ({
  secondaryText: {
    color: '#a5a5a5',
    marginBottom: '2em',
  },
  header: {
    marginBottom: '1em',
  },
  formControl: {
    width: '100%',
  },
  inputGrid: {
    marginBottom: '1.5em',
  },
  loginGrid: {
    marginTop: '2em',
  },
  loginMain: {
    padding: '0 230px 0 0',
    backgroundColor: 'white',
  },
  error: {
    color: 'red',
  },
}))

function Authorization(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [credentials, setCredentials] = useState({
    email: '',
    password: '',
  });
 
  const handleSubmit = () => {
    props.login(credentials);
  };

  useEffect(() => {
    if (authenticated) {
      props.history.push('/patients')
    }
  }, [props.authenticated])

  const handleChange = (event) => {
    const { value, name } = event.target;
    setCredentials(oldValues => ({
      ...oldValues,
      [name]: value,
    }));
  };

  const handleForgot = () => {
    props.history.push('/forgot')
  }
  const { error, authenticated } = props;

  return (
    <main className={classes.loginMain}>
        <ValidatorForm onSubmit={handleSubmit}>
        <Grid container justify="center">
          <Grid item container xs={12} justify="center">
            <img src={Logo_img} placeholder='Logo_img'/>
          </Grid>
          <Grid item container xs={12} justify="center" className={classes.header}>
            <Typography variant="h2">
              {t('appName')}
            </Typography>
          </Grid>
          <Grid item container xs={12} justify="center">
            <Typography component="span" className={classes.secondaryText}>
            {t('welcomeMessage')}
            </Typography>
          </Grid>
          <Grid item container xs={12} direction="column" alignItems="center">
            <Grid item container xs={3} justify="center" className={classes.inputGrid}>
              <FormControl className={classes.formControl}>
                <TextValidator
                  label={t('email')}
                  id="email"
                  name="email"
                  validators={['required', 'isEmail']}
                  errorMessages={[t('requiredField'), t('invalidEmail')]}
                  value={credentials.email}
                  onChange={handleChange}
                />
              </FormControl>
            </Grid>
            <Grid item container xs={3} justify="center" className={classes.inputGrid}>
              <FormControl className={classes.formControl}>
                <TextValidator
                  label={t('password')}
                  type="password"
                  id="password"
                  name="password"
                  validators={['required']}
                  errorMessages={[t('requiredField')]}
                  value={credentials.password}
                  onChange={handleChange}
                />
              </FormControl>
            </Grid>
            
          </Grid>
          <Grid item container justify="center">
            <Grid item container xs={3} justify="flex-start" >
              <Button 
                color="primary" 
                size="small" 
                component={ButtonLink}
                to="/forgot"
                >
                {t('forgotPassword')}
              </Button>
            </Grid>
            {error && error.data && error.data.errors && (
              <Grid item container xs={12} justify="center">
                <Typography component="span" className={classes.error}>
                  {t(`${error.data.errors[0]}`)}
                </Typography>
              </Grid>
            )}
          </Grid>
          <Grid item container xs={12} justify="center" className={classes.loginGrid}>
            <Button variant="contained" color="primary" type="submit" style={{width: "10%"}}>{t('login')}</Button>
          </Grid>
        </Grid>
      </ValidatorForm>
    </main>
  );
};

const mapStateToProps = state => ({
  user: state.authorization.user,
  authenticated: state.authorization.authenticated,
  error: state.authorization.error,
});

const mapDispatchToProps = dispatch => ({
  login: (credentials) => dispatch(login(credentials)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Authorization);