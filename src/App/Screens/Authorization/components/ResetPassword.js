import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Typography,
  FormControl,
  FormControlLabel,
  Checkbox,
  TextField,
  InputAdornment,
  IconButton
} from '@material-ui/core';
import Logo_img from 'Images/Logo_img.png';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { 
  confirmEmail,
  setPassword,
 } from 'Api/AuthorizationApi';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles(theme => ({
  secondaryText: {
    color: '#a5a5a5',
    marginBottom: '2em',
  },
  header: {
    marginBottom: '1em',
  },
  formControl: {
    width: '100%',
  },
  inputGrid: {
    marginBottom: '2em',
  },
  loginGrid: {
    marginTop: '2em',
  },
  loginMain: {
    padding: '0 230px 0 0',
    backgroundColor: 'white',
  },
  error: {
    color: 'red',
  },
}))

const passwordRegex = /^(?=.*\d)(?=.*[A-Z])(?=.*[^a-zA-Z\d\s:]).{8,}$/;

function ResetPassword(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };


  const formik = useFormik({
    initialValues: {
      password1: '',
      confirmationPassword: '',
    },
    validationSchema: Yup.object({
      password1: Yup.string()
        .required(t('requiredField'))
        .test('constraints', t('passwordConstraint'), (val) => {
          return passwordRegex.test(val);
        }),
      confirmationPassword: Yup.string()
        .test('equal', t('passwordConfirmationError'), (value) => {
          return password1.value === value
        }),
    }),
    onSubmit: async (values) => {
      const data = {
        idUser: id,
        token,
        password: values.password1,
      }
      console.log(token);
      let response = null;
      if (mode === 'register') {
        response = await confirmEmail(data);
      } else if (mode === 'reset') {
        response = await setPassword(data);
      };

      if (response && response.status === 200) {
        props.history.push('/login');
      };
    },
  });

  const { error, authenticated } = props;
  const { 
    touched,
    handleChange, 
    handleBlur,
    values,
    errors,
    handleSubmit
  } = formik;
  const error1 = touched.password1 && errors.password1;
  const error2 = touched.confirmationPassword && errors.confirmationPassword;
  const { mode, id } = props.match.params;
  const token = location.search.split('?token=')[1];

  return (
    <main className={classes.loginMain}>
        <form onSubmit={handleSubmit}>
        <Grid container justify="center">
          <Grid item container xs={12} justify="center">
            <img src={Logo_img} placeholder='Logo_img'/>
          </Grid>
          <Grid item container xs={12} justify="center" className={classes.header}>
            <Typography variant="h2">
              {t('appName')}
            </Typography>
          </Grid>
          <Grid item container xs={12} justify="center">
            <Typography component="span" className={classes.secondaryText}>
            {t('resetPassword')}
            </Typography>
          </Grid>
          <Grid item container xs={12} direction="column" alignItems="center">
            <Grid item container xs={3} justify="center" className={classes.inputGrid}>
              <FormControl className={classes.formControl}>
                <TextField
                  error={!!error1}
                  label={t('password')}
                  type={showPassword ? 'text': 'password'}
                  id="password1"
                  name="password1"
                  value={values.password1}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={error1 && errors.password1}
                  InputProps={{
                    endAdornment:  
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                  }}
                />
              </FormControl>
            </Grid>
          </Grid>
          <Grid item container xs={12} direction="column" alignItems="center">
            <Grid item container xs={3} justify="center" className={classes.inputGrid}>
              <FormControl className={classes.formControl}>
                <TextField
                  error={!!error2}
                  label={t('passwordConfirmation')}
                  type={showPassword ? 'text': 'password'}
                  id="confirmationPassword"
                  name="confirmationPassword"
                  value={values.confirmationPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={error2 && errors.confirmationPassword}
                />
              </FormControl>
            </Grid>
          </Grid>
          {error && (
            <Grid item container xs={12} justify="center">
              <Typography component="span" className={classes.error}>
                {t(`${error.message[0]}`)}
              </Typography>
          </Grid>
          )}
          
          <Grid item container xs={12} justify="center" className={classes.loginGrid}>
            <Button variant="contained" color="primary" type="submit" style={{width: "10%"}}>{t('submit')}</Button>
          </Grid>
        </Grid>
      </form>
    </main>
  );
};

export default ResetPassword;