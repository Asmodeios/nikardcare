import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Typography,
  FormControl,
  FormControlLabel,
  Checkbox,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from '@material-ui/core';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import Logo_img from 'Images/Logo_img.png';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { resetPassword } from 'Api/AuthorizationApi';
import BackButton from 'Shared/components/BackButton';

const useStyles = makeStyles(theme => ({
  secondaryText: {
    color: '#a5a5a5',
    marginBottom: '2em',
  },
  header: {
    marginBottom: '1em',
  },
  formControl: {
    width: '100%',
  },
  inputGrid: {
    marginBottom: '1.5em',
  },
  loginGrid: {
    marginTop: '2em',
  },
  loginMain: {
    padding: '0 230px 0 0',
    backgroundColor: 'white',
  },
  error: {
    color: 'red',
  },
}))

function ForgotPassword(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [credentials, setCredentials] = useState({
    email: '',
  });
  const [isSent, setIsSent] = useState(false);
 
  const handleSubmit = async () => {
    const response = await resetPassword(credentials);
    if (response.status === 200) {
      setIsSent(true);
    }
  };

  const handleChange = (event) => {
    const { value, name } = event.target;
    setCredentials(oldValues => ({
      ...oldValues,
      [name]: value,
    }));
  };


  return (
    <main className={classes.loginMain}>
        <ValidatorForm onSubmit={handleSubmit}>
        <Grid container justify="center">
          <Grid item container xs={12} justify="center">
            <img src={Logo_img} placeholder='Logo_img'/>
          </Grid>
          <Grid item container xs={12} justify="center" className={classes.header}>
            <Typography variant="h2">
              {t('appName')}
            </Typography>
          </Grid>
          <Grid item container xs={12} justify="center">
            <Typography component="span" className={classes.secondaryText}>
              {isSent ? t('sentEmail') : t('recoveryMessage')}
            </Typography>
          </Grid>
          {isSent ? (
            <>
              <Grid item container xs={12} justify="center">
                <Typography variant="h4">
                  {credentials.email}
                </Typography>
              </Grid>
              <Grid item container xs={12} justify="center" className={classes.loginGrid}>
                <Grid item container xs={3} justify="center" className={classes.inputGrid}>
                  <BackButton />
                </Grid>
              </Grid>
            </>
          ):
          (
            <>
              <Grid item container xs={12} direction="column" alignItems="center">
                <Grid item container xs={3} justify="center" className={classes.inputGrid}>
                  <FormControl className={classes.formControl}>
                    <TextValidator
                      label={t('email')}
                      id="email"
                      name="email"
                      validators={['required', 'isEmail']}
                      errorMessages={[t('requiredField'), t('invalidEmail')]}
                      value={credentials.email}
                      onChange={handleChange}
                    />
                  </FormControl>
                </Grid>
              </Grid>
              <Grid item container xs={12} justify="center" className={classes.loginGrid}>
                <Grid item container xs={3} justify="space-between" className={classes.inputGrid}>
                  <BackButton />
                  <Button variant="contained" color="primary" type="submit" >{t('recover')}</Button>
                </Grid>
              </Grid>
            </>
          )}
        </Grid>
      </ValidatorForm>
      
    </main>
  );
};


export default ForgotPassword;