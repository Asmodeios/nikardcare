import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Typography,
  FormControl,
  FormControlLabel,
  Checkbox,
  TextField,
  InputAdornment,
  IconButton,
  FormLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core';
import Logo_img from 'Images/Logo_img.png';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { 
  addDoctor,
 } from 'Api/DoctorApi';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const useStyles = makeStyles(theme => ({
  secondaryText: {
    color: '#a5a5a5',
    marginBottom: '2em',
  },
  header: {
    marginBottom: '1em',
  },
  formControl: {
    marginBottom: '1em',
    minWidth: 350,
    maxWidth: 350,
  },
  error: {
    color: 'red',
  },
  saveBtn: {
    margin: '1em 9em',
    minWidth: 100,
  },
}))

const passwordRegex = /^(?=.*\d)(?=.*[A-Z])(?=.*[^a-zA-Z\d\s:]).{8,}$/;

function DoctorForm(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [showPassword, setShowPassword] = useState(false);

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = event => {
    event.preventDefault();
  };


  const formik = useFormik({
    initialValues: {
      firstName: '',
      email: '',
      lastName: '',
      sex: 'male',
      doctorCode: '',
    },
    validationSchema: Yup.object({
      firstName: Yup.string()
        .required(t('requiredField')),
      email: Yup.string()
        .required(t('requiredField'))
        .email(t('invalidEmail')),
      lastName: Yup.string()
        .required(t('requiredField')),
      doctorCode: Yup.string()
        .required(t('requiredField')),
    }),
    onSubmit: async (values) => {
      const data = {
        ...values,
        isMale: values.sex === 'male',
      }
      const response = await addDoctor(data);

      if (response && response.status === 200) {
        props.closeForm();
      };
    },
  });

  const { 
    touched,
    handleChange, 
    handleBlur,
    values,
    errors,
    handleSubmit
  } = formik;

  return (
      <form onSubmit={handleSubmit}>
      <Grid container>
        <Grid item container justify="center" xs={6}>
          <FormControl className={classes.formControl}>
            <TextField
              error={!!errors.firstName && !!touched.firstName}
              label={`${t('firstName')}*`}
              id="firstName"
              name="firstName"
              value={values.firstName}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.firstName && errors.firstName}
            />
          </FormControl>
        </Grid>
        <Grid item container justify="center" xs={6}>
          <FormControl className={classes.formControl}>
            <TextField
              error={!!errors.lastName && !!touched.lastName}
              label={`${t('lastName')}*`}
              id="lastName"
              name="lastName"
              value={values.lastName}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.lastName && errors.lastName}
            />
          </FormControl>
        </Grid>
        <Grid item container justify="center" xs={6}>
          <FormControl className={classes.formControl}>
            <TextField
              error={!!errors.email && !!touched.email}
              label={`${t('email')}*`}
              id="email"
              name="email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.email && errors.email}
            />
          </FormControl>
        </Grid>
        <Grid item container justify="center" xs={6}>
          <FormControl className={classes.formControl}>
            <TextField
              error={!!errors.doctorCode && !!touched.doctorCode}
              label={`${t('doctorCode')}*`}
              id="doctorCode"
              name="doctorCode"
              value={values.doctorCode}
              onChange={handleChange}
              onBlur={handleBlur}
              helperText={touched.doctorCode && errors.doctorCode}
            />
          </FormControl>
        </Grid>
        <Grid item container justify="center" xs={6}>
          <Grid item container justify="flex-start" xs={6}>
            <FormControl component="fieldset">
              <FormLabel component="legend">{t('sex')}</FormLabel>
              <RadioGroup aria-label="sex" name="sex" value={values.sex} onChange={handleChange} row>
                <FormControlLabel
                  value="male"
                  control={<Radio color="primary" />}
                  label={t('male')}
                  labelPlacement="end"
                />
                <FormControlLabel
                  value="female"
                  control={<Radio color="primary" />}
                  label={t('female')}
                  labelPlacement="end"
                />
              </RadioGroup>
            </FormControl>
          </Grid>
        </Grid>
        <Grid item container justify="flex-end" xs={12}>
            <Button variant="contained" type="submit" color="primary" className={classes.saveBtn}> 
              {t('save')}
            </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default DoctorForm;