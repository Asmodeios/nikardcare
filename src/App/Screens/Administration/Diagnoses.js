import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  Icon,
  Tabs,
  Tab,
  Typography,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  DialogActions,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { 
  getAdminDiagnoses,
  addDiagnosis,
  editDiagnosis,
  deleteDiagnosis
 } from 'Api/DiagnosisApi';

const useStyles = makeStyles(theme => ({

  deleteButton: {
    color: 'red',
    borderColor: 'red'
  },
  title: {
		width: '100%',
	},
}))


function Diagnoses(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const [editOpen, setEditOpen] = useState(false);
  const [deleteOpen, setDeleteOpen] = useState(false);
  const [diagnoses, setDiagnoses] = useState([]);
  const [editTitle, setEditTitle] = React.useState('');
  const [addTitle, setAddTitle] = React.useState('');
  const [currentDiagnosis, setCurrentDiagnosis] = useState({});

  useEffect(() => {
    fetchDiagnoses();
  }, [])

  
  const handleEditTitle = event => {
		const { value } = event.target;
		setEditTitle(value);
  }

  const handleAddTitle = event => {
		const { value } = event.target;
		setAddTitle(value);
  }
  

  const fetchDiagnoses = async () => {
    const response = await getAdminDiagnoses();
    if (response && response.status >= 200 && response.status <= 299) {
      setDiagnoses(response.data);
    }
  }

  const handleEditOpen = (diagnosis) => {
    setEditTitle(diagnosis.diagnosisName);
    setCurrentDiagnosis(diagnosis);
    setEditOpen(true);
  };

  const handleEditClose = () => {
    setEditOpen(false);
  };

  const handleDeleteOpen = (diagnosis) => {
    setCurrentDiagnosis(diagnosis);
    setDeleteOpen(true);
  };

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };


  const handleAddClose = () => {
    props.setOpenDiagnosis(false);
  };

  const handleEdit = async () => {
    const response = await editDiagnosis({
      idDiagnosis: currentDiagnosis.idDiagnosis,
      diagnosisName: editTitle,
    });
    if (response && response.status >= 200 && response.status <= 299) {
      handleEditClose();
      fetchDiagnoses();
    };
  };

  const handleAdd = async () => {
    const response = await addDiagnosis(addTitle);
    if (response && response.status >= 200 && response.status <= 299) {
      props.setOpenDiagnosis(false);
      fetchDiagnoses();
    };
  };

  const handleDelete = async () => {
    const response = await deleteDiagnosis(currentDiagnosis.idDiagnosis);
    if (response && response.status >= 200 && response.status <= 299) {
      handleDeleteClose();
      fetchDiagnoses();
    };
  }

  return (
    <React.Fragment>
      <TableContainer component={Paper}>
        <Table stickyHeader className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell align="center">{t('diagnosis')}</TableCell>
              <TableCell align="center"></TableCell>
              <TableCell align="center"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {diagnoses && diagnoses.length > 0 && diagnoses.map((row, i) => (
              <TableRow key={i}>
                <TableCell >{i + 1}</TableCell>
                <TableCell align="center">{row.diagnosisName}</TableCell>
                <TableCell align="center">
                  <Button color="primary" variant="outlined" onClick={() => handleEditOpen(row)}>
                    {t('edit')}
                  </Button>
                </TableCell>
                <TableCell align="center">
                  {!row.isAssigned && 
                    <Button className={classes.deleteButton} variant="outlined" onClick={() => handleDeleteOpen(row)}>
                      {t('delete')}
                    </Button>
                  }
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog 
        maxWidth="sm" 
        fullWidth={true}
        open={editOpen} 
        onClose={handleEditClose}
        aria-labelledby="edit-diagnosis-dialog"
        >
          <ValidatorForm onSubmit={handleEdit}>
            <DialogTitle id="max-width-dialog-title">
            {`${t('editDiagnosis')} "${currentDiagnosis.diagnosisName}"`}
            </DialogTitle>
            <DialogContent style={{overflowY: 'hidden'}}>
              <TextValidator
                id="editTitle"
                name="editTitle"
                label={t('title')}
                validators={['required']}
                errorMessages={[t('requiredField')]}
                value={editTitle}
                onChange={handleEditTitle}
                className={classes.title}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleEditClose} color="primary">
                {t('cancel')}
              </Button>
              <Button color="primary" type="submit">
                {t('save')}
              </Button>
            </DialogActions>
          </ValidatorForm>
      </Dialog>  
      <Dialog 
        maxWidth="sm" 
        fullWidth={true}
        open={props.openDiagnosis} 
        onClose={handleAddClose}
        aria-labelledby="add-diagnosis-dialog"
        >
          <ValidatorForm onSubmit={handleAdd}>
            <DialogTitle id="max-width-dialog-title">
              {t('addDiagnosis')}
            </DialogTitle>
            <DialogContent style={{overflowY: 'hidden'}}>
              <TextValidator
                id="addTitle"
                name="addTitle"
                label={t('title')}
                validators={['required']}
                errorMessages={[t('requiredField')]}
                value={addTitle}
                onChange={handleAddTitle}
                className={classes.title}
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleEditClose} color="primary">
                {t('cancel')}
              </Button>
              <Button color="primary" type="submit">
                {t('save')}
              </Button>
            </DialogActions>
          </ValidatorForm>
        </Dialog>  
      <Dialog 
        maxWidth="sm" 
        fullWidth={true}
        open={deleteOpen} 
        onClose={handleDeleteClose}
        aria-labelledby="add-diagnosis-dialog"
        >
          <ValidatorForm onSubmit={handleDelete}>
            <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>
              {`${t('deleteMessage')} "${currentDiagnosis.diagnosisName}"?`}
            </DialogTitle>
            <DialogActions>
              <Button onClick={handleDeleteClose} color="secondary" variant="contained">
                {t('cancel')}
              </Button>
              <Button color="primary" type="submit" variant="contained">
                {t('delete')}
              </Button>
            </DialogActions>
          </ValidatorForm>
        </Dialog>  
    </React.Fragment>
    );
}




export default Diagnoses;