import React, { useState, useEffect } from 'react';
import {
  Grid,
  Button,
  Dialog,
  DialogContent,
  DialogTitle,
  DialogActions,
  Icon,
  Tabs,
  Tab,
  Typography,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@material-ui/core';
import UserBlock from 'Shared/components/UserBlock';
import {
  getDoctors,
  deleteDoctor,
} from 'Api/DoctorApi';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import PendingScreen from 'Shared/components/PendingScreen';
import { a11yProps , TabPanel } from 'Shared/components/TabPanel';
import Diagnoses from './Diagnoses';
import DoctorForm from './DoctorForm';
import DeleteIcon from '@material-ui/icons/Delete';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
  doctors: {
    overflowY: 'auto',
    height: '100%',
    backgroundColor: 'white',
    alignContent: 'flex-start',
  },
  tabPanel: {
    height: '100%',
  },
  cardContainer: {
    height: '100%',
  },
  paperContainer: {
		padding: '0.5em 0',
		height: '90%',
  },
  paper: {
		padding: '1em',
		overflowY: 'auto',
		height: '100%'
  },
  table: {
    height: '100%'
  },
  deleteButton: {
    color: 'red',
    borderColor: 'red'
  }
}))


function Administration(props) {
  const [openDiagnosis, setOpenDiagnosis] = useState(false);
  const [open, setOpen] = useState(false);
  const [deleteOpen, setDeleteOpen] = useState(false);
  const [tab, setTab] = useState(0);
  const [currentDoctor, setCurrentDoctor] = useState({})
  const [doctors, setDoctors] = useState([]);
  const [error, setError] = useState(null);
  const { t } = useTranslation();
  const classes = useStyles();
  const { idUser } = props;
  useEffect(() => {
    fetchDoctors();
  }, [])

  useEffect(() => {
    if (!open) fetchDoctors();
  }, [open])

  const fetchDoctors = async () => {
    const response = await getDoctors();
    if (response && response.status >= 200 && response.status <= 299) {
      setDoctors(response.data);
    }
  }

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleTabChange = (event, newTab) => {
    setTab(newTab);
  }

  const handleDelete = async () => {
    const response = await deleteDoctor(currentDoctor.idDoctor);
    if (response && response.status >= 200 && response.status <= 299) {
      fetchDoctors();
      setDeleteOpen(false);
    }
  }

  return (
    <React.Fragment>
      <Grid container style={{height: '10%'}}>
        <Grid item xs={12}>
          <h1>{t('administration')}</h1>
        </Grid>
        <Grid item container xs={12} alignItems="center">
            <Grid item xs={10}>
              <Tabs
                value={tab}
                onChange={handleTabChange}
                indicatorColor="primary"
                textColor="primary"
                aria-label="questionnaires-tabs"
                >
                <Tab label={t('doctors')} {...a11yProps(0)} />
                <Tab label={t('diagnoses')} {...a11yProps(1)} />
              </Tabs>
            </Grid>
          <Grid item container justify="flex-end" xs={2}>
            {tab === 0 && 
              <Button 
                variant="contained" 
                color="primary" 
                onClick={handleClickOpen}
              >
                <i className="material-icons">group_add</i>{t('addDoctor')}
              </Button>
            }
            {tab === 1 && 
              <Button 
                variant="contained" 
                color="primary" 
                onClick={() => setOpenDiagnosis(true)}
              >
                {t('addDiagnosis')}
              </Button>
            }
          </Grid>
        </Grid>
      </Grid>
      <Typography component="div" className={classes.paperContainer}>
        <Paper className={classes.paper} >
          <TabPanel value={tab} index={0} className={classes.tabPanel}>
            <Grid container className={classes.doctors}>
              {doctors && doctors.length > 0 && doctors.map((doctor, i) => (
                <UserBlock 
                  name={doctor.doctorName} 
                  key={i} 
                  email={doctor.email}
                  doctor={doctor} 
                  gridProps={{xl: 3}}
                  handleDelete={doctor.idDoctor === idUser || doctor.hasConnections ? null :
                    () => {
                    setDeleteOpen(true);
                    setCurrentDoctor(doctor);
                  }}
                />
              ))}            
            </Grid>
          </TabPanel>
          <TabPanel value={tab} index={1} className={classes.diagnosesTabPanel}>
            <Diagnoses setOpenDiagnosis={setOpenDiagnosis} openDiagnosis={openDiagnosis}/>
          </TabPanel>
        </Paper>
      </Typography>
      <Dialog 
        maxWidth="lg" 
        fullWidth={true}
        open={open} 
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
        >
          <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>
            {t('doctorData')}
          </DialogTitle>
          <DialogContent style={{overflowY: 'hidden'}}>
            <DoctorForm closeForm={handleClose}/>
          </DialogContent>
      </Dialog>  
      <Dialog
        maxWidth="sm"
        fullWidth
        open={deleteOpen}
        onClose={() => setDeleteOpen(false)}
        aria-labelledby="max-width-dialog-title"
      >
        <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>
          {`${t('deleteMessage')} "${currentDoctor.doctorName}"?`}
        </DialogTitle>
        <DialogActions>
          <Button onClick={() => setDeleteOpen(false)} color="secondary" variant="contained">
            {t('cancel')}
          </Button>
          <Button 
            variant="contained" 
            style={{color: 'white', backgroundColor: 'red'}}
            endIcon={<DeleteIcon />}
            onClick={handleDelete}
          >
            {t('delete')}
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
    );
};


const mapStateToProps = state => ({
  idUser: state.authorization.id,
})

export default connect(mapStateToProps, null)(Administration);