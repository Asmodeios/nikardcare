import React, { useState, useEffect, useRef } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { connect } from 'react-redux';
import { createQuestion, editQuestion } from 'Redux/modules/questionnaires/actions';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import SaveIcon from '@material-ui/icons/Save';
import { Link } from 'react-router-dom';
import { getAlertTypeIdByLabel, getQuestionTypeLabelById } from 'Utils/Dictionary';
import './styles/AddAnswer.css';
import { getQuestionTypeIdByLabel } from '../../../utils/Dictionary';
import { useTranslation } from 'react-i18next';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import BackButton from 'Shared/components/BackButton';


const useStyles = makeStyles(theme => ({
  buttonText: {
    fontSize: '90%',
    fontFamily: `"Roboto", "Helvetica", "Arial", sans-serif`,
    fontWeight: 'unset'
  },
  typeButton: {
    width: '10%',
    minHeight: '5em',
    margin: '1%'
  },
  questionHeader: {
    fontSize: '1.5rem',
    fontWeight: '400',
    margin: '0',
  }
}));

var ID = function () {
  return new Date().getUTCMilliseconds();
};

function QuestionTitle(props) {
  const { titleInputValue, setTitleInputValue } = props;
  const { t } = useTranslation();
  const handleTitleInput = event => {
    setTitleInputValue(event.target.value);
  }

  return (
    <React.Fragment>
      <Grid item xs={12} className='SmallBlock'>
        <Typography variant='h5'>{t('questionTitle')}</Typography>
      </Grid>
      <Grid item xs={12} className='SmallBlock'>
        <TextValidator
          id="titleValue"
          className={'TitleInput'}
          value={titleInputValue}
          onChange={handleTitleInput}
          placeholder={t('questionTitlePlaceholder')}
          fullWidth={true}
          multiline={true}
          rows={1}
          input={<Input className={'TitleInput2'} />}
          validators={['required']}
          errorMessages={[t('requiredField')]}
        />
      </Grid>
    </React.Fragment>
  )
}

function ChoiceQuestion(props) {
  const classes = useStyles();
  const [titleInputValue, setTitleInputValue, answers, setAnswers] = props.logic;
  const { t } = useTranslation();

  const handleAnswerInput = id => event => {
    let newArr = [...answers]
    let answer = newArr.find((a) => {
      return a.idChoiceOption === id;
    });
    answer.value = event.target.value
    setAnswers(newArr);
  }

  const handleAlarmChange = id => event => {
    let newArr = [...answers]
    let answer = newArr.find((a) => {
      return a.idChoiceOption === id;
    });
    answer.idAlert = event.target.value;
    setAnswers(newArr);
  }
  const addAnswerHandler = () => {
    const newId = ID();
     setAnswers([
      ...answers,
      {
        idChoiceOption: newId,
        value: "",
        idAlert: 1
      }
    ]);
  }
  const onDeleteAnswer = id => event => {
    var newAnswersArr = answers.filter(answer => answer.idChoiceOption !== id);
    setAnswers(newAnswersArr);

  }




  const generatedAnswerInputs = answers.map((answer, index) => (
    <React.Fragment key={"answerInput" + index}>
      <Grid className='SmallBlock' direction='row' container alignItems="center" item xs={8}>
        <TextValidator
          id={answer.id}
          className={'AnswerInput'}
          value={answer.value}
          onChange={handleAnswerInput(answer.idChoiceOption)}
          placeholder={t('answerOptionPlaceholder')}
          multiline={true}
          rows={1}
          rowsMax={10}
          validators={['required']}
          errorMessages={[t('requiredField')]}
          input={<Input className={'TitleInput2'} />}
        />

      </Grid>
      <Grid item xs={4} container justify='flex-end'>
        <FormControl className='AlarmSelect'>
          <InputLabel>{t('alert')}</InputLabel>
          <Select
            value={answer.idAlert}
            onChange={handleAlarmChange(answer.idChoiceOption)}
            inputProps={{
              name: 'idAlert',
            }}
          >
            <MenuItem value={1}>{t('alertNo')}</MenuItem>
            <MenuItem value={2}>{t('alertYellow')}<div className='circle yellow' /></MenuItem>
            <MenuItem value={3}>{t('alertRed')}<div className='circle red' /></MenuItem>
          </Select>
        </FormControl>
        <IconButton aria-label="Delete" className={'DeleteAnswerButton'} onClick={onDeleteAnswer(answer.idChoiceOption)}>
          <CloseIcon />
        </IconButton>
      </Grid>
    </React.Fragment>
  ))

  return (
    <div>
      <Grid container className='BigBlock'>
        <QuestionTitle titleInputValue={titleInputValue} setTitleInputValue={setTitleInputValue} />
        <Grid item xs={12} className='SmallBlock'>
          <Divider />
        </Grid>
        <Grid item xs={12} className='SmallBlock'>
          <Typography variant='h5'>{t('answerOptionsAdd')}</Typography>
        </Grid>
        <Grid item xs={12} container className='SmallBlock'>
          {generatedAnswerInputs}
          <Button
            variant="contained"
            color="primary"
            className="addBtn containerBtn"
            onClick={addAnswerHandler}
          >
            {t('answerAdd')}
        </Button>
        </Grid>
      </Grid>
    </div>
  )
}



function ValueQuestion(props) {
  const classes = useStyles();
  const [titleInputValue, setTitleInputValue, valueRanges, setValueRanges] = props.logic;
  const [isOverlapping, setIsOverlapping] = useState(false);

  const { t } = useTranslation();


  useEffect(() => {
    ValidatorForm.addValidationRule('isOverlapping', (value) => {
       return !isOverlapping
    });
  }, )

  useEffect(() => {
    var res = false
    console.log(valueRanges)
    for(var x = 0; x < valueRanges.length; x++){
      for(var y = x+1; y < valueRanges.length; y++){
        console.log('inner', y)
        if(x != y){
          //if((valueRanges[x].lowerBoundary <= valueRanges[y].upperBoundary && valueRanges[y].lowerBoundary <= valueRanges[x].upperBoundary )){
          if(Math.max(valueRanges[x].lowerBoundary, valueRanges[y].lowerBoundary) <= Math.min(valueRanges[x].upperBoundary, valueRanges[y].upperBoundary)){
            res = true;
          }
        }
      }
    }
    setIsOverlapping(res);
  }, [valueRanges])
  const addValueRangeHandler = () => {
    const newId = ID();
    setValueRanges([
      ...valueRanges,
      { idQuestionBoundary: ID(), lowerBoundary: '', upperBoundary: '', idAlert: 1 }
    ]);

  }
  const onDeleteValueRange = id => event => {
    var newArr = valueRanges.filter(valueRange => valueRange.idQuestionBoundary !== id);
    setValueRanges(newArr);
  }
  const handleChangeValueFrom = id => event => {
    let newArr = [...valueRanges]
    let range = newArr.find((a) => {
      return a.idQuestionBoundary === id;
    });
    range.lowerBoundary = event.target.value
    setValueRanges(newArr);
  }
  const handleChangeValueTo = id => event => {
    let newArr = [...valueRanges]
    let range = newArr.find((a) => {
      return a.idQuestionBoundary === id;
    });
    range.upperBoundary = event.target.value
    setValueRanges(newArr);
  }

  const handleAlarmChange = id => event => {
    let newArr = [...valueRanges]
    let range = newArr.find((a) => {
      return a.idQuestionBoundary === id;
    });

    range.idAlert = event.target.value
    setValueRanges(newArr);
  }
  const isNotProperInterval = (valueRange) => {
    return (Number(valueRange.lowerBoundary) >= Number(valueRange.upperBoundary) && (valueRange.lowerBoundary !== '' && valueRange.upperBoundary !== ''))
  }
  return (
    <React.Fragment>
      <Grid container className='BigBlock'>
        <QuestionTitle titleInputValue={titleInputValue} setTitleInputValue={setTitleInputValue}>
          {t('valueQuestion')}
        </QuestionTitle>
        <Grid item xs={12} className='SmallBlock'>
          <Divider />
        </Grid>
        <Grid item xs={12} className='SmallBlock'>
          <Typography variant='h5'>{t('addAlarmingRange')}</Typography>
        </Grid>
        {valueRanges.map((valueRange) => {
          return (
            <React.Fragment key={valueRange.idQuestionBoundary}>
              <Grid className='SmallBlock' direction='row' container alignItems="center" item xs={8}>
                <TextValidator
                  id={"rangeFrom" + valueRange.idQuestionBoundary}
                  value={valueRange.lowerBoundary}
                  className='RangeField'
                  onChange={handleChangeValueFrom(valueRange.idQuestionBoundary)}
                  validators={['required', 'isOverlapping']}
                  type='number'
                  error={isOverlapping  || isNotProperInterval(valueRange)}
                  errorMessages={[t('requiredField')]}
                />
                <Typography variant='subtitle1'>-</Typography>
                <TextValidator
                  id={"rangeTo" + valueRange.idQuestionBoundary}
                  className='RangeField'
                  value={valueRange.upperBoundary}
                  type='number'
                  onChange={handleChangeValueTo(valueRange.idQuestionBoundary)}
                  validators={['required', 'isOverlapping']}
                  error={isOverlapping || isNotProperInterval(valueRange)}
                  errorMessages={[t('requiredField')]}
                />
              {isNotProperInterval(valueRange)
                ? <Typography variant = 'caption' style = {{color: 'red'}}>{t('notProperInterval')}</Typography>
                : null}
              </Grid>
              <Grid item xs={4} container justify='flex-end'>
                <FormControl className='AlarmSelect'>
                  <InputLabel htmlFor="filled-age-simple" className='AlarmSelect'>Alert</InputLabel>
                  <Select
                    value={valueRange.idAlert}
                    onChange={handleAlarmChange(valueRange.idQuestionBoundary)}

                    inputProps={{
                      name: 'idAlert',
                      id: 'filled-age-simple',
                    }}
                  >
                    <MenuItem value={1}>{t('alertNo')}</MenuItem>
                    <MenuItem value={2}>{t('alertYellow')}<div className='circle yellow' /></MenuItem>
                    <MenuItem value={3}>{t('alertRed')}<div className='circle red' /></MenuItem>
                  </Select>
                </FormControl>
                <IconButton aria-label="Delete" className={'DeleteValueRangeButton'} onClick={onDeleteValueRange(valueRange.idQuestionBoundary)}>
                  <CloseIcon />
                </IconButton>
              </Grid>

            </React.Fragment>
          );
        }
      )}
      <Grid item xs={12}>{isOverlapping ? <Typography variant='caption' style={{color: 'red'}}>{t('overlappingIntervals')}</Typography> : null}</Grid>
        <Button
          variant="contained"
          color="primary"
          className="addBtn containerBtn"
          onClick={addValueRangeHandler}
        >
          {t('addAlarmingRage')}
        </Button>
      </Grid>
    </React.Fragment>
  )
}

function FileQuestion(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const [titleInputValue, setTitleInputValue, characterLimit, setCharacterLimit] = props.logic;

  const handleCharacterInput = event => {
    setCharacterLimit(event.target.value);
  }
  return (
    <React.Fragment>
      <Grid container className='BigBlock'>
        <QuestionTitle titleInputValue={titleInputValue} setTitleInputValue={setTitleInputValue} />
        <Grid item xs={12} className='SmallBlock'>
          <Divider />
        </Grid>
        <Grid item xs={12} className='SmallBlock'>
          <Typography variant='h5'>{t('fileLimit')}</Typography>
        </Grid>
        <Grid item container alignItems='center' xs={12} className='SmallBlock'>
          <TextValidator
            id={'CharacterLimitInput'}
            className={'CharacterLimitInput'}
            value={characterLimit}
            type={'number'}
            onChange={handleCharacterInput}
            validators={['required']}
            errorMessages={[t('requiredField')]}
          />
          <Typography variant='subtitle1'>{t('files')}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  )

}

function OpenQuestion(props) {
  const classes = useStyles();
  const [titleInputValue, setTitleInputValue, characterLimit, setCharacterLimit] = props.logic;
  const { t } = useTranslation();
  const handleCharacterInput = event => {
    setCharacterLimit(event.target.value);
  }
  return (
    <React.Fragment>
      <Grid container className='BigBlock'>
        <QuestionTitle titleInputValue={titleInputValue} setTitleInputValue={setTitleInputValue} />
        <Grid item xs={12} className='SmallBlock'>
          <Divider />
        </Grid>
        <Grid item xs={12} className='SmallBlock'>
          <Typography variant='h5'>{t('characterLimit')}</Typography>
        </Grid>
        <Grid item container alignItems='center' xs={12} className='SmallBlock'>
          <TextValidator
            id={'CharacterLimitInput'}
            className={'CharacterLimitInput'}
            value={characterLimit}
            type={'number'}
            onChange={handleCharacterInput}
            validators={['required']}
            errorMessages={[t('requiredField')]}
          />
          <Typography variant='subtitle1'>{t('characters')}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}


function CreateQuestion(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const questionTypes = ['Single choice', 'Multiple choice', 'Value', 'Open', 'File'];
  const [question, setQuestion] = useState();
  const [selectedIndex, setSelectedIndex] = useState(-1);
  const formRef = useRef('form')
  const [titleInputValue, setTitleInputValue] = useState('');
  const [answers, setAnswers] = useState([
    {
      idChoiceOption: ID(),
      value: "",
      idAlert: 1
    }
  ]);

  const choiceQuestionLogic = [titleInputValue, setTitleInputValue, answers, setAnswers];

  const [valueRanges, setValueRanges] = useState([
    {
      idQuestionBoundary: ID(),
      lowerBoundary: '',
      upperBoundary: '',
      idAlert: 1,
    }
  ]);
  const valueQuestionLogic = [titleInputValue, setTitleInputValue, valueRanges, setValueRanges];

  const [limit, setLimit] = useState(0);
  const openQuestionLogic = [titleInputValue, setTitleInputValue, limit, setLimit];
  const { idQuestionnaire } = props.match.params;

  useEffect(() => {
    if (props.question) {
      setQuestion(props.question);
      setSelectedIndex(questionTypes.indexOf(getQuestionTypeLabelById(props.question.idQuestionType)));
      setTitleInputValue(props.question.questionTitle);
      setAnswers(props.question.choiceOptions);
      setValueRanges(props.question.questionBoundaries);
      setLimit(props.question.limit && 0);
    }
  }, [props.question])

  const onSelectType = (index) => event => {
    setSelectedIndex(index);
    if (index === 1 || index === 0) {
      setAnswers([
        {
          idChoiceOption: ID(),
          value: "",
          idAlert: 1
        }
      ])
    } else {
      setAnswers([])
    }

    setTitleInputValue('')
    if (index === 2) {
      setValueRanges([
        {
          idQuestionBoundary: ID(),
          lowerBoundary: '',
          upperBoundary: '',
          idAlert: 1
        }
      ])
    } else {
      setValueRanges([])
    }
    setLimit('');
  }

  const createQuestion = async () => {
    // const res = await formRef.current.isFormValid()
    // console.log(formRef)
    let newQuestion = {
      idQuestion: !!question ? question.idQuestion : ID(),
      questionType: questionTypes[selectedIndex],
      idQuestionType: getQuestionTypeIdByLabel(questionTypes[selectedIndex]),
      questionTitle: titleInputValue,
      limit: limit || 0,
      isNew: !!question ? question.isNew : true,
      choiceOptions: [
        ...answers
      ],
      questionBoundaries: [
        ...valueRanges
      ],
    };
    if (idQuestionnaire) {
      props.editQuestion(newQuestion);
      props.history.push(`/questionnaires/EditQuestionnaire/${idQuestionnaire}`);
    } else {
      props.createQuestion(newQuestion);
      props.history.push("AddQuestionnaire");
    }

  };

	const goBack = () => {
		props.history.goBack();
  }

  return (
    <React.Fragment>
      <ValidatorForm ref = {formRef} onSubmit={createQuestion} style={{height: '100%'}}>
        <Grid container direction="column" style={{ height: '5%' }}>
          <h1>{question ? t('questionEdit') : t('questionCreate')}</h1>
        </Grid>
        <Grid container style={{ height: '90%', padding: '0.5em 0' }}>
          <Paper className='container white questions'>
            <Grid container style={{ margin: '10px 5%', width: 'auto' }}>
              <Grid container justify={question ? 'flex-start' : 'space-between'} alignItems='center' item xs={12} className={'BigBlock'}>
                <h5 className={classes.questionHeader}>{question ? t('typeOfQuestion') : t('pickTypeOfQuestion')}:</h5>
                {!question ? questionTypes.map((label, index) => (
                  <Button
                    key={index}
                    variant='contained'
                    className={classes.typeButton}
                    color={selectedIndex === index ? 'primary' : 'secondary'}
                    onClick={onSelectType(index)}
                    >
                    {t(label)}
                  </Button>
                )) : (<h5 className={classes.questionHeader} style={{marginLeft: '0.5em'}}>{questionTypes[selectedIndex]}</h5>)}
              </Grid>
              <Grid item xs={12}>
                <Divider />
                {selectedIndex === 0 ? <ChoiceQuestion multipleChoice={false} logic={choiceQuestionLogic} /> : null}
                {selectedIndex === 1 ? <ChoiceQuestion multipleChoice={true} logic={choiceQuestionLogic} /> : null}
                {selectedIndex === 2 ? <ValueQuestion logic={valueQuestionLogic} /> : null}
                {selectedIndex === 3 ? <OpenQuestion logic={openQuestionLogic} /> : null}
                {selectedIndex === 4 ? <FileQuestion logic={openQuestionLogic} /> : null}
              </Grid>

            </Grid>
          </Paper>
        </Grid>
        <Grid container justify="space-between" style={{ height: '5%' }}>
          <BackButton />
          {selectedIndex !== -1 &&
            <Button variant="contained" color="primary" type="submit" endIcon={<SaveIcon />}>
              {t('save')}
            </Button>
          }
        </Grid>
      </ValidatorForm>
    </React.Fragment>
  )
}


const mapStateToProps = state => ({
  question: state.questionnaires.editQuestion,
  questionnaire: state.questionnaires.questionnaire,
})

const mapDispatchToProps = dispatch => ({
  createQuestion: question => dispatch(createQuestion(question)),
  editQuestion: question => dispatch(editQuestion(question)),
})

export default connect(mapStateToProps, mapDispatchToProps)(CreateQuestion);
