import React from 'react';
import './styles/Header.css'
import PropTypes from "prop-types";
import UserAvatar from 'Icons/UserAvatar.svg';
import IconButton from '@material-ui/core/IconButton';
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import AppBar from "@material-ui/core/AppBar";
import { withStyles } from "@material-ui/core/styles";
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { logout } from 'Api/AuthorizationApi';
import { parseJwt } from 'Utils/tokenParser';
import LocalStorageService from 'Utils/LocalStorageService';
const localStorageService = LocalStorageService.getService();

const styles = theme => ({
  root: {
    width: "100%",
  },
  appRoot: {
    color: "rgb(191, 197, 210)",
    flexDirection: "row",
    boxShadow: 'none',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  },
  title: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block"
    }
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#F5F5F5',
    borderRadius: 20,
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    }
  },
  searchIcon: {
    width: theme.spacing(9),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  inputRoot: {
    color: "inherit",
    width: "100%"
  },
  inputInput: {
    paddingTop: theme.spacing(),
    paddingRight: theme.spacing(),
    paddingBottom: theme.spacing(),
    paddingLeft: theme.spacing(10),
    transition: theme.transitions.create("width"),
    width: "300px",
  },
});


class Header extends React.Component {
  state = {
    open: false,
    isLanguageMenuOpen: false,
  }


  handleToogle = () => {
    this.setState(state => ({open: !state.open, isLanguageMenuOpen: false}))
  }

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ open: false, isLanguageMenuOpen: false });
  };

  openLanguageMenu = () => {
    this.setState({isLanguageMenuOpen: true});
  }

  handleToggleLanguageChange = (laguage) => {
    this.props.i18n.changeLanguage(laguage);
    this.setState({ open: false });
  }

  handleLogout = () => {
    this.props.logout();
  }


  render(){
    const { open, isLanguageMenuOpen } = this.state;
    const { classes, t  } = this.props;


    return(
      <div className={'accountMenu'} style={{width: '100%'}}>
      <AppBar position="static" classes={{
        root: classes.appRoot,
      }}>
        <div className={classes.grow} />
        <div className={"sectionProfile"}>
        <img src={UserAvatar} placeholder='userAvatar'/>
        <span className='userName'>{this.props.userName}</span>
        <IconButton 
        buttonRef={node => {
          this.anchorEl = node
        }}
        aria-label="More"
        aria-owns={open ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        className="dropdown-icon"
        onClick={this.handleToogle} >
        <i className="material-icons">
          arrow_drop_down
        </i>
        </IconButton>
        </div>
        <Popper open={open} anchorEl={this.anchorEl} transition disablePortal>
          {({TransitionProps, placement}) => (
            <Grow
            {...TransitionProps}
            id="menu-list-grow"
            style={{
              transformOrigin: placement === "bottom" ? "center top" : "center bottom"
            }}>
              <Paper>
                <ClickAwayListener onClickAway={this.handleClose}>
                  {isLanguageMenuOpen ? 
                  <MenuList>
                    <MenuItem onClick={() => this.handleToggleLanguageChange('pl-PL')} className="dropdown">
                      PL
                    </MenuItem><MenuItem onClick={() => this.handleToggleLanguageChange('en-US')} className="dropdown">
                      EN
                    </MenuItem>
                  </MenuList>:
                  <MenuList>
                    <MenuItem onClick={this.openLanguageMenu} className="dropdown">
                    <i className="material-icons dropdown">
                      translate
                    </i>
                      {t('language')}
                    </MenuItem>
                    <MenuItem onClick={this.handleLogout} className="dropdown">
                    <i className="material-icons dropdown">
                      keyboard_tab
                    </i>
                      {t('logout')}
                    </MenuItem>
                  </MenuList>
                  }
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
        </AppBar>
      </div>
    )
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  userName: state.authorization.UserName,
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout()),
})


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withTranslation()(Header)));
