import React from 'react';
import Logo_img from 'Images/Logo_img.png';


import './styles/Logo.css';


const Logo = () => (
    <div className='logo'>
        <img src={Logo_img} placeholder='Logo_img'/>
        <span>NIKardCare</span>
    </div>
);

export default Logo;
