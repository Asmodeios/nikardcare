import React from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import '../styles/Alarmblock.css';
import { withTranslation } from 'react-i18next';

const AlarmBlock = ({ alarmType, name, date, t, handleDismiss, handleOpen }) => (
  <Grid item xs={12} className="block" style={{height: '5.6rem'}}>
    <Grid container alignItems="center" style={{height: '5.6rem'}}>
      <Grid container item xs={2} justify="center">
        <div className={`${alarmType} alertCircle`}></div>
      </Grid>
      <Grid item xs={7}>
        <span className="name">{name}</span>
        <br />
        <span className="date">{date}</span>
      </Grid>
      <Grid container item xs={3} justify="center">
        <Button variant="outlined" color="primary" onClick={handleOpen} style={{width: '50%'}}>
          {alarmType === 'grey' ? t('profile') : t('more')}
        </Button>                                   
      </Grid>
    </Grid>
  </Grid>
)

export default withTranslation()(AlarmBlock);