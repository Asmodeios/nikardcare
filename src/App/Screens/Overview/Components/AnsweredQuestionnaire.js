import React, { useEffect, useState } from 'react';
import {
  Typography,
  Divider,
  Paper,
  Button,
  Grid,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  IconButton,
} from '@material-ui/core';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import BackButton from 'Shared/components/BackButton';
import { makeStyles } from '@material-ui/core/styles';
import Alert from 'Shared/components/Alert';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import CheckBoxOutlinedIcon from '@material-ui/icons/CheckBoxOutlined';
import RadioButtonUncheckedOutlinedIcon from '@material-ui/icons/RadioButtonUncheckedOutlined';
import RadioButtonCheckedOutlinedIcon from '@material-ui/icons/RadioButtonCheckedOutlined';
import { 
  getAnsweredQuestionnaire,
  dismissAnswer,
  getFile
 } from 'Api/AnswerApi';
import { useTranslation } from 'react-i18next';
import PendingScreen from 'Shared/components/PendingScreen';
import InsertDriveFileOutlinedIcon from '@material-ui/icons/InsertDriveFileOutlined';
import GetAppIcon from '@material-ui/icons/GetApp';
import { saveAs } from 'file-saver';

const useStyles = makeStyles(theme => ({
	header: {
		height: '5%',
  },
  paper: {
		padding: '2em 10em',
		overflowY: 'auto',
		height: '100%'
	},
	paperContainer: {
		padding: '0.5em 0',
		height: '90%',
  },
  openAnswer: {
    border: '1px solid var(--main-container-text-color)',
    borderRadius: '6px',
    padding: '0.5em',
    width: '100%',
  },
  divider: {
    margin: '1em 0',
    height: '1px',
    width: '100%',
  },
  choiceAnswer: {
    marginBottom: '0.5em',
  },
  questionHeader: {
    marginBottom: '1em',
  },
  answerLabel: {
    marginRight: '0.5em',
    fontSize: '1.1rem',
    width: '90%',
    wordBreak: 'break-word'
  },
  questionType: {
    fontSize: '0.8rem'
  },
  dialogInput: {
    width: '100%',
  },
}))

function OpenQuestion(props) {

  const classes = useStyles();
  const { t } = useTranslation();
  const { title, value } = props.answer;

  return (
    <>
      <Grid item container xs={12} justify="center">
        <Grid item container xs={12} justify="center">
          <h2>{title}</h2>
        </Grid>
        <Grid item container xs={12} justify="center" className={classes.questionHeader}>
          <Typography component="span" className={classes.questionType}>
            {t('Open question')}
          </Typography>
        </Grid>
        <Grid item container xs={6} justify="center" >
          <Typography component="span" className={classes.openAnswer}>
            {value}
          </Typography>
        </Grid>
      </Grid>
      <Divider className={classes.divider} variant="middle"/>
    </>
  )
}

function ValueQuestion(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const { title, value, idAlert } = props.answer;
  return (
    <>
      <Grid item container xs={12} justify="center">
        <Grid item container xs={12} justify="center" >
          <h2>{title}</h2>
        </Grid>
        <Grid item container xs={12} justify="center" className={classes.questionHeader}>
          <Typography component="span" className={classes.questionType}>
            {t('Value question')}
          </Typography>
        </Grid>
        <Grid item container xs={6}>
          <Grid item container xs={12} justify="center" alignItems="center" className={classes.choiceAnswer}>
            <Grid item container xs={1} alignItems="center">
              <RadioButtonCheckedOutlinedIcon />
            </Grid>
            <Grid item container xs>
              <Typography component="span" className={classes.answerLabel}>
                {value}
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Alert idAlert={idAlert}/>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Divider className={classes.divider} variant="middle"/>
    </>
  )
};


function FileQuestion(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const { title, files } = props.answer;

  const handleDownload = async (idFile, fileName) => {

    const response = await getFile(props.idAnswer, idFile);
    saveAs(response.data, fileName);

  }

  return (
    <>
      <Grid item container xs={12} justify="center">
        <Grid item container xs={12} justify="center" >
          <h2>{title}</h2>
        </Grid>
        <Grid item container xs={12} justify="center" className={classes.questionHeader}>
          <Typography component="span" className={classes.questionType}>
            {t('File')}
          </Typography>
        </Grid>
        <Grid item container xs={6}>
          {files && files.length > 0 ? files.map(file => (
            <Grid key={file.idFile} item container xs={12} justify="center" alignItems="center" className={classes.choiceAnswer}>
              <Grid item container xs={1} alignItems="center">
                <InsertDriveFileOutlinedIcon />
              </Grid>
              <Grid item container xs>
                <Typography component="span" className={classes.answerLabel}>
                  {file.fileName}
                  <IconButton onClick={() => handleDownload(file.idFile, file.fileName)}>
                    <GetAppIcon />
                  </IconButton>
                </Typography>
              </Grid>
            </Grid>
          )):
            <Grid item container xs={12} justify="center" alignItems="center" className={classes.choiceAnswer}>
                <Typography component="span" className={classes.answerLabel} style={{textAlign: 'center'}}>
                  {t('noFiles')}
                </Typography>
            </Grid>
         }
          
        </Grid>
      </Grid>
      <Divider className={classes.divider} variant="middle"/>
    </>
  )
};

function SingleChoice(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const { title, choiceOptions } = props.answer;
  return (
    <>
      <Grid item container xs={12} justify="center">
        <Grid item container xs={12} justify="center" >
          <h2>{title}</h2>
        </Grid>
        <Grid item container xs={12} justify="center" className={classes.questionHeader}>
          <Typography component="span" className={classes.questionType}>
            {t('Single choice')}
          </Typography>
        </Grid>
        <Grid item container xs={6}>
          {choiceOptions.map(option => (
            <Grid key={option.idChoiceOption} item container xs={12} justify="center" alignItems="center" className={classes.choiceAnswer}>
              <Grid item container xs={1} alignItems="center">
                {option.selected ? <RadioButtonCheckedOutlinedIcon /> : <RadioButtonUncheckedOutlinedIcon /> }
              </Grid>
              <Grid item container xs>
                <Typography component="span" className={classes.answerLabel}>
                  {option.title}
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Alert idAlert={option.idAlert}/>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Divider className={classes.divider} variant="middle"/>
    </>
  )
};

function MultipleChoice(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const { title, choiceOptions } = props.answer;
  return (
    <>
      <Grid item container xs={12} justify="center">
        <Grid item container xs={12} justify="center" >
          <h2>{title}</h2>
        </Grid>
        <Grid item container xs={12} justify="center" className={classes.questionHeader}>
          <Typography component="span" className={classes.questionType}>
            {t('Multiple choice')}
          </Typography>
        </Grid>
        <Grid item container xs={6}>
          {choiceOptions.map(option => (
            <Grid key={option.idChoiceOption} item container xs={12} justify="center" alignItems="center" className={classes.choiceAnswer}>
              <Grid item container xs={1} alignItems="center">
                {option.selected ? <CheckBoxOutlinedIcon /> : <CheckBoxOutlineBlankIcon /> }
              </Grid>
              <Grid item container xs>
                <Typography component="span" className={classes.answerLabel}>
                  {option.title}
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Alert idAlert={option.idAlert}/>
              </Grid>
            </Grid>
          ))}
        </Grid>
      </Grid>
      <Divider className={classes.divider} variant="middle"/>
    </>
  )
};

function AnsweredQuestionnaire(props) {
  const classes = useStyles();
  const { t } = useTranslation();
  const [data, setData] = useState({});
  const { idAnswer, mode } = props.match.params;
  const [isLoading, setIsLoading] = useState(false);
  const [dismiss, setDismiss] = useState('');
  const [open, setOpen] = useState(false);
  const handleDismissChange = event => {
    const { value } = event.target;
    setDismiss(value);
  }

  const handleDismissSubmit = async () => {
    const response = await dismissAnswer({
      comment: dismiss,
      idQuestionnaireAnswer: idAnswer
    });
    if (response && response.status >= 200 && response.status < 300) {
      props.history.goBack();
    };
  };
  
  const handleDismissOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    fetchAnswers();
  }, []);

  const fetchAnswers = async () => {
    setIsLoading(true);    
    const response = await getAnsweredQuestionnaire(idAnswer);
    if (response && response.status === 200) {
      setData(response.data);
      setIsLoading(false);
    };
  }

  const { 
    singleChoiceAnswers: single, 
    multipleChoiceAnswers: multiple,
    valueAnswers: value,
    fileAnswers: file,
    openAnswers: openQuestions,
    patientName,
    questionnaireTitle: title,
  } = data;

  if (isLoading) {
    return <PendingScreen />;
  };

  return (
    <>
      <Grid container justify="space-between" className={classes.header}>
				<Typography component="div">
					<h1>{patientName}</h1>
				</Typography>
			</Grid>
      <Typography component="div" className={classes.paperContainer}>
				<Paper className={classes.paper}>
					<Grid container style={{height: '100%'}} alignContent="flex-start">
            {single && single.length > 0 && single.map((single, i) => (
              <SingleChoice key={i} answer={single} />
            ))}
            {openQuestions && openQuestions.length > 0 && openQuestions.map((open, i) => (
              <OpenQuestion key={i} answer={open} />
            ))}
            {multiple && multiple.length > 0 && multiple.map((multiple, i) => (
              <MultipleChoice key={i} answer={multiple} />
            ))}
            {value && value.length > 0 && value.map((value, i) => (
              <ValueQuestion key={i} answer={value} />
            ))}
            {file && file.length > 0 && file.map((file, i) => (
              <FileQuestion idAnswer={idAnswer} key={i} answer={file} />
            ))}
          </Grid>
				</Paper>
			</Typography>
      <Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
          <BackButton />
          {mode === 'dismiss' &&
            <Button color="primary" variant="contained" onClick={handleDismissOpen}>
              {t('dismiss')}
            </Button>
          }
      </Grid>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-dismiss"
        aria-describedby="alert-dialog-description"
        fullWidth
      >
        <ValidatorForm onSubmit={handleDismissSubmit}>
          <DialogTitle id="alert-dialog-dismiss">{t('dismissTitle')}</DialogTitle>
          <DialogContent>
            <TextValidator 
              id="dismiss" 
              name="dismiss" 
              label={t('dismiss')}
              validators={['required']}
              errorMessages={[t('requiredField')]}
              value={dismiss}
              onChange={handleDismissChange}
              className={classes.dialogInput}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              {t('cancel')}
            </Button>
            <Button color="primary" type="submit">
              {t('save')}
            </Button>
          </DialogActions>
        </ValidatorForm>
      </Dialog>
    </>
  )
}


export default AnsweredQuestionnaire;