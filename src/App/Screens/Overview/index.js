import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Grid,
  Dialog,
  DialogTitle,
  Button,
  DialogActions,
  DialogContent
} from '@material-ui/core';
import AlarmBlock from './Components/AlarmBlock';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { makeStyles } from '@material-ui/core/styles';
import './styles/Overview.css';
import { getAlertTypeLabelById } from 'Utils/Dictionary';
import { format } from 'date-fns'

const useStyles = makeStyles(theme => ({
  dialogInput: {
    width: '100%',
  },
}));

function Overview(props) {
  const { t } = useTranslation();
  const classes = useStyles();

  const { alerted, missed } = props;

  return (
    <React.Fragment>
      <Grid container style={{height: '100%'}} spacing={4}>
          <Grid item container xs={6} style={{height: '100%'}}>
            <Grid item container xs={12} style={{height: '100%'}}>
              <Grid item xs={12}>
                <h2 style={{height: '5%'}}>{t('alerts')}</h2>
              </Grid>
              {alerted && alerted.length > 0 && (
                <Grid item container xs={12} style={{height: '95%', overflowY: 'auto', alignContent: 'flex-start'}}>
                  {alerted.map((alert, i) => (
                    <AlarmBlock 
                      key={i}
                      name={alert.patientName} 
                      date={format(new Date(alert.answeredAt), 'dd/MM/yyyy')}
                      alarmType={getAlertTypeLabelById(alert.idAlert).toLowerCase()} 
                      handleOpen={() => props.history.push(`/answers/${alert.idAnswer}/dismiss`)}
                    />
                  ))}
                </Grid>
              )}
            </Grid>	
          </Grid>
          <Grid item xs={6} style={{height: '100%'}}>
            <Grid item container xs={12} style={{height: '100%'}}>
              <Grid item xs={12} style={{height: '5%'}}>
                <h2>{t("alertsNotInTime")}</h2>
              </Grid>
              {missed && missed.length > 0 && (
                <Grid item container xs={12} style={{height: '95%', overflowY: 'auto', alignContent: 'flex-start'}}>
                  {missed.map((miss, i) => (
                    <AlarmBlock
                      key={i} 
                      name={miss.patientName}
                      date={format(new Date(miss.missedAt), 'dd/MM/yyyy')}
                      alarmType='grey'
                      handleOpen={() => props.history.push(`/patients/${miss.idPatient}/calendar`)}
                    />
                  ))}
                </Grid>
              )}
            </Grid>
          </Grid>
      </Grid>
    </React.Fragment>
  )
};

export default Overview;