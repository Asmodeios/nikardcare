import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import UserBlock from 'Shared/components/UserBlock';
import './styles/PatientList.css';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PatientModal from "./Components/PatientModal";
import { getAllPatients, addPatient } from 'Api/PatientApi';
import LinearProgress from '@material-ui/core/LinearProgress';
import Drawer from "@material-ui/core/Drawer";
import FilterList from './Components/FilterList'
import Icon from '@material-ui/core/Icon';
import { withTranslation } from 'react-i18next';
import { withStyles } from '@material-ui/core/styles';
import PendingScreen from 'Shared/components/PendingScreen';

const styles = {
  patientList: {
    overflowY: 'auto',
    height: "100%",
    fontFamily: 'var(--main-font-Source)',
    backgroundColor: 'white',
    alignContent: 'flex-start',
  },
};


class PatientList extends Component {
  
  state = {
    open: false,
    error: '',
    patients: [],
    loading: true,
    drawer: false,
  }

  componentDidMount() {
    this.fetchPatients();
  }

 

  async fetchPatients(filter) {
    const response = await getAllPatients(filter);
    if (response && response.status >= 200 && response.status <= 299) {
      this.setState({ patients: response.data });
    }
  }

  handleAddPatient = async (patient) => {
    const response = await addPatient(patient);
    if (response && response.status >= 200 && response.status <= 299) {
      this.fetchPatients();
    }
    this.handleClose();
  }


  applyFilter = filter => {
    this.setState({ drawer: false })
    this.fetchPatients(filter);
  }

  toggleDrawer = open => event => {
    this.setState({ drawer: open });
  }

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render(){
    const { t, classes } = this.props;
    const { patients, loading } = this.state;

    return (
    <React.Fragment>
      <Grid container className="patientlist-header" style={{height: '5%'}}>
        <Grid item xs={12}>
          <h1>{t('patients')}</h1>
        </Grid>
      </Grid>
      <Grid container direction="row" justify="flex-end" className="patientlist-buttons" style={{height: '5%'}}>
        <Grid item>
          <Button 
            variant="contained" 
            color="primary" 
            className="addBtn containerBtn" 
            onClick={this.handleClickOpen}
            >
            <i className="material-icons">group_add</i>{t('addPatient')}
          </Button>
        </Grid>
        <Dialog 
        maxWidth="lg" 
        fullWidth={true}
        open={this.state.open} 
        onClose={this.handleClose}
        aria-labelledby="max-width-dialog-title"
        >
          <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>{t('patientData')}</DialogTitle>
          <DialogContent style={{overflowY: 'hidden'}}>
            <PatientModal handleAddPatient={this.handleAddPatient}/>
          </DialogContent>
        </Dialog>
        <Grid item xs={1} style={{flexBasis: '0%'}}>
          <Button 
          variant="contained"
          color="secondary" 
          className="filterBtn containerBtn"
          onClick={this.toggleDrawer(true)}
          startIcon={<Icon>tune</Icon>}
          >
            {t('filter')}
          </Button>
          <Drawer anchor="right" open={this.state.drawer} onClose={this.toggleDrawer(false)}>
            <FilterList applyFilter={this.applyFilter}/>
          </Drawer>
        </Grid>
      </Grid>
      <div style={{height: '90%'}}>
        <Grid container className={classes.patientList}>
          {patients.length > 0 && patients.map((p, i) => (
            <UserBlock name={p.patientName} phoneNumber={p.phoneNumber} id={p.idPatient} key={i} gridProps={{xs: 6, lg: 4, xl: 4}}/>
          ))}            
        </Grid>
      </div>
        
    </React.Fragment>
    );
  }
}




export default withTranslation()(withStyles(styles)(PatientList))