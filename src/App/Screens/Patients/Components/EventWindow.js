import React, {useState} from 'react';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ClockIcon from '@material-ui/icons/AccessTime';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import {KeyboardTimePicker} from "@material-ui/pickers";
import MomentUtils from '@date-io/moment';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import {deleteEvent, addEvent, assignQuestionnaire} from 'Api/PatientApi';
import QuestionnaireTab from './QuestionnaireTab'
import Snackbar from '@material-ui/core/Snackbar';
import moment from 'moment'
import {getQuestionnaireStatusById} from 'Utils/Dictionary'
import {withTranslation} from 'react-i18next';

const styles = theme => ({
  grow: {
    flexGrow: 1
  },
  errorSnackbar:{
    backgroundColor: theme.palette.error.dark,
  },
  EnabledButtonStyle: {
    margin: '5px 25px 5px 5px',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: '#2E5BFF',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "white",
    backgroundColor: "#2E5BFF",
    "&:hover": {
      backgroundColor: "white",
      borderColor: '#2E5BFF',
      color: '#2E5BFF'
    }
  },
  DeleteButtonStyle: {
    margin: '5px 5px 5px 5px',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: '#f62b2b',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "white",
    backgroundColor: "#f62b2b",
    "&:hover": {
      backgroundColor: '#e40a0a'
    }
  },
  DisabledButtonStyle: {
    margin: '5px 25px 5px 5px',
    borderStyle: 'solid',
    borderWidth: '1px',
    borderColor: 'rgb(191, 197, 210)',
    borderRadius: 15,
    letterSpacing: 1,
    fontWeight: "normal",
    color: "rgb(191, 197, 210)",
    backgroundColor: "white",
    "&:hover": {
      borderColor: 'rgb(191, 197, 210)'
    }
  },
  textField: {
    width: 75,
    height: '2rem',
    margin: '2px 5px 2px 5px'
  }
})

const DialogTitle = withStyles(theme => ({
  root: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing(1),
    background: '#2E5BFF',
    minWidth: 250
  },
  h5: {
    color: 'white',
    margin: '2px',
    marginLeft: '5px'
  },
  subtitle1: {
    color: 'white',
    marginLeft: '5px'
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: 'white'
  }
}))(props => {
  const {children, classes, onClose, time} = props;
  return (<MuiDialogTitle disableTypography={true} className={classes.root}>
    <Typography variant="h5" className={classes.h5}>{children}</Typography>
    <Typography variant="subtitle1" className={classes.subtitle1}>{time}</Typography>
    {
      onClose
        ? (<IconButton aria-label="Close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon/>
        </IconButton>)
        : null
    }
  </MuiDialogTitle>);
});

const VisitTab = withStyles(styles)(withTranslation()(props => {

  const {
    handleTimeToChange,
    handleTimeFromChange,
    timeFromValue,
    descriptionValue,
    handleInput,
    classes,
    t
  } = props;
  return (<div style={{
      flexGrow: 1,
      margin: '15px'
    }}>

    <Grid container spacing={2}>
      <Grid item xs={12}>
        <ClockIcon style={{
            position: 'relative',
            top: '7px',
            marginRight: '5px'
          }}/>

        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardTimePicker ampm={false} autoOk={true} mask='__:__' placeholder='12:00' value={timeFromValue} onChange={date => handleTimeFromChange(date)}/>
        </MuiPickersUtilsProvider>
      </Grid>

      <Grid item xs={12}>
        <Typography variant='h5'>
          {t('description')}
        </Typography>
        <TextField id="descriptionValue" className={classes.textDescription} value={descriptionValue} onChange={handleInput} fullWidth={true} variant="outlined" multiline={true} rows={5} rowsMax={8}/>
      </Grid>
    </Grid>
  </div>)
}));
const DialogContent = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

class EventCreationDialog extends React.Component {
  constructor(props) {
    super(props);
    this.classes = props.classes;
    this.t = props.t;
    if (this.props.selectedEvent !== undefined) {
      const startTime = props.selectedEvent.start;
      if(this.props.selectedEvent.classNames[0] === 'visit'){
        this.state = {
          questionnaire: false,
          visit: true,
          open: this.props.open,
          timeFromValue: startTime,
          descriptionValue: this.props.selectedEvent.extendedProps.description,
          openAlert: false,
          alertText: '',
          selectedDate: props.selectedEvent.start,
          intervalInput:1,
          selectedQuestionnaire: -1,
          isInterval: false,
          numOfTimes: 1,
          availableFor: 1
        }
      } else if (this.props.selectedEvent.classNames[0] === 'questionnaire'){
        this.state = {
          questionnaire: true,
          visit: false,
          open: this.props.open,
          timeFromValue: startTime,
          descriptionValue: this.props.selectedEvent.extendedProps.description,
          openAlert: false,
          alertText: '',
          selectedDate: props.selectedEvent.start,
          intervalInput:1,
          selectedQuestionnaire: Number(props.selectedEvent.id),
          isInterval: false,
          numOfTimes: 1,
          availableFor: 1
        }
      }

    } else {
      this.state = {
        questionnaire: true,
        visit: false,
        open: this.props.open,
        timeFromValue: props.selectedDate.setHours(12),
        openAlert: false,
        descriptionValue: '',
        alertText: '',
        selectedDate: props.selectedDate,
        intervalInput: 1,
        selectedQuestionnaire: -1,
        isInterval: false,
        numOfTimes: 1,
        availableFor: 1
      }
    }
  }
  setAvailableFor = (num) => {
    this.setState({availableFor: num});
  }
  setNumOfTimes = (num) => {
    this.setState({numOfTimes: num})
  }
  setIntervalInput = (arg) => {
    this.setState({intervalInput: arg})
  }
  setSelectedQuestionnaire = (arg) => {
    this.setState({selectedQuestionnaire: arg})
  }
  setIsInterval = (arg) => {
    this.setState({isInterval: arg});
  }

  handleTimeFromChange = (date) => {
    this.setState({timeFromValue: date})
  }

  handleTimeInput = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleClose = () => {
    this.props.action();
  };

  handleChangeToQuestionnaire = () => {
    this.setState({questionnaire: true, visit: false})
  }

  handleChangeToVisit = () => {
    this.setState({questionnaire: false, visit: true, expand: false})
  }

  generateId = () => {
    return '_' + Math.random().toString(36).substr(2, 9);
  }

  handleOpenAlert = (text) => {
    this.setState({openAlert: true, alertText: text});
  }

  handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    this.setState({openAlert: false});
  }
  handleDelete = () => {
    this.props.selectedEvent
      ? this.deleteEvent()
      : this.handleClose()
  }
  deleteEvent = () => {
    if (this.props.selectedEvent !== undefined) {
      deleteEvent(this.props.idPatient, this.props.selectedEvent.id).then((response) => {
        if (response.status === 204) {
          this.handleClose()
        } else {
          throw new Error(response.statusText)
        }
      }).catch(error => console.log(error));
    }
  }
  handleChangeTimeFrom = (arg) => {
    this.setState({timeFromValue: arg})
  }
  saveVisit = () => {
    let eventDate = moment(this.state.selectedDate)
    let startingDate = moment(this.state.timeFromValue)
    eventDate.set('hour', 0)
    eventDate.set('minute', 0)
    eventDate.add(startingDate.utcOffset(), 'm')

    var newevent = {
      idPatient: this.props.idPatient,
      description: this.state.descriptionValue,
      visitDate: eventDate.toISOString(),
      timeFrom: startingDate.utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]"),
    }
    if(startingDate.isBefore(moment())){
      this.handleOpenAlert(this.t('cannotCreateEventBefore'));
    }else if (this.state.descriptionValue === '') {
      this.handleOpenAlert(this.t('errorEmptyDescription'));
    } else {
      if (this.props.selectedEvent !== undefined) {
        deleteEvent(this.props.idPatient, this.props.selectedEvent.id).then(response => console.log(response)).catch(err => console.log(err));
      }
      addEvent(Number(this.props.idPatient), newevent).then(response => this.handleClose()).catch(err => console.log(err));
    }

  };
  saveQuestionnaire = () => {
    const newObj = {
      idPatient: this.props.idPatient,
      idQuestionnaire: this.state.selectedQuestionnaire,
      intervalDays: this.state.intervalInput,
      numberOfTimes: this.state.numOfTimes,
      availableFor: this.state.availableFor,
      startDate: moment(this.state.timeFromValue).utc().format("YYYY-MM-DDTHH:mm:ss.SSS[Z]"),
    }
    assignQuestionnaire(this.props.idPatient, newObj).then((response) => {
      this.handleClose()
      console.log(response)
    }).catch((err) => {
      console.log(err)
      this.handleOpenAlert('Something went wrong')
    })

  }
  render() {


    const {t} = this.props;
    const QuestionnaireTabLogic = {
      isInterval: this.state.isInterval,
      intervalInput: this.state.intervalInput,
      selectedQuestionnaire: this.state.selectedQuestionnaire,
      setSelectedQuestionnaire: this.setSelectedQuestionnaire,
      setIsInterval: this.setIsInterval,
      setIntervalInput: this.setIntervalInput,
      setNumOfTimes: this.setNumOfTimes,
      numOfTimes: this.state.numOfTimes,
      availableFor: this.state.availableFor,
      setAvailableFor: this.setAvailableFor,
      startTime: this.state.timeFromValue,
      setTime: this.handleChangeTimeFrom
    }

    return (<div style={{}}>
      <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={this.state.open} fullWidth={true} maxWidth={this.state.questionnaire
          ? 'lg'
          : 'sm'} style={{
          minWidth: '500px'
        }}>
        <DialogTitle id="customized-dialog-title" onClose={this.handleClose} time={moment(this.state.selectedDate).format('D/MM/YYYY')}>
          {this.props.selectedEvent
            ? this.props.selectedEvent.extendedProps.questionnaireStatus
              ? `${this.props.selectedEvent.title} (${t(getQuestionnaireStatusById(this.props.selectedEvent.extendedProps.questionnaireStatus))})`
              : t('visit')
            : t('event')}
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>

            <Grid item xs={12}>
              <Button disabled={!this.state.questionnaire && this.props.disabled} onClick={this.handleChangeToQuestionnaire} className={this.state.questionnaire
                  ? this.classes.EnabledButtonStyle
                  : this.classes.DisabledButtonStyle}>
                {t('questionnaire')}
              </Button>
              <Button disabled={!this.state.visit && this.props.disabled} onClick={this.handleChangeToVisit} className={this.state.visit
                  ? this.classes.EnabledButtonStyle
                  : this.classes.DisabledButtonStyle}>
                {t('visit')}
              </Button>
            </Grid>
            {
              this.state.visit
                ? <VisitTab handleInput={this.handleTimeInput} handleTimeFromChange={this.handleTimeFromChange} handleTimeToChange={this.handleTimeToChange} timeFromValue={this.state.timeFromValue} descriptionValue={this.state.descriptionValue}/>
                : null
            }
            {
              this.state.questionnaire
                ? <QuestionnaireTab selectedDate={this.props.selectedDate} logic = {QuestionnaireTabLogic} selectedEvent = {this.props.selectedEvent}/>
                : null
            }
            <Grid item xs={12} style={{
                display: 'flex'
              }}>
              {this.state.visit
                ? (<Button className={this.classes.DeleteButtonStyle} onClick={this.handleDelete}>
                {t('delete')}
                </Button>)
                : null}
              <div className={this.classes.grow}/>
              <Button disabled = {(this.state.questionnaire && !!this.props.selectedEvent)} className={(this.state.questionnaire && !!this.props.selectedEvent) ? this.classes.DisabledButtonStyle:  this.classes.EnabledButtonStyle} style={{
                  margin: '5px'
                }} onClick={this.state.visit ? this.saveVisit : ((this.state.questionnaire && !!this.props.selectedEvent) ? () => {} : this.saveQuestionnaire)}>
                {t('save')}
              </Button>
            </Grid>
          </Grid>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left'
            }}
            className={this.classes.errorSnackbar}
            open={this.state.openAlert}
            autoHideDuration={6000}
            onClose={this.handleCloseAlert}
            ContentProps={{
              'aria-describedby' : 'message-id',
              className: this.classes.errorSnackbar
            }}
            message={<span id = "message-id" > {
              this.state.alertText
            }
            </span>}
            action={
              [<IconButton key="close" aria-label="Close" color="inherit" style={{
                  padding: '1px'
                }} onClick={this.handleCloseAlert}>
                <CloseIcon/>
              </IconButton>
            ]}/>
        </DialogContent>

      </Dialog>

    </div>);
  }
}

export default withStyles(styles)(withTranslation()(EventCreationDialog));
