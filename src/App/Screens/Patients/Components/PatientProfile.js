import React from 'react';
import {
  Grid,
  Radio,
  RadioGroup,
  FormControlLabel,
  Select,
  MenuItem,
  Button,
  TextField,
  Input,
  FormControl,
  FormLabel,
  InputLabel,
  Typography,
  LinearProgress,
  IconButton,
  Table,
  TableCell,
  TableHead,
  TableRow,
  TableBody,
 
} from '@material-ui/core';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import moment from 'moment';
import '../styles/PatientProfile.css';
import {
  getPatient,
  editPatient
} from 'Api/PatientApi'
import { getDiagnoses } from 'Api/DiagnosisApi';
import { getClinicalGroups } from 'Api/ClinicalGroupApi';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { isValidPesel, extractDateFromPesel } from 'Utils/Pesel'
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { withTranslation } from 'react-i18next';
import SelectValidator from 'Shared/components/SelectValidator';
import VisibilityIcon from '@material-ui/icons/Visibility';
import SimplePopover from 'Shared/components/SimplePopover';
import { format } from 'date-fns';


const styles = theme => ({
  root: {
    margin: '1rem 0',
  },
  formControl: {
    marginBottom: '1em',
    minWidth: 350,
    maxWidth: 350,
  },
  radioGroup: {
    display: 'flex',
    flexDirection: 'row',
  },
  saveBtn: {
    margin: '2em 14em',
    minWidth: 150,
  },
  historyMargin: {
    marginLeft: '48px',
    marginBottom: '1em',
    minWidth: 350,
    maxWidth: 350,
  },
})

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    }
  }
};

const maxBirthDate = new Date();
maxBirthDate.setDate((new Date().getDate() - 1));

class PatientProfile extends React.Component {

    state = {
      disabledInputs: true,
      firstName: '',
      lastName: '',
      phoneNumber: '',
      pesel: '',
      email: '',
      sex: '',
      dateOfBirth: '',
      fetchedClinicalGroups: [],
      selectedClinicalGroups: {},
      selectedDiagnoses: [],
      fetchedDiagnoses: [],
      loading: true,
      description: '',
      hasAppointments: false,
      clinicalGroupHistory: [],
      targetShow: null,
    };

  handleHistoryShow = (event) => {
    this.setState({ targetShow: event.target });
  };

  handleHistoryClose = () => {
    this.setState({ targetShow: null });
  }

  async componentDidMount() {
    ValidatorForm.addValidationRule('isPesel', (value) => {
      if (!value) return true;
      if (value) {
        if (isValidPesel(value)) {
          this.setState({ dateOfBirth: extractDateFromPesel(value) });
          return true;
        }
      }
      return false;
    })
    this.fetchPatient();
  }

  componentWillUnmount() {
    ValidatorForm.removeValidationRule('isPesel');
  }


  async fetchPatient() {
    const clinicalGroupsResponse = await getClinicalGroups();
    this.setState({ fetchedClinicalGroups: clinicalGroupsResponse.data })

    const diagnosesResponse = await getDiagnoses();
    this.setState({ fetchedDiagnoses: diagnosesResponse.data })

    const patientResponse = await getPatient(this.props.idPatient);

    let patient = patientResponse.data;
    this.setState({
      firstName: patient.firstName,
      lastName: patient.lastName,
      phoneNumber: patient.phoneNumber,
      sex: patient.sex ? 'male' : 'female',
      pesel: patient.pesel ? patient.pesel : '',
      email: patient.email,
      dateOfBirth: patient.dateOfBirth,
      selectedDiagnoses: patient.diagnoses.map(diagnosis => diagnosis.id),
      selectedClinicalGroups: patient.currentClinicalGroup.id,
      description: patient.description,
      isConfirmed: patient.isEmailConfirmed,
      hasAppointments: patient.hasAppointments,
      clinicalGroupHistory: patient.clinicalGroupHistory,
    });
    this.props.handleFetchedPatient(patient);
    this.setState({loading: false});
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleDateChange = date => {
    this.setState({dateOfBirth: date});
  }

  enableInputs = () => {
    this.setState({ disabledInputs: false });
  }

  disableInputs = () => {
    this.setState({ disabledInputs: true });
  }

  submitChanges = async () => {
    this.disableInputs();
    let patient = {
      userInfo: {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        isMale: (this.state.sex === 'male'),
      },
      pesel: this.state.pesel ? this.state.pesel : null,
      phoneNumber: this.state.phoneNumber,
      dateOfBirth: this.state.dateOfBirth,
      idClinicalGroup: this.state.selectedClinicalGroups,
      idsDiagnosis: this.state.selectedDiagnoses,
      description: this.state.description,
    };

    patient.idPatient = this.props.idPatient;
    const response = await editPatient(this.props.idPatient, patient);
    if (response) {
      this.fetchPatient();
    }
  }

  render() {
    const { classes, t, isAdmin } = this.props
    const { loading } = this.state
    const isHistory = this.state.clinicalGroupHistory && this.state.clinicalGroupHistory.length > 1;
    if (loading) {
      return (
        null
      );
    }
    return (
      <ValidatorForm onSubmit={this.submitChanges} style={{height: '100%'}}>
        <Grid container style={{height: '100%'}}>
          <Grid item container xs={12}>
            <Typography variant="h4">
              {`${this.state.firstName} ${this.state.lastName} (${this.state.isConfirmed ? t('confirmed') : t('notConfirmed')})`}
              <Button size="small" name="disabledInputs" onClick={this.enableInputs}>
                <i className="material-icons"  style={{marginLeft: '0.5em', color: 'var(--main-container-text-color)'}}>edit</i>
              </Button>
            </Typography>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
            <TextValidator
              label={`${t("firstName")}*`}
              id="firstName"
              name="firstName"
              validators={['required']}
              errorMessages={[t('requiredField')]}
              value={this.state.firstName}
              onChange={this.handleChange}
              disabled={this.state.disabledInputs}
            />
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <TextValidator
                label={`${t("email")}*`}
                id="email"
                name="email"
                validators={['required', 'isEmail']}
                errorMessages={[t('requiredField'), t('invalidEmail')]}
                value={this.state.email}
                onChange={this.handleChange}
                disabled={this.state.disabledInputs}
              />
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <TextValidator
                label={`${t("lastName")}*`}
                id="lastName"
                name="lastName"
                validators={['required']}
                errorMessages={[t('requiredField')]}
                value={this.state.lastName}
                onChange={this.handleChange}
                disabled={this.state.disabledInputs}
              />
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <KeyboardDatePicker
                  required
                  autoOk
                  format="dd/MM/yyyy"
                  maxDate={maxBirthDate}
                  margin="normal"
                  id="dateOfBirth"
                  name="dateOfBirth"
                  label={t("birthDate")}
                  value={this.state.dateOfBirth}
                  onChange={this.handleDateChange}
                  disabled={this.state.disabledInputs}
                />
              </MuiPickersUtilsProvider>
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <SelectValidator
              label={`${t('sex')}*`}
              name="sex"
              value={this.state.sex}
              validators={['required']}
              errorMessages={[t('requiredField')]}
              onChange={this.handleChange}
              className={classes.radioGroup}
              formStyles={classes.formControl}
              disabled={this.state.disabledInputs}
            >
              {[{label: t('female'), value: 'female'}, {label: t('male'), value: 'male'}].map((group, i) => (
                <MenuItem key={i} value={group.value}>
                  {group.label}
                </MenuItem>
              ))}
            </SelectValidator>
          </Grid>
          <Grid item container justify="center" alignContent="flex-start" xs={6}>
            <SelectValidator
                label={`${t('clinicalGroup')}*`}
                name="selectedClinicalGroups"
                value={this.state.selectedClinicalGroups}
                onChange={this.handleChange}
                input={<Input id="clinicalGroup" />}
                validators={['required']}
                errorMessages={[t('requiredField')]}
                MenuProps={MenuProps}
                disabled={this.state.disabledInputs || this.state.hasAppointments}
                formStyles={isHistory ? classes.historyMargin : classes.formControl}

                >
                {(this.state.fetchedClinicalGroups).map(group => (
                  <MenuItem key={group.idClinicalGroup} value={group.idClinicalGroup}>
                    {group.clinicalGroupName}
                  </MenuItem>
                ))}
            </SelectValidator>
            {isHistory &&
                <>
                  <IconButton aria-label="show" className={classes.showIcon} onClick={this.handleHistoryShow}>
                    <VisibilityIcon />
                  </IconButton>
                  <SimplePopover target={this.state.targetShow} handleClose={this.handleHistoryClose}>
                    <Table className={classes.table} aria-label="simple table">
                      <TableHead>
                        <TableRow>
                          <TableCell>{t('clinicalGroup')}</TableCell>
                          <TableCell>{t('date')}</TableCell>
                        </TableRow>
                      </TableHead>
                      <TableBody>
                        {this.state.clinicalGroupHistory.map((group, i) => (
                          <TableRow key={i}>
                            <TableCell>{group.clinicalGroup}</TableCell>
                            <TableCell>{format(new Date(group.dateFrom), 'dd/MM/yyyy')}</TableCell>
                          </TableRow>
                        ))}
                      </TableBody>
                    </Table>
                  </SimplePopover>
                </>
              }
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <TextValidator
                label={`${t("phoneNumber")}*`}
                id="phoneNumber"
                name="phoneNumber"
                validators={['required']}
                errorMessages={[t('requiredField')]}
                value={this.state.phoneNumber}
                onChange={this.handleChange}
                disabled={this.state.disabledInputs}
              />
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <SelectValidator
              multiple
              label={`${t('diagnosis')}*`}
              name="selectedDiagnoses"
              validators={['required']}
              errorMessages={[t('requiredField')]}
              value={this.state.selectedDiagnoses}
              onChange={this.handleChange}
              disabled={this.state.disabledInputs}
              input={<Input id="diagnosis" />}
              MenuProps={MenuProps}
              formStyles={classes.formControl}
            >
              {(this.state.fetchedDiagnoses).map(group => (
                <MenuItem key={group.id} value={group.id}>
                  {group.label}
                </MenuItem>
              ))}
            </SelectValidator>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <TextValidator
                  id="pesel"
                  name="pesel"
                  label="PESEL"
                  validators={['isPesel']}
                  errorMessages={[t('invalidPesel')]}
                  value={this.state.pesel}
                  onChange={this.handleChange}
                  disabled={this.state.disabledInputs}
                />
            </FormControl>
          </Grid>
          <Grid item container justify="center" xs={6}>
            <FormControl className={classes.formControl}>
              <TextField
                id="description"
                name="description"
                label={t("description")}
                onChange={this.handleChange}
                defaultValue={this.state.description}
                disabled={this.state.disabledInputs}
              />
            </FormControl>
          </Grid>
          {!this.state.disabledInputs &&
            <Grid item container justify="flex-end" xs={12} style={{height: '5%'}}>
              <Button variant="contained" type="submit" color="primary" type="submit" className={classes.saveBtn}>
              {t('save')}
              </Button>
            </Grid>
          }
        </Grid>
      </ValidatorForm>
    )
  }

}



export default withStyles(styles, { withTheme: true })(withTranslation()(PatientProfile));
