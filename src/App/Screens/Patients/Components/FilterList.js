import { makeStyles } from "@material-ui/core/styles";
import Divider from '@material-ui/core/Divider';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { getClinicalGroups } from 'Api/ClinicalGroupApi';
import { getDiagnoses } from 'Api/DiagnosisApi';
import { getDoctors } from 'Api/DoctorApi';
import React, { useState, useEffect } from 'react';
import Input from '@material-ui/core/Input';
import FormControl from '@material-ui/core/FormControl';
import { 
  buildQueryIn,
  buildEqualQuery,
 } from 'Utils/QueryBuilder'
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import buildQuery from 'odata-query';
import { useTranslation } from "react-i18next";

const useStyles = makeStyles(theme => ({
    list: {
      width: 300
    },
    h1: {
        textAlign: 'center',
        margin: '0.5em 0',
    },
    filterRow: {
        padding: '1em 0.8em',
    },
    filterButton: {
        backgroundColor: '#2e5bff',
        width: '100%',
    },
    formControl: {
        width: '100%',
        marginBottom: '1em',
    },
    form: {
        padding: '0 1em',
    }

    
}));

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};


export default function FilterList(props) {
    const classes = useStyles();
    const { t } = useTranslation();
    const [values, setValues] = useState({
        idClinicalGroup: '',
        idDiagnosis: [],
        idDoctor: [],
        patientName: '',
    });
    const [clinicalGroups, setClinicalGroups] = useState([]);
    const [diagnosises, setDiagnosises] = useState([]);
    const [doctors, setDoctors] = useState([]);



    useEffect(() => {

        async function fetchClinicalGroups() {
            const clinicalGroups = await getClinicalGroups();
            setClinicalGroups(clinicalGroups.data);
        }

        async function fetchDiagnosises() {
            const diagnosises = await getDiagnoses();
            setDiagnosises(diagnosises.data);
        }

        async function fetchDoctors() {
            const doctors = await getDoctors();
            setDoctors(doctors.data);
        }

        fetchClinicalGroups();
        fetchDiagnosises();
        fetchDoctors();

    }, [])

    const applyFilter = () => {
        const filter = {
            patientName: {
                contains: values.patientName
            },
        }
        let inQueryClinicalGroups = buildEqualQuery('idClinicalGroup', values.idClinicalGroup);
        let inQueryDiagnoses = buildQueryIn('diagnoses', values.idDiagnosis, false);
        let query = buildQuery({filter});
        query += inQueryClinicalGroups;
        query += inQueryDiagnoses;
        props.applyFilter(query);
    }


    const handleChange = event => {
        const eventValue = event.target.value;
        const eventName = event.target.name;
        setValues(oldValues => ({
            ...oldValues,
            [eventName]: eventValue,
        }));

    }

    return (
        <div className={classes.list}>   
            <h1 className={classes.h1}>{t('filter')}</h1>
            <Divider variant="middle" style={{marginBottom: '1em'}}/>
            <form autoComplete="off" className={classes.form}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="clinicalGroup">{t('clinicalGroup')}</InputLabel>
                    <Select
                    name="idClinicalGroup"
                    value={values.idClinicalGroup}
                    onChange={handleChange}
                    input={<Input id="clinicalGroup" />}
                    MenuProps={MenuProps}
                    >
                    {clinicalGroups.map((group) => (
                        <MenuItem key={group.idClinicalGroup} value={group.idClinicalGroup}>{group.clinicalGroupName}</MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="diagnosis">{t('diagnosis')}</InputLabel>
                    <Select
                        multiple
                        name="idDiagnosis"
                        value={values.idDiagnosis}
                        onChange={handleChange}
                        input={<Input id="diagnosis" />}
                        MenuProps={MenuProps}
                    >
                        {diagnosises.map((diagnosis, index) => (
                            <MenuItem key={index} value={diagnosis.id}>{diagnosis.label}</MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="doctor">{t('doctor')}</InputLabel>
                    <Select
                    multiple
                    name="idDoctor"
                    value={values.idDoctor}
                    onChange={handleChange}
                    input={<Input id="doctor" />}
                    MenuProps={MenuProps}
                    >
                    {doctors.map((doctor, index) => (
                        <MenuItem key={index} value={doctor.idDoctor}>{doctor.doctorName}</MenuItem>
                    ))}
                    </Select>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <TextField
                        id="patientName"
                        name="patientName"
                        label={t("fullName")}
                        value={values.patientName}
                        onChange={handleChange}
                        margin="normal"
                    />
                </FormControl>

            </form>
            <div className={classes.filterRow}>
                <Button variant="contained" color="primary" className={classes.filterButton} onClick={applyFilter}>{t('applyFilter')}</Button>
            </div>
        </div>
    );
}
