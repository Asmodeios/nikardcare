import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { 
  Tabs,
  Tab,
  Grid,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
} from '@material-ui/core';
import PatientProfile from './PatientProfile';
import PatientAnswers from './PatientAnswers';
import Calendar from './DoctorSideCalendar'
import { withTranslation } from 'react-i18next';
import BackButton from 'Shared/components/BackButton';
import { connect } from 'react-redux';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  deletePatient
} from 'Api/PatientApi';

const styles = theme => ({
  root: {
    height: '100%',
    flexGrow: 1,
    backgroundColor: 'inherit',
  },
  tabsIndicator: {
    backgroundColor: '#2E5BFF',
  },
  tabRoot: {
    height: '5%',
    textTransform: 'initial',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontSize: 18,
    fontFamily: [
      'var(--main-font-Source)',
    ],
    '&:hover': {
      color: '#2E5BFF',
      opacity: 1,
    },
    '&:focus': {
      color: '#2E5BFF',
    },
    },
    typography: {
      padding: theme.spacing(3),
    },
    tabSelected: {
      color: '#2E5BFF',
      fontWeight: theme.typography.fontWeightRegular,
    },
    container: {
      height: '90%',
      padding: '1em 0',
    },

    info: {
      height: '100%',
      overflowY: 'auto',
      padding: '2em',
    }
})

class Patient extends React.Component {
    state = {
        value: 0,
        hasAnswers: true,
        open: false,
        patientName: '',
    }

    handleChange = (event, value) => {
        this.setState({ value: value });
    };

    handleFetchedPatient = (patient) => {
      this.setState({ 
        hasAnswers: patient.hasAnswers,
        patientName: `${patient.firstName} ${patient.lastName}`
      });
    };

    setDeleteOpen = (open) => {
      this.setState({ open: open });
    };

    componentDidMount() {
      const { tabName } = this.props.match.params;
      if (tabName === "calendar") {
        this.setState({ value: 1 });
      };
    };

    handleDelete = async () => {
      const response = await deletePatient(this.props.match.params.idPatient);
      if (response && response.status >= 200 && response.status <= 299) {
        this.props.history.push('/patients')
      };
    }

    render() {
        const { value } = this.state;
        const { classes, t, isAdmin } = this.props
        const { idPatient } = this.props.match.params;

        return (
          <React.Fragment>
            <div className={classes.root}>
              <Tabs
                value={value}
                onChange={this.handleChange}
                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
              >
              <Tab
                disableRipple
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                label={t("profile")}
              />
              <Tab
                disableRipple
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                label={t("calendar")}
              />
              <Tab
                disableRipple
                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                label={t("answers")}
              />
              </Tabs>
              <div className={classes.container}>
                <div className={classes.info} style={{ backgroundColor: 'white' }}>
                  {value === 0 && <PatientProfile idPatient={idPatient} handleFetchedPatient={this.handleFetchedPatient}/>}
                  {value === 1 && <Calendar idPatient={idPatient} />}
                  {value === 2 && <PatientAnswers idPatient={idPatient} {...this.props}/>}
                </div>
              </div>
              <Grid item container xs={12} justify="space-between" style={{height: '5%'}}>
                <BackButton />
                {isAdmin && !this.state.hasAnswers &&
                  <Button 
                    variant="contained" 
                    style={{color: 'white', backgroundColor: 'red'}}
                    endIcon={<DeleteIcon />}
                    onClick={() => this.setDeleteOpen(true)}
                  >
                    {t('delete')}
                  </Button>
                }
              </Grid>
            </div>
            <Dialog
              maxWidth="sm"
              fullWidth
              open={this.state.open}
              onClose={() => this.setDeleteOpen(false)}
              aria-labelledby="max-width-dialog-title"
            >
              <DialogTitle id="max-width-dialog-title" style={{textAlign: 'center'}}>
                {`${t('deleteMessage')} "${this.state.patientName}"?`}
              </DialogTitle>
              <DialogActions>
                <Button onClick={() => this.setDeleteOpen(false)} color="secondary" variant="contained">
                  {t('cancel')}
                </Button>
                <Button 
                  variant="contained" 
                  style={{color: 'white', backgroundColor: 'red'}}
                  endIcon={<DeleteIcon />}
                  onClick={this.handleDelete}
                >
                  {t('delete')}
                </Button>
              </DialogActions>
            </Dialog>
        </React.Fragment>
        )
    };
}

const mapStateToProps = state => ({
  isAdmin: state.authorization.IsAdmin,
});

export default connect(mapStateToProps, null)(withStyles(styles)(withTranslation()(Patient)));
