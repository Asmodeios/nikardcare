import React from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import interactionPlugin from '@fullcalendar/interaction' // needed for dayClick
import EventWindow from './EventWindow'
import axios from 'axios'
import Paper from '@material-ui/core/Paper'
import moment from 'moment'
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import { getEvents } from 'Api/PatientApi';
import '../styles/DoctorSideCalendar.css';
import { withTranslation } from 'react-i18next'


const styles = theme => ({



});
class DoctorSideCalendar extends React.Component {
  calendarRef = React.createRef();

  constructor(props) {
    super(props);
    this.state = {
      calendarWeekends: true,
      calendarEvents: [ // initial event data

      ],
      showComponent:false,
      showEvent: false,
      selectedEvent:{},
      selectedDate:{},
      loading:true,
      curDate: new Date(),
    }


  }

  render() {
    const {t, i18n} = this.props;
    return (
        <div className='PatientSideCalendar'>
          {this.state.showComponent ? <EventWindow idPatient = {this.props.idPatient} addEvent = {this.addEvent} open={this.state.showComponent} action = {this.closer} selectedDate={this.state.selectedDate}/> : null}
          {this.state.showEvent ? <EventWindow idPatient ={this.props.idPatient} selectedEvent={this.state.selectedEvent} disabled = {true} addEvent = {this.addEvent} deleteEvent = {this.deleteEvent} open={this.state.showEvent} action = {this.closer} selectedDate={this.state.selectedDate}/> : null}
          <FullCalendar
            defaultView="dayGridMonth"
            eventLimit="4"
            fixedWeekCount={false}
            header={{
              left: 'prev,today,next',
              center: 'title',
              right: ''
            }}
            buttonIcons = {false}
            eventTimeFormat = {{
              hour: '2-digit',
              minute: '2-digit',
              meridiem: false,
              hour12: false
            }}
            buttonText = {{today: t('current'), prev: t('previous'), next: t('next')}}
            locale = {i18n.language}
            plugins={[ dayGridPlugin, interactionPlugin ]}
            ref={ this.calendarRef }
            weekends={ this.state.calendarWeekends }
            events={ this.state.calendarEvents }
            dateClick={ this.handleDateClick }
            conetntHeight={650}
            aspectRatio={1.7}
            eventClick = {this.handleEventClick}
            datesRender = {this.handleMonthChange}
            />
        </div>

    )
  }

  closer = () =>{
    this.loadData(this.calendarRef.current.calendar.view.currentStart)
    this.setState({
      showComponent:false,
      showEvent:false
    })


  }

  addEvent = (date) => {
    // console.log(date)
    this.setState({
      calendarEvents : this.state.calendarEvents.concat(date)
    })

  }
  deleteEvent = (event) => {
    this.setState({
      calendarEvents: this.state.calendarEvents.filter( (n) => {return n.id.valueOf() !== event.id.valueOf()}),
    })
    console.log(this.state.calendarEvents)
  }
  handleMonthChange = (info) => {
    console.log('Check month', info.view.currentStart, info.view.currentStart)
    console.log()
    if(this.curMonth !== this.getCalendarMonth(info.view.currentStart)){
      this.loadData(info.view.currentStart);
      this.setState({
        curData: info.view.currentStart
      })
      this.curMonth = this.getCalendarMonth(info.view.currentStart);
      console.log(this.getCalendarMonth(info.view.currentStart))
    }
  }


  getCalendarMonth(date){
    return date.getMonth()+1;
  }

  getCalendarYear(date){
    return date.getYear()+1900;
  }

  clearEvents = () => {
    this.setState({
      calendarEvents: []
    })
  }

  loadData = (date) =>{
    this.clearEvents()

    getEvents(this.props.idPatient, this.getCalendarYear(date), this.getCalendarMonth(date)).then((response) => {
      this.mapVisits(response.data.Visit)
      this.mapQuestionnaires(response.data.Questionnaire)
      this.setState({
        loading:false
      })
    }).catch((error) => {console.log(error)});
  }

  mapQuestionnaires = (arg) => {
    var questionnaires = []
    arg.map((n)=>{
      var curEvent = {
        id: n.idEvent,
        title: n.eventName,
        start: moment(n.eventDate).toDate(),
        className: `questionnaire status${n.questionnaireStatus}`,
        description: n.description,
        idQuestionnaire: n.idQuestionnaire,
        questionnaireStatus: n.questionnaireStatus,
      }
      questionnaires.push(curEvent)
    })
    this.setState({
      calendarEvents: this.state.calendarEvents.concat(questionnaires)
    })
  }

  mapVisits = (arg) => {
    var visits = []
    arg.map((n)=>{
      var curEvent = {
        id: n.idEvent,
        title: n.eventName,
        start: moment(n.timeFrom).toDate(),
        className: 'visit',
        description: n.description
      }
      visits.push(curEvent)
    })
    this.setState({
      calendarEvents: this.state.calendarEvents.concat(visits)
    })
    console.log(this.state)
  }
  handleEventClick = (arg) => {
    if(arg.event.classNames[0] === 'questionnaire'){
      this.setState({
        showEvent:true,
        selectedEvent:arg.event
      })
    } else if (arg.event.classNames[0] === 'visit') {
      this.setState({
        showEvent:true,
        selectedEvent:arg.event
      })
    }


  }

  handleDateClick = (arg) => {
    console.log(arg)
    this.setState({
      showComponent:true,
      selectedDate:arg.date
    })
  }

}
export default withStyles(styles)(withTranslation()(DoctorSideCalendar));
