import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Paper,
  Button,
  Grid,
} from '@material-ui/core';
import { useTranslation } from 'react-i18next';
import EnhancedTable from 'Shared/components/EnhancedTable';
import { buildQuery, buildCountQuery } from 'Utils/Query';
import SearchBar from 'Shared/components/SearchBar';
import {
  getPatientAnswers
} from 'Api/AnswerApi';
import { format } from 'date-fns'
import { getAlertTypeLabelById } from 'Utils/Dictionary';

const useStyles = makeStyles({
  root: {
    minWidth: '80%',
    overflowX: 'auto',
  },
  table: {
    width: '100%',
  },
  moreBtn: {
    border: '1px solid #2E5BFF',
    fontSize: '0.7rem',
    width: '70%',
  },
  circle3: {
    backgroundColor: 'red',
    borderRadius: '50%',
    height: '16px',
    width: '16px',
    marginLeft: '2em'
  },
  circle2: {
    backgroundColor: 'yellow',
    borderRadius: '50%',
    height: '16px',
    width: '16px',
    marginLeft: '2em'
  },
  circle1: {
    backgroundColor: '#dedada',
    borderRadius: '50%',
    height: '16px',
    width: '16px',
    marginLeft: '2em'
  },
  tableCell: {
    '&:hover': {
      cursor: 'pointer',
    },
  },
  });

const PatientAnswers = (props) => {
  const { t } = useTranslation();
  const classes = useStyles();
  const [data, setData] = useState([]);
  const [count, setCount] = useState(0);
  const [queryParams, setQueryParams] = useState({
    orderby: '',
    pagination: {
      page: 1,
      perPage: 20,
    }
  });
  const { idPatient } = props;

  const headRows = [
    { id: "idAlert", disablePadding: false, label: t('alertAlarming') },
    { id: "title", disablePadding: false, label: t('title') },
    { id: "answeredAt", disablePadding: false, label: t('date') },
    { id: "assignedBy", disablePadding: false, label: t('assignedBy') },
  ];

  const handleOrderChange = ({ order, orderBy }) => {
    if (order && orderBy) {
      setQueryParams(oldValues => ({
        ...oldValues, 
        orderby: `${orderBy} ${order}`
      }));
    }
  };

  const handlePerPage = value => {
    setQueryParams(oldValues => ({
      ...oldValues, 
      pagination: {
        ...oldValues.pagination,
        perPage: value
      }
    }));
  }

  const handlePage = value => {
    setQueryParams(oldValues => ({
      ...oldValues, 
      pagination: {
        ...oldValues.pagination,
        page: value + 1
      }
    }));
  }

  const handleRowClick = row => {
    props.history.push(`/answers/${row.idAnswer}/view`)
  }

  async function getCount(params) {
    const response = await getPatientAnswers(idPatient, params);
    if (response.data.length > 0) {
      const proposalCount = response.data[0].count || 0;
      setCount(proposalCount)
    } else {
      setCount(0);
    };
  };

  useEffect(() => {
    getCount(buildCountQuery(queryParams.searchBy))
  }, [queryParams.searchBy]);

  useEffect(() => {
    fetchAnswers(buildQuery(queryParams))
    console.log(queryParams)
  }, [queryParams]);


  async function fetchAnswers(params) {
    let response = await getPatientAnswers(idPatient, params);
    if (response.status >= 200 && response.status < 300) {
      setData(response.data);
    }
  };

  return (
    <>
      <Grid container justify="center">
        <EnhancedTable 
          data={data} 
          handleOrder={handleOrderChange} 
          handlePerPage={handlePerPage} 
          count={count} 
          handlePage={handlePage}
          // initialOrderBy="idAlert desc"
          headRows={headRows}
          defaultRowsPerPage={20}
        >
          {data
            .map((row, index) => {
              let page = queryParams.pagination.page - 1;
              let perPage = queryParams.pagination.perPage;
              return (
                <TableRow hover tabIndex={-1} key={index}>
                  <TableCell onClick={() => handleRowClick(row)}>
                    {(index + 1) + (page * perPage)}
                  </TableCell>
                  <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                    <div className={classes[`circle${row.idAlert}`]} />
                  </TableCell>
                  <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                    {row.title}
                  </TableCell>
                  <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                    {format(new Date(row.answeredAt), 'dd/MM/yyyy')}
                  </TableCell>
                  <TableCell align="left" className={classes.tableCell} onClick={() => handleRowClick(row)}>
                    {row.assignedBy}
                  </TableCell>
                </TableRow>
              );
            })}
        </EnhancedTable>
      </Grid>
    </>
  );
};

export default PatientAnswers;
