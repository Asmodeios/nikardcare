import React, {useState, useEffect} from 'react'
import {withStyles, makeStyles} from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import ClockIcon from '@material-ui/icons/AccessTime';
import {KeyboardTimePicker} from "@material-ui/pickers";
import MomentUtils from '@date-io/moment';
import {MuiPickersUtilsProvider} from '@material-ui/pickers';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import QuestionnaireCard from 'Shared/components/QuestionnaireCard';
import {getQuestionnaires, getQuestionnaire} from 'Api/QuestionnaireApi';
import PendingScreen from 'Shared/components/PendingScreen';
import {clearEditQuestion, clearEditQuestions} from 'Redux/modules/questionnaires/actions';
import TextField from '@material-ui/core/TextField';
import {connect} from 'react-redux';
import Checkbox from '@material-ui/core/Checkbox';


const useStyles = makeStyles(theme => ({
  container: {
    margin: '10px 25px 10px 25px',
    flex: 1
  },
  SelectQuestionnaireLabel: {
    marginTop: '10px'
  },
  questionnaireContainer: {
    marginTop: '10px',
    flex: 1,
    overflowY: 'scroll',
    border: '1px solid #BFC5D2',
    borderRadius: '10px',
    maxHeight: '450px',
    minHeight: '350px'
  },
  questionnaireItem: {
    borderLeftWidth: '5px',
    borderWidth: '0px',
    borderStyle: 'solid',
    padding: '15px',
    borderColor: '#2E5BFF',
    backgroundColor: '#EEF2FF',
    cursor: 'pointer',
    marginBottom: '5px'
  },
  selected: {
    borderWidth: '1px'
  },
  expandButton: {
    cursor: 'pointer'
  },
  intervalContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginTop: '10px',
    flex: 1,
  },
  intervalCheckboxContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginLeft: '15px'
  },
  intervalInput: {
    width: '35px',
    margin: '0px 10px 0px 10px'
  },
  intervalCheckbox: {
    paddingBottom: '2px'
  },
  intervalInputContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginRight: '25px'
  },
  questionnairesEmptyContainer: {
    marginTop: '10px',
    flex: 1,
    overflowY: 'scroll',
    border: '1px solid #BFC5D2',
    borderRadius: '10px',
    minHeight: '200px'
  }
}))

function QuestionnaireTab(props) {
  const { t } = useTranslation();
  const classes = useStyles();
  const {selectedQuestionnaire, setSelectedQuestionnaire, intervalInput, setIntervalInput, isInterval, setIsInterval, numOfTimes, setNumOfTimes, availableFor, setAvailableFor, startTime, setTime} = props.logic;
  const [questionnairesState, setQuestionnaresState] = useState(props)
  const [selectedQuestionnaireFromApi, setSelectedQuestionnaireFromApi] = useState(props.questionnaire);
  const [newObj, setNewObj] = useState(props);

  if(!!props.selectedEvent){
    useEffect(() => {
      props.fetchQuestionnaire(props.selectedEvent.extendedProps.idQuestionnaire)
    }, [])

  } else {
    useEffect(() => {
      props.fetchQuestionnaires(4);
    }, [])
  }
  useEffect(() => {
    setQuestionnaresState(props);
  }, [props])



  const {questionnaires, isPending} = questionnairesState;

  const handleSelect = (id) => () => {
    setSelectedQuestionnaire(id)
  }
  const handleIntervalInputChange = (event) => {
    if(event.target.value > 0) {
      setIntervalInput(event.target.value)
    }
  }
  const handleChangeNumOfTimes = (event) => {
    if(event.target.value > 0) {
      setNumOfTimes(event.target.value)
    }
  }
  const handleChangeAvailableFor = (event) => {
    if(event.target.value > 0) {
      setAvailableFor(event.target.value)
    }
  }
  const isEmpty = () => {
    return questionnaires.length === 0;
  }

  return (
  <div className={classes.container}>
    {!!!props.selectedEvent && (<React.Fragment><div style = {{display:'flex', flexDirection:'row'}}>
        <ClockIcon style={{
            position: 'relative',
            top: '7px',
            marginRight: '5px'
          }}/>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <KeyboardTimePicker
            ampm={false}
            disabled = {!!props.selectedEvent}
            autoOk={true}
            mask='__:__'
            placeholder='12:00'
            value={startTime}
            onChange={date => setTime(date)}
          />
        </MuiPickersUtilsProvider>
        <div className = {classes.intervalCheckboxContainer}>
          <Typography variant="subtitle1" className={classes.SelectQuestionnaireLabel}>
            {t('interval')}:
          </Typography>
          <Checkbox
            className = {classes.intervalCheckbox}
            color = 'primary'
            checked= {isInterval}
            disabled = {!!props.selectedEvent}
            onChange={() => setIsInterval(!isInterval)}
            value='isInterval'
          />
        </div>
      </div>
      <div className = {classes.intervalContainer}>
        <div className = {classes.intervalInputContainer}>
          <Typography variant="subtitle1" className={classes.SelectQuestionnaireLabel}>
            {t('availableFor')}
          </Typography>
          <TextField
            type = 'number'
            id = 'id-availableFor-input'
            value = {availableFor}
            onChange = {handleChangeAvailableFor}
            className = {classes.intervalInput}
          />
        </div>
        {isInterval &&
          <React.Fragment>
            <div className = {classes.intervalInputContainer}>
              <Typography variant="subtitle1" className={classes.SelectQuestionnaireLabel}>
                {t('days')}:
              </Typography>
              <TextField
                type = 'number'
                id = 'id-interval-input'
                value = {intervalInput}
                onChange = {handleIntervalInputChange}
                className = {classes.intervalInput}
              />
            </div>
            <div className = {classes.intervalInputContainer}>
              <Typography variant="subtitle1" className={classes.SelectQuestionnaireLabel}>
                {t('numberOfTimes')}
              </Typography>
              <TextField
                type = 'number'
                id = 'id-numOftimes-input'
                value = {numOfTimes}
                onChange = {handleChangeNumOfTimes}
                className = {classes.intervalInput}
              />
            </div>
          </React.Fragment>
         }
      </div>
    </React.Fragment>)}
    <Typography variant="h5" className={classes.SelectQuestionnaireLabel}>
      {!!props.selectedEvent ? t('selectedQuestionnaire') : t('selectQuestionnaire')}
    </Typography>
    { isPending && <PendingScreen/> }

      <Grid container spacing={4}
        alignContent = {(isEmpty() && !!!props.selectedEvent)  ? 'center' : 'flex-start'}
        className={(isEmpty() && !!!props.selectedEvent) ? classes.questionnairesEmptyContainer : classes.questionnaireContainer}>
      {!!props.selectedEvent
        ? (<Grid item container justify="center" xs={4} >
          <QuestionnaireCard
            questionnaire={props.questionnaire}
            type={'selected'}
            />
        </Grid>)
        : questionnaires && questionnaires.map((questionnaire, i) => (
        <Grid item container justify="center" xs={4} key={i} >
          <QuestionnaireCard
            questionnaire={questionnaire}
            type={questionnaire.idQuestionnaire === selectedQuestionnaire
              ? "selected"
              : 'unassigned'}
            handleAdd={handleSelect(questionnaire.idQuestionnaire)}/>
        </Grid>))
      }
      {
        (isEmpty() && !!!props.selectedEvent) ? <Typography variant = 'h5' style = {{margin: 'auto'}}>Empty</Typography> : null
      }
    </Grid>

  </div>)
}

const mapStateToProps = state => ({
  questionnaires: state.questionnaires.questionnaires.list,
  isFetched: state.questionnaires.questionnaires.isFetched,
  isPending: state.questionnaires.questionnaires.isPending,
  questionnaireIsPending: state.questionnaires.questionnaire.isPending,
  questionnaire: state.questionnaires.questionnaire
})

const mapDispatchToProps = dispatch => {
  return {
    fetchQuestionnaires: (questionnaireType) => dispatch(getQuestionnaires(questionnaireType)),
    fetchQuestionnaire: (idQuestionnaire) => dispatch(getQuestionnaire(idQuestionnaire))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuestionnaireTab);
