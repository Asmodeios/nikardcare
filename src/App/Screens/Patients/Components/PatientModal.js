import React from 'react';
import {
  Grid,
  MenuItem,
  Button,
  TextField,
  Input,
  FormControl,
} from '@material-ui/core';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import '../styles/PatientProfile.css';
import { getClinicalGroups } from 'Api/ClinicalGroupApi';
import { getDiagnoses } from 'Api/DiagnosisApi';
import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';
import { isValidPesel, extractDateFromPesel } from 'Utils/Pesel';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { withTranslation } from 'react-i18next';
import SelectValidator from 'Shared/components/SelectValidator';

const styles = theme => ({
    error: {
      color: 'red',
      fontSize: '0.8em',
      marginLeft: '15em',
    },
    radioGroup: {
      display: 'flex',
      flexDirection: 'row',
    },
    formControl: {
      marginBottom: '1em',
      minWidth: 350,
      maxWidth: 350,
    },
    saveBtn: {
      margin: '2em 9em',
      minWidth: 100,
    }
    
  })

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};


const maxBirthDate = new Date();
maxBirthDate.setDate((new Date().getDate() - 1));


class PatientModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      pesel: '',
      email: '',
      sex: 'male',
      dateOfBirth: maxBirthDate,
      fetchedClinicalGroups: [],
      selectedClinicalGroup: [],
      selectedDiagnoses: [],
      fetchedDiagnoses: [],
    };
  }

  async componentDidMount() {
    ValidatorForm.addValidationRule('isPesel', (value) => {

      if (isValidPesel(value)) {
        this.setState({ dateOfBirth: extractDateFromPesel(value) });
        return true;
      } else if (!value) {
        return true;
      }
      return false;
    })

    const clinicalGroupsResponse = await getClinicalGroups();
    this.setState({ fetchedClinicalGroups: clinicalGroupsResponse.data })
    
    const diagnosisResponse = await getDiagnoses();
    this.setState({ fetchedDiagnoses: diagnosisResponse.data })

  }

  componentWillUnmount() {
    ValidatorForm.removeValidationRule('isPesel');
  }

  handleChange = event => {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  };

  handleDateChange = date => {
    this.setState({ dateOfBirth: date });
  }
  
  handleSubmit = event => {
    let patient = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      isMale: (this.state.sex === 'male'),
      pesel: this.state.pesel,
      phoneNumber: this.state.phoneNumber,
      dateOfBirth: this.state.dateOfBirth,
      idClinicalGroup: this.state.selectedClinicalGroup,
      idsDiagnosis: this.state.selectedDiagnoses,
      description: this.state.description,
    };

    this.props.handleAddPatient(patient);
    

    
  };

      render() {
        const { classes, t } = this.props;
        return (
          <React.Fragment>
            <ValidatorForm onSubmit={this.handleSubmit}>
              <Grid container>
                <Grid item container justify="center" xs={6}> 
                  <FormControl className={classes.formControl}>
                    <TextValidator
                      label={`${t("firstName")}*`}
                      id="firstName"
                      name="firstName"
                      validators={['required']}
                      errorMessages={[t('requiredField')]}
                      value={this.state.firstName}
                      onChange={this.handleChange}
                    />
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <TextValidator
                    label={`${t("email")}*`}
                    id="email"
                    name="email"
                    validators={['required', 'isEmail']}
                    errorMessages={[t('requiredField'), t('invalidEmail')]}
                    value={this.state.email}
                    onChange={this.handleChange}
                    />
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <TextValidator
                      label={`${t("lastName")}*`}
                      id="lastName"
                      name="lastName"
                      validators={['required']}
                      errorMessages={[t('requiredField')]}
                      value={this.state.lastName}
                      onChange={this.handleChange}
                    />
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        required
                        autoOk
                        variant="inline"
                        format="dd/MM/yyyy"
                        id="dateOfBirth"
                        name="dateOfBirth"
                        maxDate={maxBirthDate}
                        label={t('birthDate')}
                        value={this.state.dateOfBirth}
                        onChange={this.handleDateChange}
                        KeyboardButtonProps={{
                          'aria-label': 'change date',
                        }}
                      />
                    </MuiPickersUtilsProvider>
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <SelectValidator
                    label={`${t('sex')}*`}
                    name="sex"
                    value={this.state.sex}
                    validators={['required']}
                    errorMessages={[t('requiredField')]}
                    onChange={this.handleChange}
                    formStyles={classes.formControl}
                  >
                    {[{label: t('female'), value: 'female'}, {label: t('male'), value: 'male'}].map((group, i) => (
                      <MenuItem key={i} value={group.value}>
                        {group.label}
                      </MenuItem>
                    ))}
                    
                  </SelectValidator>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <SelectValidator
                    label={`${t('clinicalGroup')}*`}
                    formStyles={classes.formControl}
                    name="selectedClinicalGroup"
                    value={this.state.selectedClinicalGroup}
                    onChange={this.handleChange}
                    MenuProps={MenuProps}
                    validators={['required']}
                    errorMessages={[t('requiredField')]}
                  >
                    {(this.state.fetchedClinicalGroups).map((group, i) => (
                      <MenuItem key={i} value={group.idClinicalGroup}>
                        {group.clinicalGroupName}
                      </MenuItem>
                    ))}
                  </SelectValidator>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <TextValidator
                      label={`${t("phoneNumber")}*`}
                      id="phoneNumber"
                      name="phoneNumber"
                      validators={['required']}
                      errorMessages={[t('requiredField')]}
                      value={this.state.phoneNumber}
                      onChange={this.handleChange}
                    />
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <SelectValidator
                    multiple
                    label={`${t('diagnosis')}*`}
                    formStyles={classes.formControl}
                    name="selectedDiagnoses"
                    validators={['required']}
                    errorMessages={[t('requiredField')]}
                    value={this.state.selectedDiagnoses}
                    onChange={this.handleChange}
                    MenuProps={MenuProps}
                  >
                    {(this.state.fetchedDiagnoses).map((diagnos, i) => (
                      <MenuItem key={i} value={diagnos.id}>
                        {diagnos.label}
                      </MenuItem>
                    ))}
                  </SelectValidator>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <TextValidator
                        id="pesel"
                        name="pesel"
                        label="PESEL"
                        validators={['isPesel']}
                        errorMessages={[t('invalidPesel')]}
                        value={this.state.pesel}
                        onChange={this.handleChange}
                      />
                  </FormControl>
                </Grid>
                <Grid item container justify="center" xs={6}>
                  <FormControl className={classes.formControl}>
                    <TextField
                      id="description"
                      name="description"
                      label={t("description")}
                      onChange={this.handleChange}
                      value={this.state.description}
                    />
                  </FormControl>
                </Grid>
                <Grid item container justify="flex-end" xs={12}>
                  <Button variant="contained" className="saveBtn" type="submit" color="primary" className={classes.saveBtn}> 
                  {t('save')}
                  </Button>
                </Grid>
              </Grid>

              
            </ValidatorForm>
          </React.Fragment>
        )
      }
}

PatientModal.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles, { withTheme: true })(withTranslation()(PatientModal))




