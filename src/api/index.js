import axios from 'axios';
import {
  refreshToken,
  setAuthorization,
  logout,
  getAuthToken,
  resetAuthTokenRequest,
} from './AuthorizationApi';
import store from 'Redux/store';
import {startLoading, finishLoading} from 'Redux/modules/loading/actions'
import LocalStorageService from 'Utils/LocalStorageService';
const localStorageService = LocalStorageService.getService();
const apiAddress = 'https://heart-watch-test.azurewebsites.net/api';
// const apiAddress = 'https://heartwatch-api.boryssey.com/api'
// const apiAddress = 'https://localhost:44341/api';
axios.defaults.baseURL = apiAddress;


axios.interceptors.request.use(
  config => {
      store.dispatch(startLoading())
      const token = localStorageService.getAccessToken();
      if (token) {
          config.headers['Authorization'] = `Bearer ${token}`;
      }
      config.headers['Content-Type'] = 'application/json';
      return config;
  },
  error => {
      Promise.reject(error)
  });


axios.interceptors.response.use((response) => {
  store.dispatch(finishLoading());
  return response;
 },
 function (err) {
  const error = err.response;
    if (error.status === 401 && error.config && !error.config.__isRetryRequest) {
      return getAuthToken({
        token: localStorageService.getAccessToken(),
        refreshToken: localStorageService.getRefreshToken(),
      }).then(response => {
        localStorageService.setToken(response.data);
        axios.defaults.headers.common['Authorization'] = `Bearer ${localStorageService.getAccessToken()}`;
        error.config.__isRetryRequest = true;
        return axios(error.config);
       })
       .catch(error => {
         store.dispatch(logout());
       });
     }
    return Promise.reject(error);
 });


export default axios;
