/* eslint-disable require-jsdoc */
import axios from './index';

export const  getAllPatients = (query) => {
  if (query) {
    console.log(query);
    return axios.get(`/patients${query}`);
  }
  return axios.get(`/patients`);
};

export const getPatient = (idPatient) => {
  return axios.get(`/patients/${idPatient}`);
};

export const addPatient = (patient) => {
  return axios.post('/identity/register/Patient', patient);
};

export const editPatient = (idPatient, patient) => {
  return axios.put(`/patients/${idPatient}`, patient);
};

export const getEvents = (idPatient, year, month) => {
  return axios.get(`/patients/${idPatient}/calendar?year=${year}&month=${month}`);
};

export const deleteEvent = (idPatient, idVisit) => {
  return axios.delete(`/patients/${idPatient}/calendar/visits/${idVisit}`);
};

export const addEvent = (idPatient, eventObject) => {
  return axios.post(`/patients/${idPatient}/calendar/visits`, eventObject);
};
export const assignQuestionnaire = (idPatient, questionnaireObject) => {
  return axios.post(`/patients/${idPatient}/questionnaires`, questionnaireObject);
}

export const deletePatient = (idPatient) => {
  return axios.delete(`/patients/${idPatient}`);
};