import axios from './index';

export const getDiagnoses = () => {
	return axios.get('/dictionary/diagnoses');
}

export const addDiagnosis = (label) => {
  return axios.post('/administration/diagnoses', { diagnosisName : label });
};

export const editDiagnosis = (diagnosis) => {
  return axios.put(`/administration/diagnoses/${diagnosis.idDiagnosis}`, diagnosis)
}

export const deleteDiagnosis = (id) => {
  return axios.delete(`/administration/diagnoses/${id}`);
};

export const getAdminDiagnoses = () => {
  return axios.get('/administration/diagnoses');
}