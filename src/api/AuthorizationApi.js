import axios from './index';
import {
  loginSuccess,
  loginError,
  userLogout,
} from 'Redux/modules/authorization/actions';
import { parseJwt } from 'Utils/tokenParser';
import LocalStorageService from 'Utils/LocalStorageService';
const localStorageService = LocalStorageService.getService();

export function login(credentials) {
	return dispatch => {
		axios.post('/identity/login', credentials)
		.then(response => {
      if (response.error) {
				throw (response.error);
      }
      const token = response.data.token;
      const refreshToken = response.data.refreshToken;
      const parsedToken = parseJwt(token);

      if (parsedToken.role === 'Doctor') {
        localStorageService.setToken({
          token: token,
          refreshToken: refreshToken,
        })
        dispatch(loginSuccess(parsedToken));
      } else {
        dispatch(loginError({
          message: ['InvalidCredentials'],
        }));
      }
     
			return response.data;
		})
		.catch(error => {
			dispatch(loginError(error));
		});
	};
};


export function logout() {
  return dispatch => {
    localStorageService.clearToken();
    setAuthorization(null);
    dispatch(userLogout());
  }
}

export function setAuthorization(token) {
  axios.defaults.headers.common.Authorization = token;
};

let authTokenRequest;

export function getAuthToken({ token, refreshToken }) {
  if (!authTokenRequest) {
    authTokenRequest = axios.post('/identity/refresh', { token, refreshToken });
    authTokenRequest.then(resetAuthTokenRequest, resetAuthTokenRequest);
  }
  return authTokenRequest;
};

function resetAuthTokenRequest() {
  authTokenRequest = null;
}

export function resetPassword(email) {
  return axios.post('/identity/resetPassword', email);
};

export function confirmEmail(data) {
  return axios.post('/identity/confirmEmail', data);
};

export function setPassword(data) {
  return axios.post('/identity/setPassword', data);
};