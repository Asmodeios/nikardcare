import axios from './index';

export const getClinicalGroupsDictiponary = () => {
  return axios.get('/dictionary/clinicalGroups');
};

export const getClinicalGroups = (params) => {
  return axios.get(params ? `/clinicalGroups?${params}` : '/clinicalGroups');
};

export const getClinicalGroup = (id) => {
  return axios.get(`/clinicalGroups/${id}`);
};

export const addClinicalGroup = (clinicalGroups) => {
  return axios.post('/clinicalGroups', clinicalGroups);
};

export const addPatientsToClinicalGroup = (idClinicalGroup, idPatients) => {
  return axios.post(`/clinicalGroups/${idClinicalGroup}/addPatients`, idPatients);
};

export const assignQuestionnaire = (params) => {
  return axios.post(`/clinicalGroups/${params.idClinicalGroup}/questionnaires`, params);
};

export const deleteClinicalGroup = (id) => {
  return axios.delete(`/clinicalGroups/${id}`);
};
