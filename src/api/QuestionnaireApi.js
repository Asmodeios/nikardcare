import axios from 'axios';
import { fetchQuestionnairesBegin, 
	fetchQuestionnairesSuccess, 
	fetchQuestionnairesError,
	fetchQuestionnaireBegin,
	fetchQuestionnaireSuccess,
	fetchQuestionnaireError,
	editQuestionnaireBegin, 
	editQuestionnaireSuccess, 
	editQuestionnaireError,
	createQuestionnaireBegin, 
	createQuestionnaireSuccess, 
	createQuestionnaireError, 
} from 'Redux/modules/questionnaires/actions';

export function getQuestionnaires(questionnaireType) {
	return dispatch => {
		dispatch(fetchQuestionnairesBegin());
		axios.get('/questionnaires', { params: { questionnaireType }})
		.then(response => {
			if (response.error) {
				throw (response.error);
			}
			dispatch(fetchQuestionnairesSuccess(response.data));
			return response.data;
		})
		.catch(error => {
			dispatch(fetchQuestionnairesError(error));
		});
	};
}

export function addQuestionnaire(questionnaire) {
	return dispatch => {
		dispatch(createQuestionnaireBegin());
		axios.post('/questionnaires', questionnaire)
		.then(response => {
			if (response.error) {
				throw (response.error);
			}
			dispatch(createQuestionnaireSuccess());
			return response.data;
		})
		.catch(error => {
			dispatch(createQuestionnaireError(error));
		});
	};
};

export function getQuestionnaire(idQuestionnaire) {
	return dispatch => {
		dispatch(fetchQuestionnaireBegin());
		axios.get(`/questionnaires/${idQuestionnaire}`)
		.then(response => {
			if (response.error) {
				throw (response.error);
			}
			dispatch(fetchQuestionnaireSuccess(response.data));
			return response.data;
		})
		.catch(error => {
			dispatch(fetchQuestionnaireError(error));
		});
	};
};

export function editQuestionnaire(questionnaire) {
	return dispatch => {
		dispatch(editQuestionnaireBegin());
		axios.put('/questionnaires', questionnaire)
		.then(response => {
			if (response.error) {
				throw (response.error);
			}
			dispatch(editQuestionnaireSuccess());
			return true;
		})
		.catch(error => {
			dispatch(editQuestionnaireError(error.response.data));
		});
	};
};



