import axios from 'axios';

export function getAnsweredQuestionnaire(idAnswer) {
  return axios.get(`/answers/${idAnswer}`);
};

export function dismissAnswer(data) {
  return axios.post(`/answers/${data.idQuestionnaireAnswer}/dismiss`, data);
};

export function getClinicalGroupAnswers(idGroup, params) {
  return axios.get(`/answers/clinicalGroups/${idGroup}?${params}`);
};

export function getPatientAnswers(idPatient, params) {
  return axios.get(`/answers/patient/${idPatient}?${params}`);
};

export function getFile(idAnswer, idFile) {
  return axios.get(`/answers/${idAnswer}/file/${idFile}`, { responseType: 'blob', timeout: 30000 });
};