import axios from 'axios';
import { fetchQuestionsBegin, fetchQuestionsSuccess, fetchQuestionsError } from 'Redux/modules/questionnaires/actions';

export function getQuestions() {
	return dispatch => {
		dispatch(fetchQuestionsBegin());
		axios.get('/questions')
		.then(response => {
			if (response.error) {
				throw (response.error);
			}
			dispatch(fetchQuestionsSuccess(response.data));
			return response.data;
		})
		.catch(error => {
			dispatch(fetchQuestionsError(error));
		});
	}
}
