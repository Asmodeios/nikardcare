import axios from './index';

export const getDoctors = () => {
	return axios.get('/doctors');
};

export const addDoctor = (data) => {
  return axios.post('/identity/register/Doctor', data);
};

export const deleteDoctor = (idDoctor) => {
  return axios.delete(`/doctors/${idDoctor}`);
};
