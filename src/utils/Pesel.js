export const isValidPesel = pesel => {
    let weight = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3];
    let sum = 0;
    let controlNumber = parseInt(pesel.substring(10, 11));
    for (let i = 0; i < weight.length; i++) {
        sum += (parseInt(pesel.substring(i, i + 1)) * weight[i]);
    }
    sum = sum % 10;
    return 10 - sum === controlNumber;
  }


export const extractDateFromPesel = pesel => {
    let year = pesel.slice(0, 2);
    let month = pesel.slice(2, 4);
    let day = pesel.slice(4, 6);

    if (month > 12) {
        month = +month - 20;
        year = `20${year}`;
    } else {
        year = `19${year}`;
    }

    return `${year}-${month}-${day}`;
}