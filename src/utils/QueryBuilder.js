
/* pattern obj = {
    clinicalGroup: {
        in: [2 ,3],
        contains: 'dsadsadsa'
    }
}
*/

export const buildQueryIn = (property, array, isFirst) => {

    if (Array.isArray(array) && array.length > 0 && property) {
        let query =  isFirst ? `?$filter=` : ' and';

        for (let i = 0; i < array.length; i++) {
            let isLast = i == array.length - 1;
            query += isLast ? ` ${array[i]} in ${property}` : ` ${array[i]} in ${property} or`
        }
        return query;
    }
    return '';
}

export const buildEqualQuery = (property, value) => {
  if (value) {
    return ` and ${property} eq ${value}`;
  }
  return '';
};

function buildOrderQuery(value) {
  return `$orderby=${value}`;
}

function buildSearchQuery(value) {
  return `searchBy=${value}`
}

function paginationQuery({page, perPage}) {
  const top = perPage;
  const skip = perPage * (page - 1);
  return `$top=${top}&$skip=${skip}`;
}

const dictionary = {
  searchBy: buildSearchQuery,
  orderby: buildOrderQuery,
  pagination: paginationQuery,
}

export function buildQuery(params) {
  let query = '';
  let paramNumber = 0;
  for (let [key, value] of Object.entries(params)) {
    if (value) {
      let func = dictionary[`${key}`];
      if (func) {
        paramNumber !== 0 && (query += '&');
        query += func(value);
      }
    }
    paramNumber++;
  }
  return query;
}

export function buildCountQuery(searchParams) {
  let query = '$apply=aggregate($count as count)';
  if (searchParams) {
    query += `&${buildSearchQuery(searchParams)}`
  }
  return query;
}