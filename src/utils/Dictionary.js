
const alertList = [
  {
    "id": 1,
    "label": "No alert"
  },
  {
    "id": 2,
    "label": "Yellow"
  },
  {
    "id": 3,
    "label": "Red"
  }
]
// available questionnaire = '#3CB371' 
// missedQuestionnaire = '#BFC5D2'
// not available = '#b71845'
// answered questionnaire = '#292477'
const questionnaireStatusList = [
  {
    id: 1,
    statusName: 'Available'
  },
  {
    id: 2,
    statusName: 'Not available'
  },
  {
    id: 3,
    statusName: 'Finished'
  },
  {
    id: 4,
    statusName: 'Missed'
  },
]
const questionList = [
  {
    "id": 1,
    "label": "Single choice"
  },
  {
    "id": 2,
    "label": "Multiple choice"
  },
  {
    "id": 3,
    "label": "Value"
  },
  {
    "id": 4,
    "label": "Open"
  },
  {
    "id": 5,
    "label": "File"
  }
]
export const getQuestionnaireStatusById = (id) => {
  let found = questionnaireStatusList.filter(status => status.id === id)[0];
  return found ? found.statusName : null
}
export const getQuestionTypeIdByLabel = (label) => {
  console.log(label);
  let found = questionList.filter(question => question.label == label)[0];
  console.log(found);
  return found ? found.id : null;
}

export const getQuestionTypeLabelById = (id) => {
  console.log('Label by id', id);
  let filter = questionList.filter(q => q.id === id)[0];
  return filter ? filter.label : null;
}

export const getAlertTypeLabelById = (id) => {
  let filter = alertList.filter(a => a.id === id)[0];
  return filter ? filter.label : null;
};

export const getAlertTypeIdByLabel = (label) => {
  console.log(label);
  let filter = alertList.filter(a => a.label.toLowerCase() === label.toLowerCase())[0];
  return filter ? filter.id : null;
};
