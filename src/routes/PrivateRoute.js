import React, { useEffect } from 'react';
import AppNavigation from '../App/Screens/Navigation'
import Header from '../App/Screens/Header'
import Logo from '../App/Screens/Logo';
import {
   Route,
   Redirect,
 } from 'react-router-dom';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import LocalStorageService from 'Utils/LocalStorageService';
import { parseJwt } from 'Utils/tokenParser';
import {LinearProgress} from '@material-ui/core'
import PendingScreen from 'Shared/components/PendingScreen';
const localStorageService = LocalStorageService.getService();

function PrivateRoute({ component: Component, ...rest }) {

  const authenticated = !!localStorageService.getAccessToken();
  const { logout } = rest;
  return (
    <>
      <Route {...rest} render={(props) => (
        <>
          {authenticated && !logout ? (
            <>
              <Logo />
              <Header />
              <AppNavigation/>
              <main>
                {rest.isLoading ? <PendingScreen absolute/> : null}
                <Component {...props} />
              </main>
            </>
          ) : <Redirect to='/login' />
          }
        </>
      )} />
    </>
  );
};

const mapStateToProps = state => ({
  logout: state.authorization.logout,
  isLoading: state.loading.isLoading
});


export default connect(mapStateToProps, null)(PrivateRoute);
