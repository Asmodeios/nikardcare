import React from 'react';
import PatientList from '../App/Screens/Patients';
import Patient from '../App/Screens/Patients/Components/Patient';
import ClinicalGroupList from '../App/Screens/ClinicalGroup';
import ClinicalGroupInfo from '../App/Screens/ClinicalGroup/components/ClinicalGroupInfo';
import { 
  Route, 
  Switch,
  Redirect
 } from 'react-router-dom';
import Overview from '../App/Screens/Overview';
import Questionnaires from '../App/Screens/Questionnaires';
import CreateQuestion from '../App/Screens/Question/CreateQuestion';
import QuestionnaireInfo from '../App/Screens/Questionnaires/Components/QuestionnaireInfo';
import AddQuestionnaire from '../App/Screens/Questionnaires/Components/AddQuestionnaire';
import EditQuestionnaire from '../App/Screens/Questionnaires/Components/EditQuestionnaire';
import ClinicalGroupQuestionnaires from '../App/Screens/ClinicalGroup/components/ClinicalGroupQuestionnaires';
import AnsweredQuestionnaire from '../App/Screens/Overview/Components/AnsweredQuestionnaire';
import Authorization from '../App/Screens/Authorization';
import PrivateRoute from './PrivateRoute';
import AdminRoute from './AdminRoute';
import ResetPassword from '../App/Screens/Authorization/components/ResetPassword';
import ForgotPassword from '../App/Screens/Authorization/components/ForgotPassword';
import Administration from '../App/Screens/Administration';
const Routes = () => (
	<Switch>
    <Route exact path='/login' component={Authorization}/>
    <Route exact path='/forgot' component={ForgotPassword}/>
    <Route path='/password/:mode/:id' component={ResetPassword}/>
    <PrivateRoute exact path='/overview' component={Overview} />
		<PrivateRoute exact path='/overview' component={Overview} />
		<PrivateRoute exact path='/answers/:idAnswer/:mode' component={AnsweredQuestionnaire}/>
		<PrivateRoute exact path='/patients' component={PatientList}/>
		<AdminRoute exact path='/administration' component={Administration}/>
		<PrivateRoute exact path='/groups' component={ClinicalGroupList}/>
		<PrivateRoute exact path='/groups/:idGroup' component={ClinicalGroupInfo}/>
		<PrivateRoute exact path='/groups/:idGroup/questionnaires' component={ClinicalGroupQuestionnaires}/>
		<PrivateRoute exact path='/alarmsHistory' component={PatientList}/>
		<PrivateRoute exact path='/questionnaires' component={Questionnaires}/>
		<PrivateRoute exact path='/patients/:idPatient' component={Patient}/>
		<PrivateRoute exact path='/patients/:idPatient/:tabName' component={Patient}/>
		<PrivateRoute exact path='/questionnaires/CreateQuestion' component={CreateQuestion}/>
		<PrivateRoute exact path='/questionnaires/AddQuestionnaire' component={AddQuestionnaire}/>
		<PrivateRoute exact path='/questionnaires/CreateQuestion/:idQuestionnaire' component={CreateQuestion}/>
		<PrivateRoute exact path='/questionnaires/EditQuestionnaire/:idQuestionnaire' component={EditQuestionnaire}/>
		<PrivateRoute exact path='/questionnaires/:idQuestionnaire/:mode' component={QuestionnaireInfo}/>
    <Redirect from="/" to="/patients" />
		<Route render={() => {
				return <p>Not Found</p>
			}}/>
	</Switch>
)

export default Routes;
