import * as types from './types';

const initialState = {
  authenticated: false,
  error: null,
  logout: false,
}

export default function authorizationReducer(state = initialState, action) {
    switch (action.type) {
      case types.LOGIN_SUCCESS:
        return {
          ...state,
          ...action.payload,
          authenticated: true,
          logout: false,
          error: null,
        };
      case types.LOGIN_ERROR:
        return {
          ...state,
          authenticated: false,
          error: action.payload
        };
      case types.REFRESH_TOKEN_SUCCESS:
        return {
          ...state,
          authenticated: true,
          token: action.payload.token,
          refreshToken: action.payload.refreshToken,
        };
      case types.USER_LOGOUT:
        return {
          ...state,
          logout: true,
          authenticated: false,
        }
      default: return state;
    };
  };