import * as types from './types';

export function loginSuccess(data) {
  return {
    type: types.LOGIN_SUCCESS,
    payload: data,
  };
};

export function loginError(error) {
  return {
    type: types.LOGIN_ERROR,
    payload: error
  };
};

// export function refreshTokenSuccess(data) {
//   return {
//     type: types.REFRESH_TOKEN_SUCCESS,
//     payload: data,
//   };
// };

export function userLogout() {
  return {
    type: types.USER_LOGOUT,
  };
};