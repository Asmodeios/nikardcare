import * as types from './types';

export function fetchQuestionnairesBegin() {
  return {
      type: types.FETCH_QUESTIONNAIRES_PENDING
  };
};

export function fetchQuestionnairesSuccess(questionnaires) {
  return {
      type: types.FETCH_QUESTIONNAIRES_SUCCESS,
      payload: questionnaires,
  };
};

export function fetchQuestionnairesError(error) {
  return {
      type: types.FETCH_QUESTIONNAIRES_ERROR,
      payload: error,
  };
};
export function fetchQuestionnaireBegin() {
  return {
      type: types.FETCH_QUESTIONNAIRE_PENDING
  };
};

export function fetchQuestionnaireSuccess(questionnaire) {
  return {
      type: types.FETCH_QUESTIONNAIRE_SUCCESS,
      payload: questionnaire,
  };
};

export function fetchQuestionnaireError(error) {
  return {
      type: types.FETCH_QUESTIONNAIRE_ERROR,
      payload: error,
  };
};


export function fetchQuestionsBegin() {
  return {
      type: types.FETCH_QUESTIONS_PENDING
  };
};

export function fetchQuestionsSuccess(questions) {
  return {
      type: types.FETCH_QUESTIONS_SUCCESS,
      payload: questions,
  };
};

export function fetchQuestionsError(error) {
  return {
      type: types.FETCH_QUESTIONS_ERROR,
      payload: error,
  };
};

export function createQuestion(question) {
  return {
      type: types.CREATE_QUESTION,
      payload: question,
  };
};

export function addQuestionToAdded(question) {
  return {
      type: types.ADD_QUESTION_TO_ADDED,
      payload: question,
  };
};

export function removeQuestionFromAdded(question) {
  return {
      type: types.REMOVE_QUESTION_FROM_ADDED,
      payload: question,
  };
};

export function addQuestionToQuestionnaire(question) {
  return {
      type: types.ADD_QUESTION_TO_QUESTIONNAIRE,
      payload: question,
  };
};

export function removeQuestionFromQuestionnaire(question) {
  return {
      type: types.REMOVE_QUESTION_FROM_QUESTIONNAIRE,
      payload: question,
  };
};

export function getQuestionToEdit(question) {
  return {
    type: types.GET_QUESTION_TO_EDIT,
    payload: question,
  };
};

export function clearEditQuestion() {
  return {
    type: types.CLEAR_EDIT_QUESTION,
  };
};

export function editQuestion(question) {
  return {
    type: types.EDIT_QUESTION,
    payload: question,
  };
};

export function clearEditQuestions() {
  return {
    type: types.CLEAR_EDIT_QUESTIONS,
  };
};

export function clearAddedQuestions() {
  return {
    type: types.CLEAR_ADDED_QUESTIONS,
  };
};

export function editQuestionnaireBegin() {
  return {
    type: types.EDIT_QUESTIONNAIRE_BEGIN,
  };
};

export function editQuestionnaireSuccess() {
  return {
    type: types.EDIT_QUESTIONNAIRE_SUCCESS,
  };
};

export function editQuestionnaireError(error) {
  return {
    type: types.EDIT_QUESTIONNAIRE_ERROR,
    payload: error,
  }
};

export function createQuestionnaireBegin() {
  return {
    type: types.CREATE_QUESTIONNAIRE_BEGIN,
  };
};

export function createQuestionnaireSuccess() {
  return {
    type: types.CREATE_QUESTIONNAIRE_SUCCESS,
  };
};

export function createQuestionnaireError(error) {
  return {
    type: types.CREATE_QUESTIONNAIRE_ERROR,
    payload: error,
  }
};