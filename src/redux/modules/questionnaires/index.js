import * as types from './types';

const initialState = {
  isPending: false,
  questionnaires: {
		list: [],
		isPending: false,
		error: null,
	},
	questionnaire: {
		isPending: false,
		isEdited: false,
		error: null,
		isAdded: false,
	},
	questions: {
		isPending: false,
		addedQuestions: [],
		existingQuestions: [],
		editQuestions: [],
		error: null,
	},
	editQuestion: null,
};


export default function questionnairesReducer(state = initialState, action) {
	switch (action.type) {
		case types.FETCH_QUESTIONNAIRES_PENDING:
			return {
					...state,
					questionnaires: {
						...state.questionnaires,
						isPending: true,
					}
			};
		case types.FETCH_QUESTIONNAIRES_SUCCESS:
			return {
					...state,
					questionnaires: {
						...state.questionnaires,
						isPending: false,
						list: action.payload,
					},
					questionnaire: {
						isEdited: false,
						isAdded: false,
						isPending: false,
					}
			};
		case types.FETCH_QUESTIONNAIRES_ERROR:
			return {
					...state,
					questionnaires: {
						...state.questionnaires,
						isPending: false,
						error: action.error,
					}
			};
		case types.FETCH_QUESTIONNAIRE_PENDING:
			return {
				...state,
				questionnaire: {
					...state.questionnaire,
					isPending: true,
				}
			};
		case types.FETCH_QUESTIONNAIRE_SUCCESS:
			return {
				...state,
				questionnaire: {
					...state.questionnaire,
					...action.payload,
          isPending: false,
          isEdited: false,
				},
				questions: {
					...state.questions,
					editQuestions: [...action.payload.questions]
				}
			};
		case types.FETCH_QUESTIONNAIRE_ERROR:
			return {
				...state,
				qeustionnaire: {
					...state.questionnaire,
					error: action.error,
					isPending: false,
				}
			};
		case types.FETCH_QUESTIONS_PENDING:
				return {
					...state,
					questions: {
						...state.questions,
						isPending: true,
					}
				};
		case types.FETCH_QUESTIONS_SUCCESS:
			return {
				...state,
				questions: {
					...state.questions,
					isFetched: true,
					isPending: false,
					existingQuestions: action.payload,
				}
			};
		case types.FETCH_QUESTIONS_ERROR:
			return {
				...state,
				questions: {
					...state.questions,
					isPending: false,
					error: action.error,
				}
			};
		case types.CREATE_QUESTION:
			return {
				...state,
				questions: {
					...state.questions,
					addedQuestions: [...state.questions.addedQuestions.filter(x => x.idQuestion !== action.payload.idQuestion), action.payload],
				}
			};
		case types.ADD_QUESTION_TO_ADDED:
			return {
				...state,
				questions: {
					...state.questions,
					addedQuestions: [...state.questions.addedQuestions, action.payload],
				}
			};
		case types.REMOVE_QUESTION_FROM_ADDED:
			return {
				...state,
				questions: {
					...state.questions,
					addedQuestions: state.questions.addedQuestions.filter(q => q != action.payload),
				}
			};
			case types.ADD_QUESTION_TO_QUESTIONNAIRE:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
					},
					questions: {
						...state.questions,
						editQuestions: [...state.questions.editQuestions, action.payload],
					}
				};
			case types.REMOVE_QUESTION_FROM_QUESTIONNAIRE:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
					},
					questions: {
						...state.questions,
						editQuestions: state.questions.editQuestions.filter(q => q != action.payload),
					}
				};
			case types.GET_QUESTION_TO_EDIT:
				return {
					...state,
					editQuestion: action.payload,
				};
			case types.CLEAR_EDIT_QUESTION:
				return {
					...state,
					editQuestion: null,
				};
			case types.EDIT_QUESTION:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
					},
					questions: {
						...state.questions,
						editQuestions: [...state.questions.editQuestions.filter(x => x.idQuestion !== action.payload.idQuestion), action.payload],
					},
				};
			case types.CLEAR_EDIT_QUESTIONS: 
				return {
					...state,
					questions: {
						...state.questions,
						editQuestions: [],
					}
				};
			case types.EDIT_QUESTIONNAIRE_BEGIN:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
						isPending: true,
					}
				};
			case types.EDIT_QUESTIONNAIRE_SUCCESS:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
						isEdited: true,
						isPending: false,
					}
				};
			case types.EDIT_QUESTIONNAIRE_ERROR:
				return {
					...state,
					questionnaire: {
						...state.questionnaires,
						isEdited: false,
						isPending: false,
						error: action.payload,
					}
				}
			case types.CREATE_QUESTIONNAIRE_BEGIN:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
						isPending: true,
					}
				};
			case types.CREATE_QUESTIONNAIRE_SUCCESS:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
						isAdded: true,
						isPending: false,
					}
				};
			case types.CREATE_QUESTIONNAIRE_ERROR:
				return {
					...state,
					questionnaire: {
						...state.questionnaire,
						isAdded: false,
						isPending: false,
						error: action.payload,
					}
        };
      case types.CLEAR_ADDED_QUESTIONS:
        return {
          ...state,
          questions: {
            ...state.questions,
            addedQuestions: [],
          }
        }
		default:
			return state;
  };
};