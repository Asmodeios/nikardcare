import * as types from './types';


export function startLoading() {
  return{
    type: types.START_LOADING,
  }
}

export function finishLoading() {
  return {
    type: types.FINISH_LOADING
  }
}
