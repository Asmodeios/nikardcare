import * as types from './types';types

const initialState = {
  isLoading: false,
}


export default function loadingReducer(state = initialState, action) {
  switch(action.type){
    case types.START_LOADING:
      return {
        ...state,
        isLoading: true
      };
    case types.FINISH_LOADING:
      return {
        ...state,
        isLoading: false
      }
    default: return state;
  }
}
