import questionnairesReducer from './questionnaires';
import authorizationReducer from './authorization';
import loadingReducer from './loading';
import { combineReducers } from 'redux';


const rootReducer = combineReducers({
  questionnaires: questionnairesReducer,
  authorization: authorizationReducer,
  loading: loadingReducer,
});

export default rootReducer;
